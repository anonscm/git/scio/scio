/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.soap.SoapFault;
import org.evolvis.scio.soap.SoapStreamException;

@SuppressWarnings("serial")
public class LoginError extends SoapFault implements MySoapConstants {

	private final SoapFaultSubCode subCode;

	private String user;
	
	public LoginError(SoapFaultSubCode subCode) {
		super(subCode.getCode(), subCode.getSubCodeList());
		this.subCode = subCode;
	}

	@Override
	public void readDetailEntry(XMLStreamReader reader) throws XMLStreamException {
		if (subCode == SoapFaultSubCode.LOGIN_ERROR_ILLEGAL_USERNAME) {
			reader.require(XMLStreamConstants.START_ELEMENT, NAMESPACE_FAULT, ELEMENT_NAME_FAULT_DETAILS);
			reader.nextTag();
			reader.require(XMLStreamConstants.START_ELEMENT, NAMESPACE_FAULT, ELEMENT_NAME_USERNAME);
			user = reader.getElementText();
		} else
			throw new SoapStreamException("no detail entry expected for this subcode");
	}

	@Override
	public void writeDetailEntries(XMLStreamWriter writer) throws XMLStreamException {
		if (subCode == SoapFaultSubCode.LOGIN_ERROR_ILLEGAL_USERNAME) {
			writer.writeStartElement(NAMESPACE_FAULT, ELEMENT_NAME_FAULT_DETAILS);
			writer.writeStartElement(NAMESPACE_FAULT, ELEMENT_NAME_USERNAME);
			writer.writeCharacters(user);
			writer.writeEndElement();
			writer.writeEndElement();
			writer.writeStartElement(NAMESPACE_FAULT, ELEMENT_NAME_USERNAME);
			writer.writeEndElement();
		}
	}

	@Override
	public String toString() {
		String str = super.toString();
		if (subCode == SoapFaultSubCode.LOGIN_ERROR_ILLEGAL_USERNAME)
			str += "\nuser  : "+user;
		return str;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
}
