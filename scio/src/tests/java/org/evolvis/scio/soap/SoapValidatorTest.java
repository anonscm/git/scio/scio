/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.io.IOException;

import org.xml.sax.SAXException;

import junit.framework.TestCase;

public class SoapValidatorTest extends TestCase {

	public void testSimple() throws SAXException, IOException {
		assertTrue("test valid SOAP", SoapValidator.isValid("<?xml version='1.0' encoding='UTF-8'?><env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"><env:Body /></env:Envelope>"));
		assertTrue("test valid SOAP without pi", SoapValidator.isValid("<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"><env:Body /></env:Envelope>"));
		assertFalse("empty message", SoapValidator.isValid(""));
		assertFalse("no body", SoapValidator.isValid("<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"></env:Envelope>"));
		assertFalse("wrong env namespace", SoapValidator.isValid("<env:Envelope xmlns:env=\"http://www.w3.org/2003/06/soap-envelope\"><env:Body /></env:Envelope>"));
	}
}
