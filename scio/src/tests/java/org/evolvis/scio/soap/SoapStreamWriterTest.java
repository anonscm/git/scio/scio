/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.io.IOException;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import junit.framework.TestCase;

import org.evolvis.scio.XmlSerializable;
import org.evolvis.scio.soap.SoapFault;
import org.evolvis.scio.soap.SoapStreamException;
import org.evolvis.scio.soap.SoapStreamWriter;
import org.evolvis.scio.soap.SoapConstants.SoapFaultCode;
import org.evolvis.scio.soap.SoapFault.Reason;
import org.xml.sax.SAXException;

/**
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class SoapStreamWriterTest extends TestCase {


	public void testWriteHeaderBlocks() throws XMLStreamException, FactoryConfigurationError, SoapStreamException, SAXException, IOException {
		SoapValidator.OutputStream os;
		os = new SoapValidator.OutputStream();
		SoapStreamWriter swriter = new SoapStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(os));
		
		swriter.writeHeaderBlock(new XmlSerializable() {
			public void writeXmlRepresentation(XMLStreamWriter writer) throws XMLStreamException {
				writer.writeStartElement("http://namespace1", "elem1");
				writer.writeStartElement("elem2");
			}
		}, "testrole2", true, false);
		swriter.writeHeaderBlock(new XmlSerializable() {
			public void writeXmlRepresentation(XMLStreamWriter writer) throws XMLStreamException {
				writer.writeStartElement("http://namespace3", "elem3");
				writer.writeStartElement("elem4");
			}
		}, "testrole", false, true);
		swriter.writeBodyBlock(new XmlSerializable(){
			public void writeXmlRepresentation(XMLStreamWriter writer) throws XMLStreamException {
				writer.writeStartElement("mybodynamespace", "mybodyelement");
			}
		});
		swriter.close();
		assertTrue("test if soap is valid", os.isValid());
	}

	public void testMinimal() throws XMLStreamException, FactoryConfigurationError, SAXException, IOException, SoapStreamException {
		SoapValidator.OutputStream os;
		os = new SoapValidator.OutputStream();
		SoapStreamWriter swriter = new SoapStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(os));
		swriter.close();
		assertTrue("test if soap is valid", os.isValid());
	}
	
	public void testFault() throws XMLStreamException, FactoryConfigurationError, SAXException, IOException, SoapStreamException {
		SoapValidator.OutputStream os;
		os = new SoapValidator.OutputStream();
		SoapStreamWriter swriter = new SoapStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(os));

		SoapFault fault = new SoapFault(SoapFaultCode.SENDER);

		
		try {
			swriter.writeFault(fault);
			fail("no reason => excpetion");
		} catch (RuntimeException e) {
		}

		fault.addReason(new Reason("de", "Fehlergrund"));

		swriter.writeFault(fault);
		swriter.close();
		assertTrue("test if soap is valid", os.isValid());
	}
	
	public void testFaultWithBodyBlock() throws XMLStreamException, FactoryConfigurationError, SAXException, IOException, SoapStreamException {
		SoapValidator.OutputStream os;
		os = new SoapValidator.OutputStream();
		SoapStreamWriter swriter = new SoapStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(os));

		swriter.writeBodyBlock(new XmlSerializable() {
			public void writeXmlRepresentation(XMLStreamWriter writer) throws XMLStreamException {
				writer.writeStartElement("namespace", "test");
			}
		});
		
		SoapFault fault = new SoapFault(SoapFaultCode.SENDER);
		fault.addReason(new Reason("de", "Fehlergrund"));

		try {
			swriter.writeFault(fault);
			fail("body content => exception");
		} catch (Exception e) {
		}		
		swriter.close();
		assertTrue("test if soap is valid", os.isValid());
	}
}
