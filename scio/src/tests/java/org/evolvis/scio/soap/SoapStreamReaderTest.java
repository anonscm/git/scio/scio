/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;

import junit.framework.TestCase;

import org.evolvis.scio.soap.BodyBlockReader;
import org.evolvis.scio.soap.HeaderBlockReader;
import org.evolvis.scio.soap.SoapFault;
import org.evolvis.scio.soap.SoapStreamException;
import org.evolvis.scio.soap.SoapStreamReader;
import org.evolvis.scio.soap.SoapConstants.SoapFaultCode;
import org.evolvis.scio.soap.SoapConstants.SoapVersion;
import org.evolvis.scio.soap.SoapFault.Reason;

public class SoapStreamReaderTest extends TestCase {

	private String soapMinimal = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
    "<env:Body/>"+
 "</env:Envelope>";
	private String soapMinimalWithHeader = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
    "<env:Header/><env:Body/>"+
 "</env:Envelope>";
	private String soapIllegalWithoutBody = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"/>";
	private String soapIllegalWithoutBodyButWithHeader = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
    "<env:Header/>"+
 "</env:Envelope>";
	private String soapIllegalWithoutBodyButWithHeaderBlock = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
    "<env:Header><myheader/></env:Header>"+
 "</env:Envelope>";
	private String soapIllegalWrongRoot = "<bla/>";
	private String soapIllegalUnknownVersion = "<env:Envelope xmlns:env=\"http://unknownnamespace\">"+
    		"<env:Body/>"+
    		"</env:Envelope>";
	


	
	public void testSoapSimple() throws XMLStreamException, SoapStreamException, FactoryConfigurationError {
		SoapStreamReader sr;
		
		sr = createReader(soapMinimal);
		assertEquals(sr.getSoapVersion(), SoapVersion.SOAP_1_2);
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasFault());
		assertFalse(sr.hasFault());
		assertFalse(sr.hasFault());
		
		sr = createReader(soapMinimalWithHeader);
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasFault());
		assertFalse(sr.hasFault());
		assertFalse(sr.hasFault());
		
		sr = createReader(soapIllegalWithoutBody);
		assertEquals(sr.getSoapVersion(), SoapVersion.SOAP_1_2);
		try {
			sr.hasNextBodyBlock();
			fail();
		} catch (XMLStreamException e) {
		}
		
		sr = createReader(soapIllegalWithoutBodyButWithHeader);
		try {
			sr.hasNextBodyBlock();
			fail();
		} catch (XMLStreamException e) {
		}
		
		sr = createReader(soapIllegalWithoutBodyButWithHeaderBlock);
		try {
			sr.hasNextBodyBlock();
			fail();
		} catch (XMLStreamException e) {
		}
		
		sr = createReader(soapIllegalWrongRoot);
		try {
			sr.hasNextHeaderBlock();
			fail();
		} catch (XMLStreamException e) {
		}
		
		sr = createReader(soapIllegalUnknownVersion);
		try {
			sr.hasNextHeaderBlock();
			fail();
		} catch (XMLStreamException e) {
		}	
	}
	
	
	private String soapWithHeaderBlock = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
    "<env:Header>"+
    	"<m:block1 xmlns:m=\"nsb1\">text1<m:b1s>text2</m:b1s>text3</m:block1>"+
    	"<n:block2 xmlns:n=\"nsb2\" xmlns:m=\"nsb1\">text4<m:b2s>text5</m:b2s>text6</n:block2>"+
    	"<o:block3 xmlns:o=\"nsb3\" xmlns:n=\"nsb2\">text7<n:b3s/>text8</o:block3>"+
    "</env:Header>"+
    "<env:Body/>"+
 "</env:Envelope>";
	
	public void testSoapHeaders() throws XMLStreamException, SoapStreamException, FactoryConfigurationError {
		SoapStreamReader sr;
		
		sr = createReader(soapWithHeaderBlock);
		assertTrue(sr.hasNextHeaderBlock());
		
		HeaderBlockReader hb = sr.nextHeaderBlock();
		assertEquals(XMLStreamConstants.START_ELEMENT, hb.getEventType());
		assertEquals(new QName("nsb1", "block1"), hb.getName());
		assertEquals(XMLStreamConstants.CHARACTERS, hb.next());
		assertEquals("text1", hb.getText());
		assertEquals(XMLStreamConstants.START_ELEMENT, hb.next());
		assertEquals(new QName("nsb1", "b1s"), hb.getName());
		assertEquals(XMLStreamConstants.CHARACTERS, hb.next());
		assertEquals("text2", hb.getText());
		assertEquals(XMLStreamConstants.END_ELEMENT, hb.next());
		assertEquals(XMLStreamConstants.CHARACTERS, hb.next());
		assertEquals("text3", hb.getText());
		// don't read end tag => must be skipped
		
		hb = sr.nextHeaderBlock();
		assertEquals(XMLStreamConstants.START_ELEMENT, hb.getEventType());
		assertEquals(new QName("nsb2", "block2"), hb.getName());
		assertEquals(XMLStreamConstants.CHARACTERS, hb.next());
		assertEquals("text4", hb.getText());
		assertEquals(XMLStreamConstants.START_ELEMENT, hb.next());
		assertEquals(new QName("nsb1", "b2s"), hb.getName());
		assertEquals(XMLStreamConstants.CHARACTERS, hb.next());
		assertEquals("text5", hb.getText());
		assertEquals(XMLStreamConstants.END_ELEMENT, hb.next());
		assertEquals(XMLStreamConstants.CHARACTERS, hb.next());
		assertEquals("text6", hb.getText());
		assertTrue(hb.hasNext());
		assertEquals(XMLStreamConstants.END_ELEMENT, hb.next());
		assertFalse(hb.hasNext());
		assertEquals(XMLStreamConstants.END_DOCUMENT, hb.next());
		try {
			hb.next();
			fail("end of subtree");
		} catch (XMLStreamException e) {
		}
		
		hb = sr.nextHeaderBlock();
		assertEquals(XMLStreamConstants.START_ELEMENT, hb.getEventType());
		assertEquals(new QName("nsb3", "block3"), hb.getName());
		assertEquals(XMLStreamConstants.CHARACTERS, hb.next());
		assertEquals("text7", hb.getText());
		assertEquals(XMLStreamConstants.START_ELEMENT, hb.next());
		assertEquals(new QName("nsb2", "b3s"), hb.getName());
		assertEquals(XMLStreamConstants.END_ELEMENT, hb.next());
		assertEquals(XMLStreamConstants.CHARACTERS, hb.next());
		assertEquals("text8", hb.getText());
		assertTrue(hb.hasNext());
		assertEquals(XMLStreamConstants.END_ELEMENT, hb.next());
		assertFalse(hb.hasNext());
		assertEquals(XMLStreamConstants.END_DOCUMENT, hb.next());
		try {
			hb.next();
			fail("end of subtree");
		} catch (XMLStreamException e) {
		}

		try {
			assertFalse(sr.hasNextHeaderBlock());
			hb = sr.nextHeaderBlock();
			fail("end of header");
		} catch (NoSuchElementException e) {
		}
		assertFalse(sr.hasNextHeaderBlock());
		

		sr = createReader(soapWithHeaderBlock);
		assertTrue(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextBodyBlock());
	}
	
	

	private String soapWithBody = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
    "<env:Body>"+
       "<n0:identityBoolean xmlns:n0=\"http://www.evolvis.org/scio/soap\">"+
          "<value>false</value>"+
       "</n0:identityBoolean>"+
       "<n0:test xmlns:n0=\"ns2\">"+
       	  "testtext"+
       "</n0:test>"+
       "<test2/>"+
    "</env:Body>"+
 "</env:Envelope>";
	
	public void testSoapBody() throws XMLStreamException, FactoryConfigurationError {
		SoapStreamReader sr;
		
		sr = createReader(soapWithBody);
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		BodyBlockReader bbr = sr.nextBodyBlock();
		assertEquals(XMLStreamConstants.START_ELEMENT, bbr.getEventType());
		assertEquals(new QName("http://www.evolvis.org/scio/soap", "identityBoolean"), bbr.getName());
		assertEquals(XMLStreamConstants.START_ELEMENT, bbr.next());
		assertEquals(new QName("", "value"), bbr.getName());
		assertEquals(XMLStreamConstants.CHARACTERS, bbr.next());
		assertEquals("false", bbr.getText());
		assertEquals(XMLStreamConstants.END_ELEMENT, bbr.next());
		assertEquals(XMLStreamConstants.END_ELEMENT, bbr.next());
		assertEquals(XMLStreamConstants.END_DOCUMENT, bbr.next());

		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		bbr = sr.nextBodyBlock();
		assertEquals(XMLStreamConstants.START_ELEMENT, bbr.getEventType());
		assertEquals(new QName("ns2", "test"), bbr.getName());
		// don't read complete subtree => must be skipped

		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		bbr = sr.nextBodyBlock();
		assertEquals(XMLStreamConstants.START_ELEMENT, bbr.getEventType());
		assertEquals(new QName("", "test2"), bbr.getName());
		assertEquals(XMLStreamConstants.END_ELEMENT, bbr.next());
		assertEquals(XMLStreamConstants.END_DOCUMENT, bbr.next());

		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		try {
			bbr = sr.nextBodyBlock();
			fail("no more body blocks");
		} catch (NoSuchElementException e) {
		}		
	}
	
	
	private String soapFaultMinimal = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
    "<env:Body>"+
    	"<env:Fault>"+
    		"<env:Code>"+
    		"<env:Value>env:Sender</env:Value>"+
    		"</env:Code>"+
    		"<env:Reason>"+
    			"<env:Text xml:lang=\"en\">reason1</env:Text>"+
    		"</env:Reason>"+
    	"</env:Fault>"+
    "</env:Body>"+
 "</env:Envelope>";


	private String soapFaultWithAll = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
    "<env:Body>"+
    	"<env:Fault>"+
    		"<env:Code>"+
    		"<env:Value>env:Sender</env:Value>"+
    		"<env:Subcode>"+
    		"<env:Value xmlns:m=\"testns1\">m:testcode1</env:Value>"+
    		"</env:Subcode>"+
    		"</env:Code>"+
    		"<env:Reason>"+
				"<env:Text xml:lang=\"en\">reason1</env:Text>"+
				"<env:Text xml:lang=\"de\">grund1</env:Text>"+
    		"</env:Reason>"+
    		"<env:Node>node1</env:Node>"+
    		"<env:Role>role1</env:Role>"+
    	"</env:Fault>"+
    "</env:Body>"+
 "</env:Envelope>";
	
	public void testSoapFault() throws XMLStreamException, SoapStreamException, FactoryConfigurationError {
		SoapStreamReader sr;
		
		sr = createReader(soapFaultMinimal);
		assertTrue(sr.hasFault());
		SoapFault fault = sr.getFault();
		assertEquals(fault.getCode(), SoapFaultCode.SENDER);
		assertNull(fault.getRole());
		assertTrue(fault.getSubCodeList().isEmpty());
		assertNull(fault.getNode());
		Iterator<Reason> it = fault.getReasonIterator();
		assertTrue(it.hasNext());
		Reason r = it.next();
		assertEquals(r.getText(), "reason1");
		assertEquals(r.getLanguage(), "en");
		assertFalse(it.hasNext());
		
		sr = createReader(soapFaultWithAll);
		List<QName> scds = sr.getFault().getSubCodeList();
		assertEquals(1, scds.size());
		assertEquals(new QName("testns1", "testcode1"), scds.get(0));
		assertTrue(sr.hasFault());
		fault = sr.getFault();
		assertEquals(fault.getRole(), "role1");
		assertEquals(fault.getNode(), "node1");
		it = fault.getReasonIterator();
		assertTrue(it.hasNext());
		r = it.next();
		assertEquals(r, new Reason("en", "reason1"));
		assertTrue(it.hasNext());
		r = it.next();
		assertEquals(r, new Reason("de", "grund1"));
		assertFalse(it.hasNext());
	}
		
	

	private String soapWithoutHeader = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
      "<env:Body>"+
         "<n0:identityBoolean xmlns:n0=\"http://www.evolvis.org/scio/soap\">"+
            "<value>false</value>"+
         "</n0:identityBoolean>"+
      "</env:Body>"+
   "</env:Envelope>";
	
	public void testSoapWithoutHeader() throws XMLStreamException, SoapStreamException, FactoryConfigurationError {
		SoapStreamReader sr;
		sr = createReader(soapWithoutHeader);
		assertFalse(sr.hasNextHeaderBlock());
		try {
			sr.nextHeaderBlock();
			fail("no header available");
		} catch (NoSuchElementException e) {
		}
		assertFalse(sr.hasNextHeaderBlock());
	}
	
	private String soapWithoutBody = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
	 "<env:Header/>"+
   "</env:Envelope>";
	
	public void testSoapWithoutBody() throws XMLStreamException, SoapStreamException, FactoryConfigurationError {
		SoapStreamReader sr;
		sr = createReader(soapWithoutBody);

			try {
				sr.hasNextHeaderBlock();
				fail("no body available");
			} catch (XMLStreamException e) {
			}
	}
	
	private String soapWithEmptyHeaderAndBody = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
	 "<env:Header/>"+
     "<env:Body/>"+
  "</env:Envelope>";
	
	public void testSoapWithEmptyHeaderAndBody() throws XMLStreamException, SoapStreamException, FactoryConfigurationError {
		SoapStreamReader sr;
		sr = createReader(soapWithEmptyHeaderAndBody);
			assertFalse(sr.hasNextHeaderBlock());
	}
		
	
	private String soapWithHeader = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
	 "<env:Header>"+
	    "<blib>pp<blab>jj</blab>abc</blib> <def>ghi</def>"+
	 "</env:Header>"+
      "<env:Body>"+
         "<n0:identityBoolean xmlns:n0=\"http://www.evolvis.org/scio/soap\">"+
            "<value>false</value>"+
         "</n0:identityBoolean>"+
         "<abc/>"+
      "</env:Body>"+
   "</env:Envelope>";

	public void testSoapWithHeader() throws XMLStreamException, SoapStreamException, FactoryConfigurationError {
		SoapStreamReader sr;
		sr = createReader(soapWithHeader);
		assertTrue(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextHeaderBlock());
		sr.nextHeaderBlock();
		assertTrue(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextHeaderBlock());
		sr.nextHeaderBlock();
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertFalse(sr.hasNextHeaderBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertEquals(new QName("http://www.evolvis.org/scio/soap", "identityBoolean"), sr.nextBodyBlock().getName());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertTrue(sr.hasNextBodyBlock());
		assertEquals(new QName("abc"), sr.nextBodyBlock().getName());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		assertFalse(sr.hasNextBodyBlock());
		
		sr = createReader(soapWithHeader);
		HeaderBlockReader block = sr.nextHeaderBlock();
		assertEquals(block.getName(), new QName("blib"));
		sr.nextHeaderBlock();
		assertEquals(block.getName(), new QName("def"));
		try {
			sr.nextHeaderBlock();
			System.out.println(block.getName());
			fail("no header available");
		} catch (NoSuchElementException e) {
		}
	}

	private SoapStreamReader createReader(String soap) throws XMLStreamException, FactoryConfigurationError {
		return new SoapStreamReader(XMLInputFactory.newInstance().createXMLStreamReader(
				new ByteArrayInputStream(soap.getBytes())));
	}
}
