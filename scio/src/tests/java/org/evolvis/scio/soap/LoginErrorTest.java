/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

import org.evolvis.scio.soap.SoapFault;
import org.evolvis.scio.soap.SoapStreamException;
import org.evolvis.scio.soap.SoapStreamReader;
import org.evolvis.scio.soap.SoapStreamWriter;

import junit.framework.TestCase;

public class LoginErrorTest extends TestCase implements MySoapConstants {

	
	private List<QName> subCodeList = SoapFaultSubCode.LOGIN_ERROR_ILLEGAL_USERNAME.getSubCodeList();
	
	private String soapFaultWithAll = "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\">"+
    "<env:Body>"+
    	"<env:Fault>"+
    		"<env:Code>"+
    		"<env:Value>env:Sender</env:Value>"+
    		"<env:Subcode>"+
    		"<env:Value xmlns:m=\""+subCodeList.get(0).getNamespaceURI()+"\">m:"+subCodeList.get(0).getLocalPart()+"</env:Value>"+
    		"<env:Subcode>"+
    		"<env:Value xmlns:m=\""+subCodeList.get(1).getNamespaceURI()+"\">m:"+subCodeList.get(1).getLocalPart()+"</env:Value>"+
    		"</env:Subcode>"+
    		"</env:Subcode>"+
    		"</env:Code>"+
    		"<env:Reason>"+
				"<env:Text xml:lang=\"en\">reason1</env:Text>"+
				"<env:Text xml:lang=\"de\">grund1</env:Text>"+
    		"</env:Reason>"+
    		"<env:Node>node1</env:Node>"+
    		"<env:Role>role1</env:Role>"+
    		"<env:Detail>"+
    		"<my:faultDetails xmlns:my=\"http://www.evolvis.org/scio/soap/faults\">"+
    		"<my:username>"+
    		"peter"+
    		"</my:username>"+
    		"</my:faultDetails>"+
    		"</env:Detail>"+
    	"</env:Fault>"+
    "</env:Body>"+
 "</env:Envelope>";
	
	public void testSoapFault() throws XMLStreamException, SoapStreamException, FactoryConfigurationError {
		SoapStreamReader sr = createReader(soapFaultWithAll);
		assertTrue(sr.hasFault());
		SoapFault fault = sr.getFault();
		assertTrue(fault instanceof LoginError);
		LoginError lerr = (LoginError) fault;
		assertEquals("peter", lerr.getUser());
		assertEquals(SoapFaultCode.SENDER, fault.getCode());
		List<QName> scl = fault.getSubCodeList();
		SoapFaultSubCode code = SoapFaultSubCode.valueOfId(fault.getCode(), scl);

		assertEquals(SoapFaultSubCode.LOGIN_ERROR_ILLEGAL_USERNAME, code);

		StringBuilder result = new StringBuilder();
		SoapStreamWriter sw = createWriter(result);
		sw.writeFault(fault);
		System.out.println(result);
	}
	
	private SoapStreamReader createReader(String soap) throws XMLStreamException, FactoryConfigurationError {
		return new SoapStreamReader(XMLInputFactory.newInstance().createXMLStreamReader(
				new ByteArrayInputStream(soap.getBytes())), new MySoapFaultFactory());
	}

	
	private SoapStreamWriter createWriter(final StringBuilder sb) throws XMLStreamException, FactoryConfigurationError {
		return new SoapStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(
				new OutputStream() {
					@Override
					public void write(int b) throws IOException {
						sb.append((char)b);
					}
				}));
	}
}
