/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Endpoint;
import javax.xml.ws.soap.SOAPBinding;

import junit.framework.TestCase;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.evolvis.scio.bind.BindContext;
import org.evolvis.scio.bind.BindContextHolder;
import org.evolvis.soapdemo.TestInterface;
import org.evolvis.soapdemo.types.TestObject;

public class TestServiceTest extends TestCase {

	public static final boolean START_CXF_SERVER = true;
	public static final boolean START_MY_SERVER = false;

	public static final int PORT_INCREASE_TCP_MONITOR = 1;
	public static final boolean USE_TCP_MONITOR_FOR_CXF_SERVER = false;
	public static final boolean USE_TCP_MONITOR_FOR_MY_SERVER = false;

	public static final int PORT_CXF_SERVER = 9000;
	public static final int PORT_MY_SERVER = 9002;
	
	public static final boolean USE_CXF_CLIENT = true;
	public static final boolean USE_MY_CLIENT = true;
	
	public static final String CXF_SOAP_BINDING_ID = SOAPBinding.SOAP12HTTP_BINDING;

	private List<String> activeServerUrls = new LinkedList<String>();
	
	/**
	 * Will be added in setUp to be stopped in tearDown
	 */
	private List<Endpoint> activeEndpoints = new LinkedList<Endpoint>();
	
	/**
	 * This client can be used in the test methods. if a operation of this
	 * client is called it has to do all the soap calls to all the servers.
	 */
	private TestInterface testClient;
	

	private static final String createServerUrl(int port) {
		return "http://localhost:"+port+"/testService";
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		// start all servers
		if (START_CXF_SERVER) {
			String url = createServerUrl(PORT_CXF_SERVER);
			Endpoint endpoint = Endpoint.create(CXF_SOAP_BINDING_ID, new TestInterfaceImpl());
			endpoint.publish(url);
			activeEndpoints.add(endpoint);
			if (USE_TCP_MONITOR_FOR_CXF_SERVER)
				url = createServerUrl(PORT_CXF_SERVER + PORT_INCREASE_TCP_MONITOR);
			activeServerUrls.add(url);
		}
		if (START_MY_SERVER) {
			throw new RuntimeException("please implement");
		}

		SimultaneousInvocationHandler<TestInterface> client = new SimultaneousInvocationHandler<TestInterface>() {
			@Override
			public boolean checkEqual(Object objA, Object objB) {
				if (objA != null && objB != null)
					if (objA instanceof TestObject && objB instanceof TestObject)
						return equalsTestObject((TestObject) objA, (TestObject) objB); 
				return super.checkEqual(objA, objB);
			}
		};
		
		testClient = (TestInterface) Proxy.newProxyInstance(
				getClass().getClassLoader(),
                new Class[] { TestInterface.class },
                client);
		
		// create a client for each active server and for each client type
		if (USE_CXF_CLIENT) {
			JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
			factory.getInInterceptors().add(new LoggingInInterceptor());
			factory.getOutInterceptors().add(new LoggingOutInterceptor());
			factory.setBindingId(CXF_SOAP_BINDING_ID);
			factory.setServiceClass(TestInterface.class);
			for (String url : activeServerUrls) {
				factory.setAddress(url);
				TestInterface _client = (TestInterface) factory.create();
				client.addClient(_client);
			}
		}
		if (USE_MY_CLIENT) {
			// create and register bind context
			BindContext defaultContext = BindContext.newInstance("org.evolvis.soapdemo.types");
			BindContextHolder.setDefaultContext(defaultContext);
			
			for (String url : activeServerUrls) {
				TestInterface scioClient = (TestInterface) StubFactory.getInstance().createStub(url, TestInterface.class);
				client.addClient(scioClient);
			}
		}
	}
	
	public void testSipmle() throws DatatypeConfigurationException {
		byte b = createRandomByte();
		assertEquals(b, testClient.identityByte(b));
		
		TestObject to = new TestObject();
		to.setDateParam(createRandomXMLGregorianCalendar());
		to.setStringParam(createRandomString());
		assertEquals(to, testClient.identityTestObject(to));
		
		String s = createRandomString();
		assertEquals(s, testClient.identityString(s));
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		for (Endpoint ep : activeEndpoints)
			ep.stop();
	}


	private boolean equalsTestObject(TestObject to1, TestObject to2) {
		return to1.getStringParam().equals(to2.getStringParam()) &&
		to1.getStringParam().equals(to2.getStringParam());
	}

	private void assertEquals(TestObject to1, TestObject to2) {
		assertTrue(equalsTestObject(to1, to2));
	}
	
	private void assertEquals(String str, byte[] ba, byte[] bc) {
		assertNotNull(str, ba);
		assertNotNull(str, bc);
		assertEquals(str, ba.length, bc.length);
		for (int i = 0; i < ba.length; i++)
			assertEquals(str, ba[i], bc[i]);
	}
	
	private int createRandomInteger(int min, int max) {
		return (int) Math.round(Math.random() * ((long)max - min) + min);
	}
	
	private int createRandomInteger() {
		return createRandomInteger(Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	private double createRandomDouble(double min, double max) {
		return Math.random() * (max - min) + min;
	}
	
	private XMLGregorianCalendar createRandomXMLGregorianCalendar() throws DatatypeConfigurationException {
		BigInteger year = BigInteger.valueOf(createRandomInteger(-999999999, 999999999));
		int month = createRandomInteger(1, 12);
		int day = createRandomInteger(1, 28);
		int hour = createRandomInteger(0, 23);
		int minute = createRandomInteger(0, 59);
		int second = createRandomInteger(0, 59);
		BigDecimal millisecond = BigDecimal.valueOf(createRandomDouble(0, 1));
		int timezone = createRandomInteger(-14, 14);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(year, month, day, hour, minute, second, millisecond, timezone);
	}

	private byte createRandomByte() {
		return (byte) createRandomInteger(Byte.MIN_VALUE, Byte.MAX_VALUE);
	}
	
	private byte[] createRandomByteArray() {
		return createRandomByteArray(Byte.MIN_VALUE, Byte.MAX_VALUE);
	}
	
	private byte[] createRandomByteArray(int min, int max) {
		int length = 0;
		if (createRandomBoolean())
			length = (int) Math.round(Math.random()*10000);
		byte[] b = new byte[length];
		for (; length > 0;) {
			byte by = (byte) createRandomInteger(min, max);
			b[--length] = by;
		}
		return b;
	}
	
	private String createRandomString() {
		byte b[] = createRandomByteArray(' ', '~');
		return new String(b);
	}

	private boolean createRandomBoolean() {
		return Math.random() < 0.5;
	}
}
