/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;

import org.evolvis.scio.soap.SoapConstants;


public interface MySoapConstants extends SoapConstants {

	
	public static final String NAMESPACE_FAULT = "http://www.evolvis.org/scio/soap/faults";
	public static final String ELEMENT_NAME_FAULT_DETAILS = "faultDetails";
	public static final String ELEMENT_NAME_USERNAME = "username";
	
	
	public static enum SoapFaultSubCode {
		
		LOGIN_ERROR (SoapFaultCode.SENDER, "LoginError"),
		LOGIN_ERROR_ILLEGAL_USERNAME (LOGIN_ERROR, "IllegalUsername");
		
		/** The identifier of this fault sub code */
		private final String id;
		/** The parent fault sub code. If it is specified parentCode must be null */
		private final SoapFaultSubCode parentSubCode;
		/** The parent fault code. If it is specified parentSubCode must be null */
		private final SoapFaultCode parentCode;
		
		private SoapFaultSubCode(SoapFaultSubCode parent, String id) {
			parentCode = null;
			this.parentSubCode = parent;
			this.id = id;
		}
		
		private SoapFaultSubCode(SoapFaultCode parent, String id) {
			this.parentCode = parent;
			parentSubCode = null;
			this.id = id;
		}
		
		public boolean isRootSubCode() {
			return parentCode != null;
		}
		
		public SoapFaultSubCode getParentSubCode() {
			return parentSubCode;
		}
		
		public SoapFaultCode getCode() {
			return parentCode != null ? parentCode : parentSubCode.getCode();
		}
		
		public List<QName> getSubCodeList() {
			List<QName> subCodeList = isRootSubCode() ? new LinkedList<QName>() : parentSubCode.getSubCodeList();
			subCodeList.add(new QName(NAMESPACE_FAULT, id));
			return subCodeList;
		}

		private static SoapFaultSubCode valueOfId(SoapFaultCode code, List<QName> subCodeList, int untilIndex) {
			if (code == null || subCodeList == null || subCodeList.isEmpty() || untilIndex < 0)
				return null;
			QName last = subCodeList.get(untilIndex);//remove(subCodeList.size()-1);
			if (untilIndex == 0) { // root subcode
				for (SoapFaultSubCode subCode : values())
					if (code.equals(subCode.parentCode) && 
							subCode.id.equals(last.getLocalPart()) &&
							NAMESPACE_FAULT.equals(last.getNamespaceURI()))
						return subCode;
			} else {                     // not root subcode
				SoapFaultSubCode parent = valueOfId(code, subCodeList, untilIndex - 1);
				if (parent != null)
					for (SoapFaultSubCode subCode : values())
						if (parent.equals(subCode.parentSubCode) && 
								subCode.id.equals(last.getLocalPart()) &&
								NAMESPACE_FAULT.equals(last.getNamespaceURI()))
							return subCode;
			}
			return null;
		}

		public static SoapFaultSubCode valueOfId(SoapFaultCode code, List<QName> subCodeList) {
			return valueOfId(code, subCodeList, subCodeList.size() - 1);
		}
	}
}
