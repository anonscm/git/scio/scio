/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.util.List;

import javax.xml.namespace.QName;

import org.evolvis.scio.soap.SoapFault;
import org.evolvis.scio.soap.SoapFaultFactory;
import org.evolvis.scio.soap.SoapStreamException;


public class MySoapFaultFactory extends SoapFaultFactory implements MySoapConstants {
	
	@Override
	public SoapFault createSoapFault(SoapFaultCode code, List<QName> subCodeList) throws SoapStreamException {
		SoapFaultSubCode subCode = SoapFaultSubCode.valueOfId(code, subCodeList);
		switch (subCode) {
			case LOGIN_ERROR_ILLEGAL_USERNAME:
				return new LoginError(subCode);
			default:
				throw new SoapStreamException("illegal subcode list "+subCodeList);
		}
	}
}
