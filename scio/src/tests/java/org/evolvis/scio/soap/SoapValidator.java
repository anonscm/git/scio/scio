/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.evolvis.scio.soap.SoapConstants;
import org.xml.sax.SAXException;

/**
 * Validate SOAP messages with the xml schema from
 * http://www.w3.org/2003/05/soap-envelope/.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class SoapValidator {

	private static final String SCHEMA_FILENAME_SOAP = "soap-envelope_05_2003.xsd";
	private static final Logger log = Logger.getLogger(SoapValidator.class.getName());
	private static boolean showException = false;
	
	private SoapValidator() {}
	
	public static final boolean isValid(String soapString) throws SAXException, IOException {
		log.info("Validating SOAP:\n"+soapString);
		SchemaFactory schemaFactory = SchemaFactory.newInstance(SoapConstants.NAMESPACE_XSD);
		Schema schemaGrammar = schemaFactory.newSchema(SoapValidator.class.getResource(SCHEMA_FILENAME_SOAP));
		Validator schemaValidator = schemaGrammar.newValidator();
		Source source = new StreamSource(new ByteArrayInputStream(soapString.getBytes()));
		try {
			schemaValidator.validate(source);
		} catch (SAXException e) {
			if (showException)
				log.log(Level.INFO, "SOAP is invalid", e);
			else
				log.log(Level.INFO, "SOAP is invalid");
			return false;
		}
		return true;
	}
	
	public static final class OutputStream extends java.io.OutputStream {
		
		private StringBuilder sb = new StringBuilder();
		private java.io.OutputStream pipe;
		
		public OutputStream(java.io.OutputStream pipe) {
			this.pipe = pipe;
		}
		
		public OutputStream() {
			this(null);
		}

		@Override
		public void write(int b) throws IOException {
			sb.append((char) b);
			if (pipe != null)
				pipe.write(b);
		}
		
		public boolean isValid() throws SAXException, IOException {
			return SoapValidator.isValid(sb.toString());
		}
	}
	
	public class InputStream extends java.io.InputStream {
		
		private StringBuilder sb = new StringBuilder();
		private java.io.InputStream pipe;
		
		public InputStream(java.io.InputStream pipe) {
			this.pipe = pipe;
		}
		
		public boolean isValid() throws SAXException, IOException {
			return SoapValidator.isValid(sb.toString());
		}

		@Override
		public int read() throws IOException {
			int b = pipe.read();
			if (b != -1)
				sb.append((char) b);
			return b;
		}
	}
}
