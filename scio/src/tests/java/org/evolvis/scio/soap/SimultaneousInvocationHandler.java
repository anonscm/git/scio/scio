/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;


/**
 * Some client stubs with the same service class can be grouped together
 * with this class. If a method of this class is called, the same method
 * will be called for all clients and it will be checked if all results are eaqual.   
 *
 * @param <T>
 */
class SimultaneousInvocationHandler<T> implements InvocationHandler {
	
	private List<T> clients = new LinkedList<T>();
	
	public SimultaneousInvocationHandler(T... clients) {
		for (T client : clients)
			addClient(client);
	}
	
	public void addClient(T client) {
		clients.add(client);
	}
	
	public boolean checkEqual(Object objA, Object objB) {
		if (objA != null)
			return objA.equals(objB);
		else
			return false;
	}
	
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (clients.size() == 0)
			throw new RuntimeException("no clients available");
		System.out.println(clients.get(0)+":::"+ args);
		Object result = method.invoke(clients.get(0), args);
		for (int i = 1; i < clients.size(); i++) {
			
			Object result2 = method.invoke(clients.get(i), args);
			//check if all results are equal
			if (result == null)
				if (result2 == null)
					continue;
				else
					throw new RuntimeException("different results : null != '"+result2+"'");
			if (result instanceof byte[]) { //result is a byte array => equals method will fail because of different pointers
				byte[] ba = (byte[]) result;
				byte[] bb = (byte[]) result2;
				for (int j=0; j<ba.length; j++)
					if (ba[j] != bb[j])
						throw new RuntimeException("different values at array index "+j);
			} else if (!checkEqual(result, result2))
				throw new RuntimeException("different results : '"+result+"' and '"+result2+"' or 'equals'-method is unimplemented");
		}
		return result;
	}
}