/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.testtypes;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement
public class MyPerson {
	String name = "dummy";
	String given = "dummy";	
	Integer age = -1;
	String jobPosition = "dummy";
	
	List addresses = new ArrayList();

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlAttribute
	public String getGiven() {
		return given;
	}

	public void setGiven(String given) {
		this.given = given;
	}


	@XmlElement(name="alter", nillable=true, required=false)
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer i) {
		this.age = i;
	}

	public List getAddresses() {
		return addresses;
	}

	public void setAddresses(List addresses) {
		this.addresses = addresses;
	}

	public void setAddress(Address address) {
		addresses.add(address);
	}
	
	public String toString() {
		return "Person(name="+name
		+", given="+given
		+", age="+age		
		+", addresses="+addresses
		+", jobPosition="+jobPosition
		+")";		
	}

	@XmlElement(name="jobPosition", nillable=false, required=false)
	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof MyPerson))
			return false;			

		MyPerson other = (MyPerson)o;
		return nullOrEquals(getName(), other.getName()) 
		||  nullOrEquals(getGiven(), other.getGiven())
		||  nullOrEquals(getAge(), other.getAge())
		||  nullOrEquals(getJobPosition(), other.getJobPosition())
		||  nullOrEquals(getAddresses(), other.getAddresses());
	}

	private boolean nullOrEquals(Object o1, Object o2) {		
		return (o1 == null && o2 == null) || (o1 != null && o1.equals(o2));
	}
}
