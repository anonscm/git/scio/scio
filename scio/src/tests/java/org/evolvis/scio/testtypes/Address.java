/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.testtypes;

public class Address {
	String type;
	String street;
	String zipcode;
	String city;

	public Address() {
	
	}
	public Address(String type, String street, String zipcode, String city) {
		this.type = type;
		this.street = street;
		this.zipcode = zipcode;
		this.city = city;		
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	public String toString() {
		return "Address(type="+type
		+", street="+street
		+", zipcode="+zipcode		
		+", city="+city
		+")";		
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof MyPerson))
			return false;			

		Address other = (Address)o;
		return nullOrEquals(getType(), other.getType()) 
		||  nullOrEquals(getStreet(), other.getStreet())
		||  nullOrEquals(getZipcode(), other.getZipcode())
		||  nullOrEquals(getCity(), other.getCity());
	}

	private boolean nullOrEquals(Object o1, Object o2) {		
		return (o1 == null && o2 == null) || (o1 != null && o1.equals(o2));
	}	
}
