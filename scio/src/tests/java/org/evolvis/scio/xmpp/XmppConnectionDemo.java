/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.xmpp;

import java.io.IOException;
import java.net.UnknownHostException;

import org.evolvis.scio.messages.TextMessage;


public class XmppConnectionDemo {

	/**
	 * @param args
	 * @throws Exception 
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws Exception {

		TextMessage msg = new TextMessage("Hallo Welt");

		String host = args.length > 0 ? args[0] : "127.0.0.1";
//		XMPPConnection con = new XMPPConnection("sebmob", host, 5222);
//		con.setUsername("test2");
//		con.setPassword("test");
//		msg.setTo("test@sebmob");
		
		//XMPPConnection con = new XMPPConnection("priamus.tarent.de", "priamus.tarent.de", 5222);
		XMPPConnection con = new XMPPConnection("priamus.tarent.de", "priamus.tarent.de", 5222);
		con.setUsername("");
		con.setPassword("");
		msg.setTo("smanck@priamus.tarent.de");

		
		con.connect();
		msg.setFrom(con.getJid());
		con.queryRoster();
		
		con.sendMessage(msg);

		Thread.sleep(30000);
		System.out.println("close now");
		//con.close();
	}

}
