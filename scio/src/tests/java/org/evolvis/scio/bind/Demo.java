/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.bind;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.evolvis.scio.testtypes.Address;
import org.evolvis.scio.testtypes.MyPerson;

public class Demo {

	public static void main(String[] args ) throws Exception {
		test();
		test();
		test();
		test();
		test();
	}
	
	public static void test() throws Exception {
		System.out.println();
		System.out.println();
		BindContext cntx;
		File tmpfile;

		long start = System.currentTimeMillis();
		cntx = BindContext.newInstance("org.evolvis.scio.testtypes");
		System.out.println("created context: "+ (System.currentTimeMillis()-start));
		
		tmpfile = File.createTempFile("JunitTest", ".xml");

		MyPerson p = new MyPerson();
		p.setName("Maus");
		p.setGiven("Micky");
		p.setAge(10);
		List l = new ArrayList();
		l.add(new Address("home", "Wacholderweg 10", "53127", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		p.setAddresses(l);
		
		Marshaller m = cntx.createMarshaller();
		start = System.currentTimeMillis();
		m.marshal(p, tmpfile);
		System.out.println("marshalled: "+ (System.currentTimeMillis()-start));
		
		//m.marshal(p, System.out);		
		
		//System.out.println();
		//System.out.println("reading in again");
		Unmarshaller u = cntx.createUnarshaller();
		start = System.currentTimeMillis();
		MyPerson pIn = (MyPerson)u.unmarshal(tmpfile);
		System.out.println("un marshalled: "+ (System.currentTimeMillis()-start));
		//System.out.println(pIn);
	}
}
