/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */


package org.evolvis.scio.bind;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.namespace.QName;

import junit.framework.TestCase;

import org.evolvis.scio.testtypes.Address;
import org.evolvis.scio.testtypes.MyPerson;


public class MarshallerTest extends TestCase {

	BindContext cntx;
	File tmpfile;
	
	protected void setUp() throws Exception {
		super.setUp();
		cntx = BindContext.newInstance("org.evolvis.scio.testtypes");
		tmpfile = File.createTempFile("JunitTest", ".xml");
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testPerson() throws Exception {
		MyPerson p = createTestPerson();
		
		cntx.createMarshaller().marshal(p, tmpfile);;

		JAXBContext jaxbCntx = JAXBContext.newInstance( "org.evolvis.scio.testtypes" );
		javax.xml.bind.Unmarshaller jaxbUnmarshaller = jaxbCntx.createUnmarshaller();
		MyPerson pNew = (MyPerson)jaxbUnmarshaller.unmarshal(tmpfile);
		//System.out.println(pNew);
		assertEquals("jaxb roundtrip", p, pNew);
	}
	
	public void testNullHandling() throws Exception {
		MyPerson p = new MyPerson();
		p.setName(null);
		p.setGiven("Micky");
		p.setAge(null);
		p.setJobPosition(null);
		List l = new ArrayList();
		l.add(null);
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		p.setAddresses(l);
		
		cntx.createMarshaller().marshal(p, tmpfile);

		JAXBContext jaxbCntx = JAXBContext.newInstance( "org.evolvis.scio.testtypes" );
		javax.xml.bind.Unmarshaller jaxbUnmarshaller = jaxbCntx.createUnmarshaller();
		MyPerson pNew = (MyPerson)jaxbUnmarshaller.unmarshal(tmpfile);
		
		assertEquals("attribute name is default", "dummy", pNew.getName());
		assertEquals("age is set to null, explicitely", null, pNew.getAge());
		assertEquals("first list element exists, but is null", null, pNew.getAddresses().get(0));
		assertEquals("element jobPosition is default", "dummy", pNew.getJobPosition());	
	}
	
	public void testConfigurationByHand() throws Exception {
		MyPerson p = createTestPerson();
		BindContext handmadeContext = BindContext.newInstance();
		MarshallingInfo myPersonInfo = new MarshallingInfo(new QName("myNs", "myPerson"),
														   new QName("myNs", "myPerson"),
														   MyPerson.class,
														   true);
		myPersonInfo.addAttribute("name", String.class);
		myPersonInfo.addAttribute("given", String.class);
		MarshallingElement ageElement = new MarshallingElement();
		ageElement.beanPropertyName = "age";
		ageElement.getMethod = MyPerson.class.getMethod("getAge");
		ageElement.setMethod = MyPerson.class.getMethod("setAge", Integer.class);
		ageElement.isPrimitiveType = true;
		ageElement.name = "alter";
		ageElement.namespaceName = "myNs";
		myPersonInfo.addProperty(ageElement);
		myPersonInfo.addElement("addresses", List.class);
		
		handmadeContext.addMarshallingInfo(myPersonInfo);
		
		MarshallingInfo address = new MarshallingInfo(new QName("myNs", "Address"),
				   									  new QName("myNs", "Address"),
				   									  Address.class,
				   									  false);
		address.addElement("type", String.class);
		address.addElement("street", String.class);
		address.addElement("zipcode", String.class);
		address.addElement("city", String.class);
		handmadeContext.addMarshallingInfo(address);

		cntx.createMarshaller().marshal(p, tmpfile);

		JAXBContext jaxbCntx = JAXBContext.newInstance( "org.evolvis.scio.testtypes" );
		javax.xml.bind.Unmarshaller jaxbUnmarshaller = jaxbCntx.createUnmarshaller();
		MyPerson pNew = (MyPerson)jaxbUnmarshaller.unmarshal(tmpfile);
		assertEquals("jaxb roundtrip", p, pNew);
	}


	/**
	 * @return
	 */
	private MyPerson createTestPerson() {
		MyPerson p = new MyPerson();
		p.setName("Maus");
		p.setGiven("Micky");
		p.setAge(10);
		List l = new ArrayList();
		l.add(new Address("home", "Wacholderweg 10", "53127", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		p.setAddresses(l);
		return p;
	}

}