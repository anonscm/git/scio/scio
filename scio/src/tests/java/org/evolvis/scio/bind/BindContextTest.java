/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.bind;

import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import junit.framework.TestCase;

import org.evolvis.scio.testtypes.Address;
import org.evolvis.scio.testtypes.MyPerson;
import org.evolvis.scio.util.XMLUtil;

public class BindContextTest extends TestCase {

	BindContext cntx;
	
	protected void setUp() throws Exception {
		super.setUp();
		cntx = BindContext.newInstance("org.evolvis.scio.testtypes");
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testClassArrayContructor() throws JAXBException {
		cntx = BindContext.newInstance(MyPerson.class, Address.class);		
	}

	public void testGetInfoClass() throws SecurityException, NoSuchMethodException {
		MarshallingInfo personInfo = cntx.getInfo(MyPerson.class);
		assertEquals("target class", MyPerson.class, personInfo.targetClass);
		assertEquals("target type", new QName("myNS", "myPerson"), personInfo.targetType);
		assertEquals("name", "myPerson", personInfo.name);
		assertEquals("namespace", "myNS", personInfo.namespace);
		assertEquals("isXmlRootType", true, personInfo.isXMLRootType);
		
//		for (MarshallingElement elem : personInfo.properties) {
//			System.out.println("beanName: " +elem.beanPropertyName +" xmlName: "+elem.name);			
//		}
		assertEquals("found all properties", 5, personInfo.properties.size());

		Iterator<MarshallingElement> attributes = personInfo.getAttributeProperties();
		attributes.next().name.equals("name");
		attributes.next().name.equals("given");
		if (attributes.hasNext())
			fail("no more elements expected, but found "+ attributes.next().name);
		
		Iterator<MarshallingElement> elements = personInfo.getElementProperties();
		elements.next().name.equals("alter");
		elements.next().name.equals("addresses");
		elements.next().name.equals("jobPosition");
		if (elements.hasNext())
			fail("no more elements expected, but found "+ elements.next().name);		

		MarshallingElement nameElement = personInfo.getPropertyByBeanName("name");
		
		assertEquals("nameElement: name", "name", nameElement.name);
		assertEquals("nameElement: beanPropertyName", "name", nameElement.beanPropertyName);
		assertEquals("nameElement: namespace", "myNS", nameElement.namespaceName);
		assertEquals("nameElement: primitiveType", XMLUtil.XSD_STRING, nameElement.primitiveType);
		assertEquals("nameElement: isPrimitiveType", true, nameElement.isPrimitiveType);
		assertEquals("nameElement: nillable", false, nameElement.nillable);
		assertEquals("nameElement: encodeAsAttribute", true, nameElement.encodeAsAttribute);
		assertEquals("nameElement: required", false, nameElement.required);
		assertEquals("nameElement: simpleContent", true, nameElement.simpleContent);
		assertEquals("nameElement: defaultValue", null, nameElement.defaultValue);
		assertEquals("nameElement: getMethod", MyPerson.class.getMethod("getName", new Class[]{}), nameElement.getMethod);
		assertEquals("nameElement: setMethod", MyPerson.class.getMethod("setName", new Class[]{String.class}), nameElement.setMethod);
		assertEquals("nameElement: targetJavaClass", String.class, nameElement.javaPropertyType);		

		MarshallingElement ageElement = personInfo.getPropertyByBeanName("age");
		
		assertEquals("ageElement: name", "alter", ageElement.name);
		assertEquals("ageElement: beanPropertyName", "age", ageElement.beanPropertyName);
		assertEquals("ageElement: namespace", "myNS", ageElement.namespaceName);
		assertEquals("ageElement: primitiveType", XMLUtil.XSD_INTEGER, ageElement.primitiveType);
		assertEquals("ageElement: isPrimitiveType", true, ageElement.isPrimitiveType);
		assertEquals("ageElement: nillable", true, ageElement.nillable);
		assertEquals("ageElement: encodeAsAttribute", false, ageElement.encodeAsAttribute);
		assertEquals("ageElement: required", false, ageElement.required);
		assertEquals("ageElement: simpleContent", true, ageElement.simpleContent);
		assertEquals("ageElement: defaultValue", null, ageElement.defaultValue);
		assertEquals("ageElement: getMethod", MyPerson.class.getMethod("getAge", new Class[]{}), ageElement.getMethod);
		assertEquals("ageElement: setMethod", MyPerson.class.getMethod("setAge", new Class[]{Integer.class}), ageElement.setMethod);
		assertEquals("ageElement: targetJavaClass", Integer.class, ageElement.javaPropertyType);		

		MarshallingElement addressesElement = personInfo.getPropertyByBeanName("addresses");
		
		assertEquals("addressesElement: name", "addresses", addressesElement.name);
		assertEquals("addressesElement: isListType", true, addressesElement.isListType());
		assertEquals("addressesElement: beanPropertyName", "addresses", addressesElement.beanPropertyName);
		assertEquals("addressesElement: namespace", "myNS", addressesElement.namespaceName);
		assertEquals("addressesElement: primitiveType", null, addressesElement.primitiveType);
		assertEquals("addressesElement: isPrimitiveType", false, addressesElement.isPrimitiveType);
		assertEquals("addressesElement: nillable", false, addressesElement.nillable);
		assertEquals("addressesElement: encodeAsAttribute", false, addressesElement.encodeAsAttribute);
		assertEquals("addressesElement: required", false, addressesElement.required);
		assertEquals("addressesElement: simpleContent", false, addressesElement.simpleContent);
		assertEquals("addressesElement: defaultValue", null, addressesElement.defaultValue);
		assertEquals("addressesElement: getMethod", MyPerson.class.getMethod("getAddresses", new Class[]{}), addressesElement.getMethod);
		assertEquals("addressesElement: setMethod", MyPerson.class.getMethod("setAddresses", new Class[]{List.class}), addressesElement.setMethod);
		assertEquals("addressesElement: targetJavaClass", List.class, addressesElement.javaPropertyType);		
		
	}

	public void testPropertyAccessorMethods() throws Exception {
		MarshallingInfo personInfo = cntx.getInfo(MyPerson.class);

		assertTrue("got attribute for name", personInfo.getAttributePropertyByXMLName(new QName(null, "name")) != null);
	}
}