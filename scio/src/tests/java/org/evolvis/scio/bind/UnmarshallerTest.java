/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */


package org.evolvis.scio.bind;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import junit.framework.TestCase;

import org.evolvis.scio.testtypes.Address;
import org.evolvis.scio.testtypes.MyPerson;
import org.evolvis.scio.testtypes.alfresco.AlfrescoTicket;

public class UnmarshallerTest extends TestCase {

	BindContext cntx;
	File tmpfile;
	
	protected void setUp() throws Exception {
		super.setUp();
		cntx = BindContext.newInstance("org.evolvis.scio.testtypes:org.evolvis.scio.testtypes.alfresco");
		tmpfile = File.createTempFile("JunitTest", ".xml");
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testPerson() throws Exception {
		MyPerson p = new MyPerson();
		p.setName("Maus");
		p.setGiven("Micky");
		p.setAge(10);
		List l = new ArrayList();
		l.add(new Address("home", "Wacholderweg 10", "53127", "Bonn"));
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		p.setAddresses(l);

		JAXBContext cntxJAXB = JAXBContext.newInstance( "org.evolvis.scio.testtypes" );
		Marshaller m = cntxJAXB.createMarshaller();
		m.setProperty("jaxb.formatted.output", true);
		FileOutputStream fis = new FileOutputStream(tmpfile);
		m.marshal(p, fis);
		fis.close();
		
		MyPerson pNew = (MyPerson) cntx.createUnarshaller().unmarshal(tmpfile);
		assertEquals("jaxb roundtrip", p, pNew);
	}
	
	public void testNulls() throws Exception {
		MyPerson p = new MyPerson();
		p.setName(null);
		p.setGiven("Micky");
		p.setAge(null);
		p.setJobPosition(null);
		List l = new ArrayList();
		l.add(null);
		l.add(new Address("work", "Heiulsbachstraße 24", "53123", "Bonn"));
		p.setAddresses(l);

		JAXBContext cntxJAXB = JAXBContext.newInstance( "org.evolvis.scio.testtypes" );
		Marshaller m = cntxJAXB.createMarshaller();
		m.setProperty("jaxb.formatted.output", true);
		FileOutputStream fis = new FileOutputStream(tmpfile);
		m.marshal(p, fis);
		fis.close();
		
		MyPerson pNew = (MyPerson) cntx.createUnarshaller().unmarshal(tmpfile);
		
		assertEquals("attribute name is default", "dummy", pNew.getName());
		assertEquals("age is set to null, explicitely", null, pNew.getAge());
		assertEquals("first list element exists, but is null", null, pNew.getAddresses().get(0));
		assertEquals("element jobPosition is default", "dummy", pNew.getJobPosition());	
	}
	
	public void testEmptyElement() throws XMLStreamException, FactoryConfigurationError, JAXBException {
		String xml = "<myPerson name=\"TestName\" xmlns=\"myNS\"/>";
		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));
		MyPerson pNew = (MyPerson) cntx.createUnarshaller().unmarshal(reader);
		
		assertEquals("attribute name is default", "TestName", pNew.getName());
	}
	
	public void testAlfrescoTicket() throws Exception {
		
		String xml = "<?xml version='1.0' encoding='UTF-8'?>"
					+"<alfrescoTicket xmlns:ns1=\"org.evolvis.scio.testtypes.alfresco\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns1:alfrescoTicket\">"
					+"  <ticket xsi:type=\"ns1:alfrescoTicket\">TICKET_8254a6498ce63bfc01509fadaafc7b177bee0d39</ticket>"
					+"</alfrescoTicket>";

		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));
		AlfrescoTicket ticket = (AlfrescoTicket) cntx.createUnarshaller().unmarshal(reader);
		
		assertEquals("attribute ticket is correct", "TICKET_8254a6498ce63bfc01509fadaafc7b177bee0d39", ticket.getTicket());
	}
}