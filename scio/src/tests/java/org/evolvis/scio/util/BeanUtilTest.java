/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.util;

import junit.framework.TestCase;


public class BeanUtilTest extends TestCase {

	public void testEqualsObjectObject() {
		assertTrue(BeanUtil.equals(null, null));
		assertFalse(BeanUtil.equals(null, "test123"));
		assertFalse(BeanUtil.equals("test123", null));
		assertTrue(BeanUtil.equals("test123", "test123"));
		assertFalse(BeanUtil.equals("test123", "bla"));
		assertTrue(BeanUtil.equals(1, 1));
		assertFalse(BeanUtil.equals(1, 2));
		assertTrue(BeanUtil.equals(true, true));
		assertFalse(BeanUtil.equals(false, true));
		assertFalse(BeanUtil.equals("bla", true));
		assertTrue(BeanUtil.equals(
				new BeanUtilTestObject("a", 3, true),
				new BeanUtilTestObject("a", 3, true))); 
		assertFalse(BeanUtil.equals(
				new BeanUtilTestObject("a", 3, true),
				new BeanUtilTestObject("b", 3, true))); 
		assertFalse(BeanUtil.equals(
				new BeanUtilTestObject("a", 2, true),
				new BeanUtilTestObject("a", 3, true))); 
		assertFalse(BeanUtil.equals(
				new BeanUtilTestObject("a", 3, false),
				new BeanUtilTestObject("a", 3, true))); 
	}
}
