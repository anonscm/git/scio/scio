/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.util;

import java.io.ByteArrayInputStream;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import junit.framework.TestCase;

public class XMLSubtreeReaderTest extends TestCase {

	XMLStreamReader reader;
	
	protected void setUp() throws Exception {
		super.setUp();
		String xml = "<persons><person id=\"123\"><name>Dagobert Duck</name> xxyyzz <city>Entenhausen</city></person></persons>";
		reader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));	
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testRobustHandling() throws XMLStreamException {
		reader.nextTag();
		reader.nextTag();
		assertEquals("on tag person", "person", reader.getLocalName());
		
		XMLSubtreeReader personSubTree = new XMLSubtreeReader(reader);
		assertEquals("on tag person", "person", personSubTree.getLocalName());
		personSubTree.nextTag();
		assertEquals("on tag name", "name", personSubTree.getLocalName());
		assertEquals("correct text", "Dagobert Duck", personSubTree.getElementText());
		int event;
		do {
			event = personSubTree.next();
		} while (event != XMLStreamConstants.END_DOCUMENT);
		
		try {
			personSubTree.next();
			fail("exception estimated, because of end of document");
		} catch (XMLStreamException e) {
			// this should be reached
		}
		
		personSubTree.close();
		assertEquals("on tag person", "person", reader.getLocalName());
		assertTrue("on tag person", reader.isEndElement());

		reader.nextTag();
		assertEquals("on tag persons", "persons", reader.getLocalName());		
	}

	
	public void testSkip() throws XMLStreamException {
		reader.nextTag();
		reader.nextTag();
		assertEquals("on tag person", "person", reader.getLocalName());
		
		XMLSubtreeReader personSubTree = new XMLSubtreeReader(reader);
		personSubTree.skipSubTree();
		
		assertEquals("on tag person", "person", reader.getLocalName());
		assertTrue("on tag person", reader.isEndElement());
	}
	
	public void testMinimal() throws XMLStreamException, FactoryConfigurationError {
		String xml = "<persons/>";
		reader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));
		reader.nextTag();
		XMLSubtreeReader personSubTree = new XMLSubtreeReader(reader);
		personSubTree.skipSubTree();
		assertEquals("on tag persons", "persons", reader.getLocalName());
	}

	public void testMinimal2() throws XMLStreamException, FactoryConfigurationError {
		String xml = "<persons><person/></persons>";
		reader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));
		reader.nextTag();
		reader.nextTag();
		XMLSubtreeReader personSubTree = new XMLSubtreeReader(reader);
		int event;
		do {
			event = personSubTree.next();
		} while (event != XMLStreamConstants.END_DOCUMENT);

		assertEquals("on tag person", "person", reader.getLocalName());
	}

	public void testMinimalCorrectEvents() throws XMLStreamException, FactoryConfigurationError {
		String xml = "<persons/>";
		reader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));
		reader.nextTag();
		XMLSubtreeReader personSubTree = new XMLSubtreeReader(reader);
		
		assertEquals("end element", XMLStreamReader.END_ELEMENT, personSubTree.next());
		assertEquals("end document", XMLStreamReader.END_DOCUMENT, personSubTree.next());
	}
	
	public void testGetEventType() throws XMLStreamException {
		reader.nextTag();
		reader.nextTag();
		XMLSubtreeReader personSubTree = new XMLSubtreeReader(reader);
		assertEquals("on tag person", XMLStreamConstants.START_ELEMENT, personSubTree.getEventType());
		assertEquals("on tag name", XMLStreamConstants.START_ELEMENT, personSubTree.next());
		assertEquals(XMLStreamConstants.START_ELEMENT, personSubTree.getEventType());
		assertEquals(new QName("", "name"), personSubTree.getName());
		assertEquals(XMLStreamConstants.CHARACTERS, personSubTree.next());
		assertEquals(XMLStreamConstants.CHARACTERS, personSubTree.getEventType());
		assertEquals("on tag name", XMLStreamConstants.END_ELEMENT, personSubTree.next());
		assertEquals(XMLStreamConstants.END_ELEMENT, personSubTree.getEventType());
		assertEquals(XMLStreamConstants.CHARACTERS, personSubTree.next());
		assertEquals(XMLStreamConstants.CHARACTERS, personSubTree.getEventType());
		assertEquals("on tag city", XMLStreamConstants.START_ELEMENT, personSubTree.next());
		assertEquals(XMLStreamConstants.START_ELEMENT, personSubTree.getEventType());
		assertEquals(new QName("", "city"), personSubTree.getName());
		assertEquals(XMLStreamConstants.CHARACTERS, personSubTree.next());
		assertEquals(XMLStreamConstants.CHARACTERS, personSubTree.getEventType());
		assertEquals("on tag city", XMLStreamConstants.END_ELEMENT, personSubTree.next());
		assertEquals(XMLStreamConstants.END_ELEMENT, personSubTree.getEventType());
		assertEquals("on tag person", XMLStreamConstants.END_ELEMENT, personSubTree.next());
		assertEquals(XMLStreamConstants.END_ELEMENT, personSubTree.getEventType());
		assertEquals(XMLStreamConstants.END_DOCUMENT, personSubTree.next());
		assertEquals(XMLStreamConstants.END_DOCUMENT, personSubTree.getEventType());
	}
}
