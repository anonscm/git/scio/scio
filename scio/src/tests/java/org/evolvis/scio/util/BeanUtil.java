/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.util;

import java.lang.reflect.Method;

public class BeanUtil {

	/**
	 * Checks if two beans are equal or null
	 * 
	 * @param objA
	 * @param objB
	 * @return
	 */
	public static boolean equals(Object objA, Object objB) {
		// assure that both are not null
		if (objA == null)
			return objB == null;
		if (objB == null)
			return false;
		// check if same type
		if (!objA.getClass().equals(objB.getClass()))
			return false;
		// check if equal
		String packagename = objA.getClass().getPackage().getName();
		// use Object.equals-Method if package starts with "java" or "com.sun"
		if (packagename.startsWith("java") || packagename.startsWith("com.sun"))
			return objA.equals(objB);
		// check return of all getter methods
		boolean isBean = false;
		Method[] methods = objA.getClass().getMethods();
		for (Method method : methods) {
			String name = method.getName();
			if ((name.startsWith("get") && ! name.equals("getClass")) ||
					name.startsWith("is")) {
				isBean = true;
				try {
					if (! equals(method.invoke(objA), method.invoke(objB)))
						return false;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		return isBean ? true : objA.equals(objB); 
	}
}
