/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.util;

import java.io.IOException;
import java.io.OutputStream;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import junit.framework.TestCase;

/**
 * @author Hendrik Helwich, tarent GmbH
 */
public class XMLSubtreeWriterTest extends TestCase {
	
	public void testCommonUsecase() throws XMLStreamException, FactoryConfigurationError {
		XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(new OutputStream() {
			final String out = "<?xml version='1.0' encoding='UTF-8'?><elem1><elem2><elem3><elem4><elem5>abc</elem5></elem4><elem6><elem7 att1=\"att1value\"><elem8 /></elem7></elem6></elem3></elem2></elem1>";
			int i = 0;
			@Override
			//TODO whitespaces are not ignored, etc...
			public void write(int b) throws IOException {
				assertEquals("different char at position: "+i, (char)b, out.charAt(i++));
			}});
		writer.writeStartDocument();
		writer.writeStartElement("elem1");
		writer.writeStartElement("elem2");
		writer.writeStartElement("elem3");
		XMLSubtreeWriter writer2 = new XMLSubtreeWriter(writer);
		assertEquals("test initial depth", writer2.getSubtreeDepth(), 0);
		try {
			writer2.writeEndElement();
			fail("operation should throw exception");
		} catch (XMLStreamException e) {}
		writer2.writeStartElement("elem4");
		assertEquals("test depth", writer2.getSubtreeDepth(), 1);
		writer2.writeStartElement("elem5");
		assertEquals("test depth", writer2.getSubtreeDepth(), 2);
		writer2.writeCharacters("abc");
		writer2.writeAllSubtreeEndElements();
		assertEquals("test depth", writer2.getSubtreeDepth(), 0);
		writer2.writeStartElement("elem6");
		assertEquals("test depth", writer2.getSubtreeDepth(), 1);
		writer2.writeStartElement("elem7");
		assertEquals("test depth", writer2.getSubtreeDepth(), 2);
		writer2.writeAttribute("att1", "att1value");
		writer2.writeEmptyElement("elem8");
		assertEquals("test depth", writer2.getSubtreeDepth(), 2);
		writer2.close();
		assertEquals("should be root level", writer2.getSubtreeDepth(), 0);
		try {
			writer2.writeEndElement();
			fail("operation should throw exception");
		} catch (XMLStreamException e) {}
		assertEquals("test depth", writer2.getSubtreeDepth(), 0);
		writer.writeEndElement();
		writer.writeEndElement();
		writer.writeEndElement();
		try {
			writer2.writeEndElement();
			fail("operation should throw exception");
		} catch (XMLStreamException e) {}
		writer.close();
	}
}
