/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import junit.framework.TestCase;

public class XMLUtilTest extends TestCase {

	private static final String TEST_XML = "test.xml";

	// the following files must contain the specified subtree of the above file
	private static final String TEST_XML_SKIP_0 = "testskip0.xml";
	private static final String TEST_XML_SKIP_1 = "testskip1.xml";
	private static final String TEST_XML_SKIP_2 = "testskip2.xml";

	public void testPipeXmlSubTree() throws XMLStreamException, FactoryConfigurationError, IOException {
		testPipeXmlSubTree(0, TEST_XML_SKIP_0);
		testPipeXmlSubTree(1, TEST_XML_SKIP_1);
		testPipeXmlSubTree(2, TEST_XML_SKIP_2);
	}

	/**
	 * @param  skipTags
	 *         How mutch elements should be skipped?
	 * @param  compareResource
	 *         Name of the resource to which the subtree of the sample xml file should be compared
	 */
	private void testPipeXmlSubTree(int skipTags, String compareResource) throws XMLStreamException, FactoryConfigurationError, IOException {
		// create stax xml reader for the test-xml-file
		InputStream in = getClass().getResourceAsStream(TEST_XML);
		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(in);
		// skip tags
		while (reader.hasNext() && !(reader.next() == XMLStreamConstants.START_ELEMENT && skipTags -- == 0));
		
		// create stax xml writer which demands that its output is equal to the test-xml-file
		final InputStream outin = getClass().getResourceAsStream(compareResource);
		OutputStream out = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				//System.out.print((char) b);
				assertEquals((char) b, (char) outin.read());
			}
		};
		XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(out);
		// copy the xml data from reader to the writer
		XMLUtil.pipeXmlSubTree(reader, writer);
		writer.close();
		assertEquals("test subtree at end", -1, outin.read());
	}
}
