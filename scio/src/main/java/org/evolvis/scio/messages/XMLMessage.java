/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.messages;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.bind.BindContext;
import org.evolvis.scio.bind.Unmarshaller;
import org.evolvis.scio.util.XMLUtil;

/**
 * Message with an XML Subtree as content.
 * This message has to be consumed immediately, because the 
 * XmlStreamReader of the underlaying document will be used directly.
 * For multiple Processing or asynchronous processing, a copy has to
 * be made of the contents.
 * 
 * @autor Sebastian Mancke, tarent GmbH
 */
public class XMLMessage extends BasicMessage {
	
	XMLStreamReader reader;
	Object unmarshalledElement;
	boolean isUnmarshalled = false;
	
	/**
	 * Constructs a new XmlMessage based on an XMLStreamReader
	 * @param reader The reader, positioned on a start tag
	 */
	public XMLMessage(XMLStreamReader reader) {
		if (! reader.isStartElement())
			throw new IllegalArgumentException("the reader has to be positioned on a start tag");
		this.reader = reader;
		setContentType(XMLUtil.getElementType(reader));
	}
	
	public Object getContent() {
		if (! isUnmarshalled) {
		
			// TODO: where to get the context from?
			// it should be cached and type lookup should be performed dynamic
			try {
				BindContext bindContext = BindContext.newInstance("org.evolvis.scio.xmpp");
				Unmarshaller u = bindContext.createUnarshaller();			
				unmarshalledElement = u.unmarshal(reader);
				isUnmarshalled = true;
			} catch (JAXBException e) {

				throw new RuntimeException("error while reading message", e);
			}
		}
		
		return unmarshalledElement;

	}

	public void writeXmlRepresentation(XMLStreamWriter streamWriter) throws XMLStreamException {
		// TODO: implement
		throw new RuntimeException("XmlMessage#writeXmlRepresentation() not implemented yet");
	}
	
	public boolean isStreamingMessage() {
		return true;
	}
	
}
