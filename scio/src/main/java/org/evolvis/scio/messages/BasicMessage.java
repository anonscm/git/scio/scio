/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.messages;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.Message;

/**
 *
 * @autor Sebastian Mancke, tarent GmbH
 */
public class BasicMessage implements Message {

	protected static String NS_XSD = "http://www.w3.org/2001/XMLSchema";
	protected static QName XSD_ANY = new QName(NS_XSD, "any");
	
	private Object content;
	private QName contentType = XSD_ANY;
	private String from;
	private String to;
	private String id;
	private String recallId;
	private String subject;
	private boolean errorMessage = false;
	
	public BasicMessage(QName contentType, Object content) {
		this.contentType = contentType;
		this.content = content;
	}

	public BasicMessage(QName contentType) {
		this.contentType = contentType;
	}

	public BasicMessage() {
	}

	public void writeXmlRepresentation(XMLStreamWriter streamWriter) throws XMLStreamException {
		// TODO: use object-xml mapping here in this base class		
	}
	
	public QName getContentType() {
		return contentType;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRecallId() {
		return recallId;
	}

	public void setRecallId(String recallId) {
		this.recallId = recallId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setContentType(QName contentType) {
		this.contentType = contentType;
	}

	public boolean isStreamingMessage() {
		return false;		
	}

	public void setErrorMessage(boolean isErrorMessage) {
		this.errorMessage = isErrorMessage;
	}
	
	public boolean isErrorMessage() {			
		return errorMessage;
	}
}
