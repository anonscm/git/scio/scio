/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.xmpp;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.Message;
import org.evolvis.scio.xmpp.utils.Base64;

/**
 *
 * @autor Sebastian Mancke, tarent GmbH
 */
public class StreamWriter implements XMPPConstants {

	private OutputStream outputStream;
	private XMLStreamWriter writer;
	
	public StreamWriter() {
	}	
	
	public void setOutputStream(OutputStream outputStream) throws XMLStreamException, FactoryConfigurationError {	
		this.outputStream = outputStream;
		writer = XMLOutputFactory.newInstance().createXMLStreamWriter(outputStream);
	}

	
	public synchronized void openStream(String to, boolean openDocument) throws XMLStreamException {
		if (openDocument)
			writer.writeStartDocument();
		writer.writeStartElement("stream", "stream", "http://etherx.jabber.org/streams");
		writer.writeNamespace("stream", "http://etherx.jabber.org/streams");
		writer.setDefaultNamespace("jabber:client");
		writer.writeNamespace("", "jabber:client");
		writer.writeAttribute("version", "1.0");
		writer.writeAttribute("to", to);
		send();
		//System.out.println("wrote stream open");
	}
			//outputStream.write(				
			//		"<?xml version=\"1.0\"?><stream:stream xmlns:stream=\"http://etherx.jabber.org/streams\" xmlns=\"jabber:client\" version=\"1.0\" to=\"priamus.tarent.de\">\n".getBytes()
			//		);
			//outputStream.flush();
			//outputStream.write(				
		//			"<tls:starttls xmlns:tls=\"urn:ietf:params:xml:ns:xmpp-tls\"/><proceed xmlns=\"urn:ietf:params:xml:ns:xmpp-tls\"/>".getBytes()
		//			);
		//	outputStream.flush();
			
			//outputStream.write(				
			//		"<iq type=\"get\" id=\"auth_1\" to=\"priamus.tarent.de\"><query xmlns=\"jabber:iq:auth\"><username>jabba</username><password>jabbawoki</password></query></iq>".getBytes()
			//		);

	
	public synchronized void startTLS() throws XMLStreamException {
		writeEmptyElement(TLS_START);
		send();
		//System.out.println("wrote tls start");	
	}

	public synchronized void tlsProceed() throws XMLStreamException {
		writeEmptyElement(TLS_PROCEED);
		send();
		//System.out.println("wrote tls proceed");	
	}

	public synchronized void saslPlainAuth(String username, String password) throws XMLStreamException {
		writer.setDefaultNamespace(NS_SASL);
		writer.writeStartElement("auth");
		writer.writeNamespace("", NS_SASL);
		writer.writeAttribute("mechanism", "PLAIN");
		try {
			writer.writeCharacters(Base64.encode(("\0"+username+"\0"+password).getBytes("UTF-8")));
		} catch (UnsupportedEncodingException e) {
			// ignore: utf-8 is there!
		}
		writer.writeEndElement();
		send();
		//System.out.println("wrote sasl plain");		
	}
	

	public synchronized void bind(String msgId, String resource) throws XMLStreamException {
		writer.writeStartElement("iq");
		writer.writeAttribute("type", "set");
		writer.writeAttribute("id", msgId);
		writer.setDefaultNamespace(NS_BIND);		
		writer.writeStartElement("bind");
		writer.writeNamespace("", NS_BIND);
		writer.writeStartElement("resource");
		writer.writeCharacters(resource);
		writer.writeEndElement();
		writer.writeEndElement();
		writer.writeEndElement();
		send();
		//System.out.println("wrote bind");		
	}
	
	public synchronized void session(String msgId, String to) throws XMLStreamException {
		writer.writeStartElement("iq");
		writer.writeAttribute("type", "set");
		writer.writeAttribute("id", msgId);
		writer.writeAttribute("to", to);
		writeEmptyElement(SESSION);
		writer.writeEndElement();
		send();
	}
		
	public synchronized void writeMessage(String jid, Message msg) throws XMLStreamException {
		writer.writeStartElement("message");	
		writer.writeAttribute("from", jid);
		writer.writeAttribute("to", msg.getTo());
		if (msg.getSubject() != null) {
			writer.writeStartElement("body");
			writer.writeCharacters(msg.getSubject());
			writer.writeEndElement();
		}
		writer.writeStartElement("body");
		msg.writeXmlRepresentation(writer);
		writer.writeEndElement();
		writer.writeEndElement();
		send();		
	}
	
	public synchronized void writeIQMessage(String jid, String type, Message msg) throws XMLStreamException {
		writer.writeStartElement("iq");
		writer.writeAttribute("from", jid);
		writer.writeAttribute("to", msg.getTo());
		writer.writeAttribute("type", type);
		msg.writeXmlRepresentation(writer);
		writer.writeEndElement();
		send();		
	}

	public synchronized void writePresence(String from, String to, String show, String status, int priority) throws XMLStreamException {		
		writer.writeStartElement("presence");	
		writer.writeAttribute("from", from);
		writer.writeAttribute("to", to);

		writer.writeStartElement("status");
		writer.writeCharacters(status);
		writer.writeEndElement();

		writer.writeStartElement("show");
		writer.writeCharacters(show);
		writer.writeEndElement();

		writer.writeStartElement("priority");
		writer.writeCharacters(""+priority);
		writer.writeEndElement();

		writer.writeEndElement();
		send();
	}
	
	// TODO: improve in sending only, if nessesary
	public synchronized void writeKeepAlivePackage() throws XMLStreamException {
		writer.writeCharacters(" ");
		writer.flush();
	}

	/**
	 * @throws XMLStreamException
	 */
	private void send() throws XMLStreamException {
		writer.writeCharacters("");
		writer.flush();
	}

	
	private void writeEmptyElement(QName qname) throws XMLStreamException {
		writer.writeStartElement(qname.getPrefix(), qname.getLocalPart(), qname.getNamespaceURI());
		writer.writeNamespace(qname.getPrefix(), qname.getNamespaceURI());
		writer.writeEndElement();
	}
}
