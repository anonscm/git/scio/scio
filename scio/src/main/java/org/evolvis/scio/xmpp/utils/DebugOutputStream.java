/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.xmpp.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


/**
 *
 * @autor Sebastian Mancke, tarent GmbH
 */
public class DebugOutputStream extends OutputStream {

	OutputStream delegate;
	FileOutputStream fos;
	
	public DebugOutputStream(OutputStream delegate, File file) {
		this.delegate = delegate;
		try {
			this.fos = new FileOutputStream(file, true);
		} catch (FileNotFoundException e) {		
			e.printStackTrace();
		}
	}

	@Override
	public void close() throws IOException {
		delegate.close();
		fos.close();
	}

	@Override
	public void flush() throws IOException {
		delegate.flush();
		fos.flush();
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		delegate.write(b, off, len);
		fos.write(b, off, len);
	}

	@Override
	public void write(byte[] b) throws IOException {
		delegate.write(b);
		fos.write(b);
	}

	@Override
	public void write(int b) throws IOException {
		delegate.write(b);
		fos.write(b);		
	}



}
