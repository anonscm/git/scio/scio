/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.xmpp.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @autor Sebastian Mancke, tarent GmbH
 */
public class DebugInputStream extends InputStream {

	InputStream delegate;
	FileOutputStream fos;
	
	public DebugInputStream(InputStream delegate, File logfile) {
		this.delegate = delegate;
		try {
			this.fos = new FileOutputStream(logfile, true);
		} catch (FileNotFoundException e) {		
			e.printStackTrace();
		}
	}
	
	@Override
	public int read() throws IOException {
//		System.out.println("DEBUG: read()");
		int result = delegate.read();
		fos.write(result);
		return result;
	}

	@Override
	public int available() throws IOException {
//		System.out.println("DEBUG: AVAILABLE()");		
		return delegate.available();
	}

	@Override
	public void close() throws IOException {
//		System.out.println("DEBUG: CLOSE()");
		delegate.close();
		fos.close();
	}

	@Override
	public synchronized void mark(int readlimit) {
//		System.out.println("DEBUG: MARK()");		
		// to nothing here
	}

	@Override
	public boolean markSupported() {
		return false;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
//		System.out.println("DEBUG: read(byte[], int, int)");
		
		int count = delegate.read(b, off, len);
		if (count > 0) {
			fos.write(b, off, count);
			fos.flush();
		}
		return count;
	}

	@Override
	public int read(byte[] b) throws IOException {
//		System.out.println("DEBUG: read(byte[])");
		int count = delegate.read(b);
		if (count > 0) {
			fos.write(b, 0, count);
			fos.flush();
		}
		return count;
	}

	@Override
	public synchronized void reset() throws IOException {
//		System.out.println("DEBUG: reset()");
		throw new IOException("mark not supported");
	}

	@Override
	public long skip(long n) throws IOException {
//		System.out.println("DEBUG: skip()");		
		byte[] buff = new byte[(int)n];
		int readCount = delegate.read(buff);
		fos.write(buff, 0, readCount);
		fos.flush();
		return readCount;
	}

}
