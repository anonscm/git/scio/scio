/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.xmpp;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.Message;
import org.evolvis.scio.messages.BasicMessage;
import org.evolvis.scio.security.ScioTrustManager;
import org.evolvis.scio.xmpp.utils.DebugInputStream;
import org.evolvis.scio.xmpp.utils.DebugOutputStream;

/**
 *
 * @autor Sebastian Mancke, tarent GmbH
 */
public class XMPPConnection implements StreamListener, XMPPConstants {
	
	protected String host;
	protected String service;
	protected int port;
	protected String location;	
	protected String username;
	protected String password;
	protected String jid;
	protected String resource = "scioclient";
	protected Roster roster;
	
	Socket socket;
	StreamReader streamReader;
	StreamWriter streamWriter;
	
	int lastId = 0;

	private XMPPFeatures features;
	private boolean usingTLS;
	private boolean debug = false;
	private Timer keepAliveTimer;

	public XMPPConnection() {
	}
	
	public XMPPConnection(String service, String host, int port) {
		this.host = host;
		this.port = port;	
		this.service = service;	
	}

	public void connect() throws UnknownHostException, IOException, XMPPException {
		try {
			socket = new Socket(host, port);

			streamWriter = new StreamWriter();
			streamWriter.setOutputStream(wrapped(socket.getOutputStream()));
			streamWriter.openStream(service, true);

			streamReader = new StreamReader(this);
			streamReader.setInputStream(wrapped(socket.getInputStream()));
			streamReader.readStreamOpening();
			
			// now the features should be set!
			// if tls is supported, we start the tls and set up the xmpp stream again.
			if (features.tlsSupported) {
				debug("STARTING TLS");
				streamWriter.startTLS();
				streamReader.readTLSProceed();				
				startTLS();
				streamWriter.setOutputStream(wrapped(socket.getOutputStream()));
				streamWriter.openStream(service, true);
				streamReader.setInputStream(wrapped(socket.getInputStream()));						
				streamReader.readStreamOpening();	
				debug("STARTING TLS --done");
			} else
				debug("tls not supported by server");				
			
			
			if (!features.saslPLAINSupported)
				throw new XMPPException("no sasl PLAIN authentication supported by server");
			
			debug("STARTING SASL PLAIN");
			//debug("username: "+getUsername() +" password: "+getPassword());
			streamWriter.saslPlainAuth(getUsername(), getPassword());
			streamReader.readSaslSuccess();
			streamWriter.setOutputStream(wrapped(socket.getOutputStream()));
			streamWriter.openStream(service, true);
			streamReader.setInputStream(wrapped(socket.getInputStream()));									
			streamReader.readStreamOpening();
			debug("STARTING SASL PLAIN --done");
			
			if (features.bindSupported) {
				debug("STARTING BIND");						
				String id = genId("bind");
				streamWriter.bind(id, resource);
				jid = streamReader.readBindResult(id);
				debug("STARTING BIND --done");
			} 
			else			
				debug("bind not supported by server");
			
			
			if (features.sessionSupported) {
				debug("STARTING SESSION");						
				streamWriter.session(genId("session"), service);
				//streamReader.startDispatchingThread();
				streamReader.readSessionStarted();
				debug("STARTING SESSION --done");
			}
			else			
				debug("session not supported by server");
			
			streamWriter.writePresence(jid, service, "chat", "Online", 1);
			
			startKeepAlive();
			
			streamReader.startDispatchingThread();
			//streamReader.sysoAllContent();
			debug("xmpp connected");
		} catch (XMPPException e) {
			throw e;
		} catch (Exception e) {
			throw new XMPPException("error while xmpp connect", e);
		}
	}

	/**
	 * Starts the keep alive thread 
	 */
	private void startKeepAlive() {
		keepAliveTimer = new Timer("xmpp keep alive", true);
		keepAliveTimer.schedule( new TimerTask() {				
			public void run() {
				try {
					streamWriter.writeKeepAlivePackage();
				} catch (XMLStreamException e) {
					// ignore
				}
			}				
		}, 20000, 20000);
	}
	
	private InputStream wrapped(InputStream inputStream) {
		if (!debug)
			return inputStream;
		return new DebugInputStream(inputStream, new File("input.log"));
	}

	private OutputStream wrapped(OutputStream outputStream) {
		if (!debug)
			return outputStream;
		
		return new DebugOutputStream(outputStream, new File("output.log"));
	}

	public void queryRoster() throws XMPPException {
		sendIQMessage(new QName("jabber:iq:roster", "query"), "get", service);
	}
	
	/**
	 * This method sends a message as xmpp message.
	 * As side effect, the from and the id may be set, if they are null.
	 * @param msg
	 * @throws XMPPException 
	 */
	public void sendMessage(Message msg) throws XMPPException {
		String jid = (msg.getFrom() == null) ? getJid() : msg.getFrom(); 
		if (msg.getId() == null)
			msg.setId(genId("msg"));
			
		try {
			streamWriter.writeMessage(jid, msg);
		} catch (XMLStreamException e) {
			throw new XMPPException("error wile sending message", e);
		}		
	}

	/**
	 * This method sends a message as xmpp iq message.
	 * As side effect, the from and the id may be set, if they are null.
	 * @param msg
	 * @throws XMPPException 
	 */
	public void sendIQMessage(Message msg, String type) throws XMPPException {
		String jid = (msg.getFrom() == null) ? getJid() : msg.getFrom(); 
		if (msg.getId() == null)
			msg.setId(genId("msg"));
			
		try {
			streamWriter.writeIQMessage(jid, type, msg);
		} catch (XMLStreamException e) {
			throw new XMPPException("error wile sending message", e);
		}		
	}

	/**
	 * This method sends an iq message with an empty child element from the supplied name. 
	 * @throws XMPPException 
	 */
	public void sendIQMessage(final QName name, String type, String to) throws XMPPException {
		BasicMessage msg = new BasicMessage() {
			public void writeXmlRepresentation(XMLStreamWriter streamWriter) throws XMLStreamException {
				streamWriter.writeStartElement(name.getLocalPart());
				streamWriter.setDefaultNamespace(name.getNamespaceURI());
				streamWriter.writeDefaultNamespace(name.getNamespaceURI());
				streamWriter.writeEndElement();
			}
		};
		
		msg.setTo(to);
		sendIQMessage(msg, type);
	}

	public void close() throws IOException {
		keepAliveTimer.cancel();
		streamReader.stopAndJoin();	
		socket.close();		
		debug("closed");
	}

	public void startTLS() throws XMPPException {
		debug("tlsProceedReceived");
		try {

		// This code was patially taken from smack 3.0.4 under Apache licence
		// 2.0
			SSLContext context;
			context = SSLContext.getInstance("TLS");
			// Verify certificate presented by the server
			context.init(
					null, // KeyManager not required
					new javax.net.ssl.TrustManager[] { ScioTrustManager
							.getDefaultTrustManager() },
					new java.security.SecureRandom());
			Socket plain = socket;
			// Secure the plain connection
			socket = context.getSocketFactory()
					.createSocket(plain, plain.getInetAddress().getHostName(),
							plain.getPort(), true);
			socket.setSoTimeout(0);
			//socket.setKeepAlive(true);
			// Initialize the reader and writer with the new secured version
			// initReaderAndWriter();
			socket.getInputStream();
			socket.getOutputStream();
			// Proceed to do the handshake
			((SSLSocket) socket).startHandshake();

			// Set that TLS was successful
			usingTLS = true;
		} catch (Exception e) {
			throw new XMPPException("error while tls handshake", e);
		}

	}
	
	// begin implementation interface StreamListener -------------
	public void errorOccured(String errorCodeXmlNotWellFormed, Exception e) {
		System.err.println("ERROR: "+errorCodeXmlNotWellFormed);
		if (e != null)
			e.printStackTrace(System.err);	
	}

	public void featuresReceived(XMPPFeatures features) {
		this.features = features;
		debug("features: "+ features);		
	}
	
	private void debug(String msg) {
		if (debug)
			System.out.println(msg);		
	}

	public void messageRetrieved(Message msg) {
		debug(msg.getFrom()+"> "+msg);
	}
	
	public void iqMessageRetrieved(Message msg) {
		// TODO: do this by message a selector
		if ("jabber:iq:roster#query".equals(msg.getSubject()))
			setRoster((Roster)msg.getContent());
		messageRetrieved(msg);
	}

	public void unknownMessageRetrieved(Message msg) {
		messageRetrieved(msg);
	}

	public void xmppMessageRetrieved(Message msg) {
		messageRetrieved(msg);
	}

	// end interface StreamListener -------------

	private synchronized String genId(String prefix) {
		return prefix +"_"+ (++lastId);	
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public Roster getRoster() {
		return roster;
	}

	public void setRoster(Roster roster) {
		debug("new roster received: "+roster);
		this.roster = roster;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}
}
