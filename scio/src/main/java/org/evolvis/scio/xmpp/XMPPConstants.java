/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.xmpp;

import javax.xml.namespace.QName;


/**
 *
 * @autor Sebastian Mancke, tarent GmbH
 */
public interface XMPPConstants {

	public static String NS_STREAM = "http://etherx.jabber.org/streams";
	public static String NS_CLIENT = "jabber:client";
	public static String NS_SERVER= "jabber:server";
	public static String NS_BIND= "urn:ietf:params:xml:ns:xmpp-bind";
	
	public static QName TLS_START = new QName("urn:ietf:params:xml:ns:xmpp-tls", "starttls", "tls");
	public static QName TLS_PROCEED = new QName("urn:ietf:params:xml:ns:xmpp-tls", "proceed", "tls"); 
	public static QName SESSION = new QName("urn:ietf:params:xml:ns:xmpp-session", "session"); 

	public static String NS_SASL = "urn:ietf:params:xml:ns:xmpp-sasl";
	public static QName SASL_MECHANISMS = new QName (NS_SASL, "mechanisms");
	public static QName SASL_MECHANISM = new QName (NS_SASL, "mechanism");
	
	public static QName CLIENT_MESSAGE = new QName(NS_CLIENT, "message");
	public static QName CLIENT_IQ = new QName(NS_CLIENT, "iq");
	public static QName CLIENT_PRESENCE = new QName(NS_CLIENT, "presence");
	public static QName CLIENT_MESSAGE_BODY = new QName(NS_CLIENT, "body");
	public static QName CLIENT_MESSAGE_SUBJECT = new QName(NS_CLIENT, "subject");
	
	public static String ERROR_CODE_BAD_FORMAT = "bad-format";
	public static String ERROR_CODE_XML_NOT_WELL_FORMED = "xml-not-well-formed";	

	public static final String ERROR_INTERNAL = "internal-client-error";

}