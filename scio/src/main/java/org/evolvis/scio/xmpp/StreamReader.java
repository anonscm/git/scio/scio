/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.xmpp;

import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.evolvis.scio.messages.EmptyMessage;
import org.evolvis.scio.messages.TextMessage;
import org.evolvis.scio.messages.XMLMessage;
import org.evolvis.scio.util.XMLSubtreeReader;
import org.evolvis.scio.util.XMLUtil;

/**
 *
 * @autor Sebastian Mancke, tarent GmbH
 */
public class StreamReader implements Runnable, XMPPConstants {
		
	private InputStream inputStream;
	XMLStreamReader parser;
	boolean shouldClose = false;
	
	StreamListener listener;
	Thread myThread;
		
	public StreamReader(StreamListener listener) {
		this.listener = listener;
	}

	public void startDispatchingThread() {
		if (myThread == null)
			myThread = new Thread(this);
		myThread.start();
	}
	
	public void stopAndJoin() {
		try {
			shouldClose= true;
			if (myThread != null) {
				myThread.interrupt();
				myThread.join(50);
			}
		} catch (InterruptedException e) {
			// do nothing here
		}		
	}
	
	public void setInputStream(InputStream inputStream) throws XMLStreamException {
		this.inputStream = inputStream;
		XMLInputFactory factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputStream);
	}

	public void readStreamOpening() throws XMLStreamException, XMPPException {
		System.out.println("in streamReader#readStreamOpening()");

		parser.nextTag();
		if (!XMLUtil.isTag(parser, NS_STREAM, "stream"))
			throw new XMPPException("no opening stream tag from server");			

		parser.nextTag();
		if (!XMLUtil.isTag(parser, NS_STREAM, "features"))
			throw new XMPPException("no features tag from server");
		
		processFeatures(parser);		
	}
	
	public void readTLSProceed() throws XMLStreamException, XMPPException {
		System.out.println("in streamReader#readTLSProceed");
		parser.nextTag();
		if (!XMLUtil.isTag(parser, TLS_PROCEED))
			throw new XMPPException("no tls:proceed tag from server");
	}

	public void readSaslSuccess() throws XMLStreamException, XMPPException {
		System.out.println("in streamReader#readSaslSuccess");
		parser.nextTag();

		if (XMLUtil.isTag(parser, NS_SASL, "failure")) {
			parser.nextTag();
			if ("not-authorized".equals(parser.getLocalName()))
				throw new XMPPException("user not authorized");
			throw new XMPPException("failure while autorisation");
		}
		
		if (!XMLUtil.isTag(parser, NS_SASL, "success"))
			throw new XMPPException("no sasl:success tag from server");
		
		parser.nextTag(); // </success>
		
		System.out.println("sasl success " + parser.getName());
	}

	public String readBindResult(String msgid) throws XMLStreamException, XMPPException {
		System.out.println("in streamReader#readBindResult");
		parser.nextTag();
		if (!XMLUtil.isTag(parser, NS_CLIENT, "iq"))
			throw new XMPPException("no iq tag from server");
		parser.nextTag();
		if (!XMLUtil.isTag(parser, NS_BIND, "bind"))
			throw new XMPPException("no iq tag from server");
		parser.nextTag();
		if (!XMLUtil.isTag(parser, NS_BIND, "jid"))
			throw new XMPPException("no iq tag from server");
		String jid = parser.getElementText();
		parser.nextTag(); // </bind>
		parser.nextTag(); // </iq>
		return jid;
	}
	
	public void readSessionStarted() throws XMLStreamException, XMPPException {
		System.out.println("in streamReader#readSessionStarted");
		parser.nextTag();
		if (!XMLUtil.isTag(parser, NS_CLIENT, "iq"))
			throw new XMPPException("no iq tag from server", parser);
		if (! "result".equals(parser.getAttributeValue(null, "type")))
				throw new XMPPException("respone for session is not a result");
		parser.nextTag();
		if (!XMLUtil.isTag(parser, SESSION))
			throw new XMPPException("no session tag from server");
		parser.nextTag(); // </session>
		parser.nextTag(); // </iq>
	}	
	
	public void run() {
		try {			
			int event;
			do {
				event = parser.next();
				if (event == parser.START_ELEMENT) {
					if (XMLUtil.isTag(parser, CLIENT_MESSAGE)) {
						processXMPPMessage(parser);
					} else if (XMLUtil.isTag(parser, CLIENT_IQ)) {
						processIQ(parser);
					} else if (XMLUtil.isTag(parser, CLIENT_PRESENCE)) {
						processPresenceMessage(parser);
					} else {												
						processUnknownMessage(parser);
					}
				}
			} while (!shouldClose && event != parser.END_DOCUMENT);
			
		} catch (XMLStreamException e) {
			if (!shouldClose)
				listener.errorOccured(ERROR_CODE_XML_NOT_WELL_FORMED, e);
			// else
			//     expected syntax error found!
		}
	}

	private void processUnknownMessage(XMLStreamReader parser2) throws XMLStreamException {
		XMLSubtreeReader subTree = new XMLSubtreeReader(parser);
		XMLMessage msg = new XMLMessage(subTree);
		msg.setFrom(parser.getAttributeValue(null, "from"));
		msg.setTo(parser.getAttributeValue(null, "to"));
		msg.setId(parser.getAttributeValue(null, "id"));		
		msg.setSubject(parser.getNamespaceURI()+"#"+parser.getLocalName());
		listener.unknownMessageRetrieved(msg);
		subTree.skipSubTree();
	}

	private void processPresenceMessage(XMLStreamReader parser2) {
		// TODO implement processPresenceMessage
		
	}

	private void processIQ(XMLStreamReader parser) throws XMLStreamException {
		String from = parser.getAttributeValue(null, "from");
		String to = parser.getAttributeValue(null, "to");
		String id = parser.getAttributeValue(null, "id");
		String type = parser.getAttributeValue(null, "type");
		
		int event = parser.nextTag();
		if (event == parser.START_ELEMENT) {
			XMLSubtreeReader subTree = new XMLSubtreeReader(parser);
			XMLMessage msg = new XMLMessage(subTree);
			msg.setFrom(from);
			msg.setTo(to);
			if ("result".equals(type) || "error".equals(type))
				msg.setRecallId(id);
			else
				msg.setId(id);			
			msg.setErrorMessage("error".equals(type));
			
			msg.setSubject(parser.getNamespaceURI()+"#"+parser.getLocalName());
			listener.iqMessageRetrieved(msg);
			subTree.skipSubTree();
			parser.nextTag();
		} else { //end element
			EmptyMessage msg = new EmptyMessage();
			msg.setFrom(from);
			msg.setTo(to);
			// only allowed as recall 
			msg.setRecallId(id);
			msg.setErrorMessage("error".equals(type));
			listener.iqMessageRetrieved(msg);
		}		
	}

	private void processXMPPMessage(XMLStreamReader parser) throws XMLStreamException {
		String from = parser.getAttributeValue(null, "from");
		String to = parser.getAttributeValue(null, "to");
		String id = parser.getAttributeValue(null, "id");
		String type = parser.getAttributeValue(null, "type");
		String subject = null;
		
		int event;
		do {
			event = parser.next();
			if (event == parser.START_ELEMENT) {
				if (XMLUtil.isTag(parser, CLIENT_MESSAGE_BODY)) {
					parser.next();
					if (parser.isStartElement()) {
						XMLSubtreeReader subTree = new XMLSubtreeReader(parser);
						XMLMessage msg = new XMLMessage(subTree);
						msg.setFrom(from);
						msg.setTo(to);
						msg.setId(id);						
						msg.setErrorMessage("error".equals(type));
						if (subject != null)
							msg.setSubject(subject);
						listener.xmppMessageRetrieved(msg);
						subTree.skipSubTree();
					} 
					else if (parser.isCharacters()) {
						TextMessage msg = new TextMessage();
						msg.setFrom(from);
						msg.setTo(to);
						msg.setId(id);
						msg.setErrorMessage("error".equals(type));						
						if (subject != null)
							msg.setSubject(subject);
						msg.setContent(parser.getText());
						listener.xmppMessageRetrieved(msg);
						XMLUtil.skipSubtree(parser);
					}
					else {
						//TODO: how to handle this properly?
						// do not process message at the moment
						XMLUtil.skipSubtree(parser);
					}						
				}
				else if (XMLUtil.isTag(parser, CLIENT_MESSAGE_SUBJECT)) {
					parser.next();
					if (parser.isCharacters()) {
						subject = parser.getText();
						XMLUtil.skipSubtree(parser);
					}
				}
				else
					XMLUtil.skipSubtree(parser);
			}
		} while (!shouldClose && !(event == parser.END_ELEMENT && XMLUtil.isTag(parser, CLIENT_MESSAGE)));		
	}

	/**
	 * @param parser
	 * @throws XMLStreamException
	 */
	private void processFeatures(XMLStreamReader parser)
			throws XMLStreamException {
		int event;
		XMPPFeatures features = new XMPPFeatures();
		do {
			 event = parser.next();
			 if (event == parser.START_ELEMENT) {
				 if (XMLUtil.isTag(parser, TLS_START)) {
					 features.tlsSupported = true;
					 	event = parser.next();
					 features.tlsRequired = event == parser.START_ELEMENT 
					 						&& "required".equals(parser.getLocalName());
				 }
				 else if (XMLUtil.isTag(parser, SASL_MECHANISM)) {
					 parser.next();
					 if (parser.isCharacters() && parser.getText().contains("PLAIN"))
						 features.saslPLAINSupported = true;						 
				 } else if (XMLUtil.isTag(parser, "http://jabber.org/features/compress", "method")) {
					 parser.next();
					 features.zlibSupported = parser.isCharacters() && parser.getText().contains("zlib");						 
				 } else if (XMLUtil.isTag(parser, "urn:ietf:params:xml:ns:xmpp-bind", "bind")) {
					 features.bindSupported = true;						 
				 } else if (XMLUtil.isTag(parser, "urn:ietf:params:xml:ns:xmpp-session", "session")) {
					 features.sessionSupported = true;						 
				 }
			 }
		} while (! (event == parser.END_ELEMENT && XMLUtil.isTag(parser, NS_STREAM, "features")));
		listener.featuresReceived(features);
	}


//	public void sysoAllContent() {
//		byte[] buff = new byte[1024];
//		try {
//			while (!shouldClose) {
//				if (inputStream.available() <= 0) {
//					Thread.yield();
//				} else {
//					int count = inputStream.read(buff);
//					if (count == -1) {
//						System.out.println("stream closed: return!");
//						return;
//					}
//					System.out.print(new String(buff, 0, count));
//				}
//			}
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

}