/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import javax.xml.stream.XMLStreamReader;

import org.evolvis.scio.soap.SoapConstants.SoapVersion;
import org.evolvis.scio.util.XMLSubtreeReader;

/**
 * Wrappes an {@link XMLSubtreeReader} and is used by the class {@link SoapStreamReader}
 * to hide soap specific data to the application and make this data accesible by
 * methods. It is also possible to control the reading process of an application.
 * Currently nothing is done here.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class BodyBlockReader extends XMLSubtreeReader {

	private SoapVersion soapVersion;

	/**
	 * Create a {@link BodyBlockReader} which delegates all methods to the
	 * specified delegate object in regard of the specified SOAP version.
	 * 
	 * @param  delegate
	 *         The object to which all operations are delegated
	 * @param  soapVersion
	 *         The SOAP version which should be used.
	 */
	public BodyBlockReader(XMLStreamReader delegate, SoapVersion soapVersion) {
		super(delegate);
		this.soapVersion = soapVersion;
	}

	/**
	 * Returns the version of the SOAP stream.
	 * 
	 * @return The version of the SOAP stream
	 */
	public SoapVersion getSoapVersion() {
		return soapVersion;
	}
}
