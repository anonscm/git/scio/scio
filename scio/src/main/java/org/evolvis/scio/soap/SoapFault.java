/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.soap.SoapConstants.SoapFaultCode;

/**
 * A SOAP fault is used to carry error information within a SOAP message.
 * an instance of this class can be serialized inside the SOAP body element.
 * A valid {@link SoapFault} must contain at least one instance of {@link Reason}.
 * If the default {@link SoapFaultFactory} is used in the {@link SoapStreamReader} this
 * class will be used to carry the fault data. If the SOAP fault should contain
 * application specific data, it is necessary to derive this class.
 * If fault detail entries should be used, the methods
 * {@link #readDetailEntry(XMLStreamReader)} and
 * {@link #writeDetailEntries(XMLStreamWriter)} must be derived.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
@SuppressWarnings("serial")
// TODO: encoding style jedes entries über parameter setzbar machen
// TODO: evtl. attribute vom Detail element schreibbar machen
// TODO: evtl. textknoten vom Detail element schreibbar machen
public class SoapFault extends Exception {
	
	protected final SoapFaultCode code;
	protected List<QName> subCodeList;
	protected List<Reason> reasonList;
	protected String node;
	protected String role;
	
	/**
	 * Creates a SOAP fault which type is specified by the fault code.
	 * To create a valid {@link SoapFault} instance which can be serialized to xml,
	 * at least one {@link Reason} has to been added via the 
	 * {@link #addReason(org.evolvis.scio.soap.SoapFault.Reason)} method.
	 * 
	 * @param  code
	 *         The identifying fault code.
	 */
	public SoapFault(SoapFaultCode code) {
		this.code = code;
		this.subCodeList = new LinkedList<QName>();
	}
	
	/**
	 * Creates a SOAP fault which type is specified by the fault code and the subcodes.
	 * To create a valid {@link SoapFault} instance which can be serialized to xml,
	 * at least one {@link Reason} has to been added via the 
	 * {@link #addReason(org.evolvis.scio.soap.SoapFault.Reason)} method.
	 * 
	 * @param  code
	 *         The identifying fault code.
	 * @param  subCodeList
	 *         A list of subcodes. The root subcode is the first list element.
	 */
	public SoapFault(SoapFaultCode code, List<QName> subCodeList) {
		this.code = code;
		this.subCodeList = subCodeList;
	}
	
	/**
	 * Returns the fault code.
	 * 
	 * @return The fault code
	 */
	public SoapFaultCode getCode() {
		return code;
	}

	/**
	 * Returns a list of the subcodes. The root subcode is the first element in
	 * this list.
	 * 
	 * @return A list of the subcodes
	 */
	public List<QName> getSubCodeList() {
		return subCodeList;
	}

	/**
	 * Adds a reason to the fault. At least one reason has to be added if this
	 * fault is passed to {@link SoapStreamWriter#writeFault(SoapFault)}.
	 * 
	 * @param  reason
	 *         A fault reason
	 */
	public void addReason(Reason reason) {
		if (reasonList == null)
			reasonList = new LinkedList<Reason>();
		reasonList.add(reason);
	}
	
	/**
	 * Return an iterator over all fault reasons.
	 * 
	 * @return An iterator over all fault reasons
	 */
	public Iterator<Reason> getReasonIterator() {
		return reasonList.iterator();
	}

	/**
	 * Set the optional fault node.
	 * 
	 * @param  node
	 *         The fault node
	 */
	public void setNode(String node) {
		this.node = node;
	}

	/**
	 * Returns the optional fault node.
	 * 
	 * @return The fault node
	 */
	public String getNode() {
		return node;
	}

	/**
	 * Set the optional fault role.
	 * 
	 * @param  role
	 *         the fault role
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * Returns the optional fault role.
	 * 
	 * @return The fault role
	 */
	public String getRole() {
		return role;
	}
	
	/**
	 * Handle application specific fault data. Must be overwritten by deriving classes
	 * if fault detail entries should be used.
	 * By default this method will always throw an {@link UnsupportedOperationException}.
	 * 
	 * @param  reader
	 *         The xml reader
	 * @throws XMLStreamException
	 *         Can be thrown by deriving classes.
	 * @throws UnsupportedOperationException
	 *         Will always been thrown
	 */
	public void readDetailEntry(XMLStreamReader reader) throws XMLStreamException {
		throw new UnsupportedOperationException("implement application specific soap fault handling");
	}
	
	/**
	 * Handle application specific fault data. Must be overwritten by deriving classes
	 * if fault detail entries should be used.
	 * By default this method will do nothing.
	 * 
	 * @param  writer
	 *         The xml writer
	 * @throws XMLStreamException
	 *         Can be thrown by deriving classes.
	 */
	public void writeDetailEntries(XMLStreamWriter writer) throws XMLStreamException {
		// nothing to do
	}

	/**
	 * Can be used for debugging.
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		sb.append("SoapFault (class: "+getClass().getName()+")");
		sb.append("\ncode  : ").append(code.getId());
		String tp = "        '- ";
		for (QName subCode: subCodeList) {
			sb.append("\n").append(tp).append(subCode);
			tp = "   "+tp;
		}
		for (Reason reason : reasonList)
			sb.append("\nreason: (").append(reason.getLanguage()).append(") ").append(reason.getText());
		if (node != null)
			sb.append("\nnode  : ").append(node);
		if (node != null)
			sb.append("\nrole  : ").append(role);
		return sb.toString();
	}
	
	/**
	 * Datastructure for a SOAP fault reason.
	 * This reason is intended to be interpreted by a human and not by a machine.
	 * A SOAP fault must have at lest one reason but can have many 
	 * reasons in different languages.
	 */
	//	TODO: where does the language values come from? => create constant enumeration ?
	public static final class Reason {

		private String language;
		private String text;

		/**
		 * Create a SOAP fault reason with the specified language and fault text.
		 * 
		 * @param  language
		 *         A key for the language in which the text is written.
		 * @param  text
		 *         The reason text of a SOAP fault.
		 */
		public Reason(String language, String text) {
			if (language == null || text == null)
				throw new NullPointerException("language or text must not be null");
			this.language = language;
			this.text = text;
		}

		/**
		 * @return A key for the language in which the text is written.
		 */
		public String getLanguage() {
			return language;
		}

		/**
		 * @return The reason text of a SOAP fault.
		 */
		public String getText() {
			return text;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null || !(obj instanceof Reason))
				return false;
			Reason reason = (Reason) obj;
			return	language.equals(reason.language) &&
					text.equals(reason.text);
		}
	}
}
