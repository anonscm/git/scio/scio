/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.lang.reflect.Proxy;

/**
 * A singleton factory which uses the method
 * {@link Proxy#newProxyInstance(ClassLoader, Class[], java.lang.reflect.InvocationHandler)}
 * to create a service stub instance which uses reflection
 * to perform rpc calls.
 * The class {@link DynamicProxyStub} is used to handle the reflection.
 * For example you can creaste a stub instance for a service interface
 * "ServiceInterface" and for a given service url by:
 * <br>
 * <code>
 * ServiceInterface serviceStub = (ServiceInterface) StubFactory.getInstance().createStub("http://domain.com/service", ServiceInterface.class);
 * </code>
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class StubFactory {

    private StubFactory() {}
	
	private static StubFactory instance;
	
	public static boolean USE_SCIO_JAXB = true; //TODO not nice, should have the same interfaces

    /**
     * Returns the singleton factory instance.
     * 
     * @return The singleton factory instance
     */
    public static StubFactory getInstance() {
    	if (instance == null)
    		instance = new StubFactory();
		return instance;
	}

	/**
	 * Create a service stub instance.
	 * 
	 * @param  serverUrl
	 *         The url of the service which should be used
	 * @param  stubClass
	 *         The service interface which should be used
	 * @return A service stub instance
	 */
	public Object createStub(String serverUrl, Class stubClass) {
        return Proxy.newProxyInstance(stubClass.getClassLoader(),
                new Class[] { stubClass }, new DynamicProxyStub(serverUrl, stubClass));
    }
}
