/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * Hides the SOAP specific data which can be written to a header block
 * to an application. This data will be written automatically
 * by this class.
 * It will also be assured, that the top level element of the header block
 * is qualified. 
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class HeaderBlockWriter extends DemandQualifiedRootWriter implements SoapConstants {

	private SoapVersion soapVersion;
	private String role;
	private boolean mustUnderstand;
	private boolean relay;
	
	/**
	 * Create a header block writer which writes the SOAP specific data,
	 * which is given by the parameters automatically to the SOAP stream.
	 * 
	 * @param  delegate
	 *         The {@link XMLStreamWriter} to which all the calls of the
	 *         methods of this class will be delegated
	 * @param  soapVersion
	 *         The soapVersion which should be considered while writing the
	 *         SOAP specific data
	 * @param  role
	 *         The role to which the header block is addressed.
	 * @param  mustUnderstand
	 *         <code>true</code> if the header block must be understood by the
	 *         addressed SOAP node
	 * @param  relay
	 *         <code>true</code> if the header block should be relayed to
	 *         other SOAP nodes
	 */
	public HeaderBlockWriter(XMLStreamWriter delegate, SoapVersion soapVersion, String role, boolean mustUnderstand, boolean relay) {
		super(delegate);
		this.soapVersion =  soapVersion;
		this.role = role;
		this.mustUnderstand = mustUnderstand;
		this.relay = relay;
	}

	@Override
	public void writeStartElement(String prefix, String localName, String namespaceURI) throws XMLStreamException {
		super.writeStartElement(prefix, localName, namespaceURI);
		if (depth == 1) // root element => add attributes
			writeHeaderBlockAttributes();
	}

	@Override
	public void writeStartElement(String namespaceURI, String localName) throws XMLStreamException {
		super.writeStartElement(namespaceURI, localName);
		if (depth == 1) // root element => add attributes
			writeHeaderBlockAttributes();
	}
	
	/**
	 * Write the SOAP specific attributes to the current element.
	 * This must be called on the top level header block element.
	 * 
	 * @throws XMLStreamException
	 */
	private void writeHeaderBlockAttributes() throws XMLStreamException {
		String version = soapVersion.getNamespace();
		if (role != null)
			delegate.writeAttribute(version,
					ATTRIBUTE_NAME_HEADER_BLOCK_ROLE, role);
		if (mustUnderstand)
			delegate.writeAttribute(version,
					ATTRIBUTE_NAME_HEADER_BLOCK_MUST_UNDERSTAND,
					XML_BOOLEAN_TRUE_VALUE);
		if (relay)
			delegate.writeAttribute(version,
					ATTRIBUTE_NAME_HEADER_BLOCK_RELAY,
					XML_BOOLEAN_TRUE_VALUE);
	}
}
