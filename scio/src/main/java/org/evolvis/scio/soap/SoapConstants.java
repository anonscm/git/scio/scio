/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

/**
 * Containes constants which are used by soap related classes.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public interface SoapConstants {

	public static final String ELEMENT_NAME_ENVELOPE          = "Envelope";
	public static final String ELEMENT_NAME_HEADER            = "Header";
	public static final String ELEMENT_NAME_BODY              = "Body";
	
    public static final String ELEMENT_NAME_FAULT             = "Fault";
    public static final String ELEMENT_NAME_FAULT_CODE        = "Code";
    public static final String ELEMENT_NAME_FAULT_SUBCODE     = "Subcode";
    public static final String ELEMENT_NAME_FAULT_CODE_VALUE  = "Value";
    public static final String ELEMENT_NAME_FAULT_REASON      = "Reason";
    public static final String ELEMENT_NAME_FAULT_REASON_TEXT = "Text";
    public static final String ELEMENT_NAME_FAULT_NODE        = "Node";
    public static final String ELEMENT_NAME_FAULT_ROLE        = "Role";
    public static final String ELEMENT_NAME_FAULT_DETAIL      = "Detail";
    

    public static final String ATTRIBUTE_NAME_HEADER_BLOCK_ROLE            = "role";
    public static final String ATTRIBUTE_NAME_HEADER_BLOCK_MUST_UNDERSTAND = "mustUnderstand";
    public static final String ATTRIBUTE_NAME_HEADER_BLOCK_RELAY           = "relay";
    
    
    public static final String ATTRIBUTE_NAME_ENCODING_STYLE    = "encodingStyle";
    
    public static final String ATTRIBUTE_NAME_FAULT_REASON_TEXT_LANGUAGE    = "lang";

    public static final String XML_BOOLEAN_TRUE_VALUE    = "true";
    
    /**
     * envelope prefix used for writing SOAP data.
     */
    public static final String PREFIX_ENVELOPE = "env";
    

    public static final String NAMESPACE_XML_NAMESPACE = "http://www.w3.org/XML/1998/namespace";
    
    public static final String USER_AGENT_NAME = "tarent SOAP 0.1";
    public static final String SOAP_ENCODING = "UTF-8";

	// TODO implement SOAP version 1.1 and 1.0 and add them to this enumeration
	public static enum SoapVersion {
		
		SOAP_1_2 ("http://www.w3.org/2003/05/soap-envelope"), // SOAP 1.2
		SOAP_1_1 ("http://schemas.xmlsoap.org/soap/envelope/"); //SOAP 1.1
		
		private final String namespace;
		
		private SoapVersion(String namespace) {
			this.namespace = namespace;
		}
		
		public static SoapVersion valueOfNamespace(String namespace) {
			for (SoapVersion version : values())
				if (version.namespace.equals(namespace))
					return version;
			return null;
		}

		public String getNamespace() {
			return namespace;
		}
	}
    
    
	public static enum SoapFaultCode {
		
		DATA_ENCODING_UNKNOWN ("DataEncodingUnknown"),
		MUST_UNDERSTAND       ("MustUnderstand"), 
		RECEIVER              ("Receiver"), 
		SENDER                ("Sender"), 
		VERSION_MISMATCH      ("VersionMismatch");

		private final String id;
		
		private SoapFaultCode(String id) {
			this.id = id;
		}
		
		public String getId() {
			return id;
		}

		public static SoapFaultCode valueOfId(String id) {
			for (SoapFaultCode code : values())
				if (code.id.equals(id))
					return code;
			return null;
		}
	}
	
	public static enum SoapRole {

 		NONE              ("http://www.w3.org/2003/05/soap-envelope/role/none"),
		NEXT              ("http://www.w3.org/2003/05/soap-envelope/role/next"),
		ULTIMATE_RECEIVER ("http://www.w3.org/2003/05/soap-envelope/role/ultimateReceiver");
		
		private final String namespace;
		
		private SoapRole(String namespace) {
			this.namespace = namespace;
		}
		
		public static SoapRole valueOfNamespace(String namespace) {
			for (SoapRole version : values())
				if (version.namespace.equals(namespace))
					return version;
			return null;
		}

		public String getNamespace() {
			return namespace;
		}
	}
	

	
	public static final char XML_PREFIX_SEPARATOR =':'; 
    
    /** Namespace constant: http://www.w3.org/2001/XMLSchema */
    public static final String NAMESPACE_XSD = "http://www.w3.org/2001/XMLSchema";
    /** Namespace constant: http://www.w3.org/2001/XMLSchema */
    public static final String NAMESPACE_XSI = "http://www.w3.org/2001/XMLSchema-instance";
    /** Namespace constant: http://www.w3.org/1999/XMLSchema */
    public static final String NAMESPACE_XSD1999 = "http://www.w3.org/1999/XMLSchema";
    /** Namespace constant: http://www.w3.org/1999/XMLSchema */
    public static final String NAMESPACE_XSI1999 = "http://www.w3.org/1999/XMLSchema-instance";

}
