/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import javax.xml.stream.XMLStreamException;

/**
 * This exception should be thrown while reading a soap stream,
 * if the data is not in the expected or necessary format.
 * It should also be thrown while writing a soap stream if the
 * data which should be written is not in the needed constitution
 * or if the writing class is configured incorrectly.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
@SuppressWarnings("serial")
public class SoapStreamException extends XMLStreamException {

	/**
	 * Create an instance with the specified error message.
	 * 
	 * @param  message
	 *         The error message
	 */
	public SoapStreamException(String message) {
		super(message);
	}

	/**
	 * Create an instance with the specified error message and the
	 * underlying cause of this exception.
	 * 
	 * @param  message
	 *         The error message
	 * @param  cause
	 *         The error cause
	 */
	public SoapStreamException(String message, Throwable cause) {
		super(message, cause);
	}
}
