/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.XmlSerializable;
import org.evolvis.scio.soap.SoapFault.Reason;
import org.evolvis.scio.util.PrefixXMLStreamWriter;
import org.evolvis.scio.util.XMLSubtreeWriter;

/**
 * Writes a SOAP message. The application specific data is passed via the
 * {@link #writeHeaderBlock(XmlSerializable, String, boolean, boolean)},
 * {@link #writeBodyBlock(XmlSerializable)} and
 * {@link #writeFault(SoapFault)}.
 * First of all you have to write all header blocks. Afterwards a SOAP Fault or
 * all header blocks have to be written.
 * If a body block is written by the {@link #writeBodyBlock(XmlSerializable)} operation
 * a call of
 * {@link #writeHeaderBlock(XmlSerializable, org.evolvis.scio.soapnew.SoapConstants.SoapVersion, String, boolean, boolean)}
 * or {@link #writeFault(SoapFault)}
 * will cause a {@link SoapStreamException}.
 * If a SOAP fault is written by the {@link #writeFault(SoapFault)} operation
 * the SOAP document will be closed and a call of
 * {@link #writeHeaderBlock(XmlSerializable, org.evolvis.scio.soapnew.SoapConstants.SoapVersion, String, boolean, boolean)}
 * or {@link #writeBodyBlock(XmlSerializable)}
 * will cause a {@link SoapStreamException}.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class SoapStreamWriter implements SoapConstants {
	
	private XMLStreamWriter writer;
	private SoapVersion soapVersion;
	
	private Position position = Position.START;
	private enum Position {
		START,        /** wrote nothing */
		HEADER,       /** wrote header starting element but not the header end element */
		BODY,         /** wrote body start element but not the body end element */
		BODY_BLOCK,   /** wrote at least one body block but not the body end element */
		END           /** document closed */
	};
	
	/**
	 * Creates a {@link SoapStreamWriter} with SOAP version
	 * {@link SoapVersion#SOAP_1_2}.
	 * 
	 * @param  writer
	 *         The {@link XMLStreamWriter} which will be used to write the SOAP data.
	 */
	public SoapStreamWriter(XMLStreamWriter writer) {
		this(writer, SoapVersion.SOAP_1_2);
	}
	
	/**
	 * Creates a {@link SoapStreamWriter} with the specified SOAP version.
	 * 
	 * @param  writer
	 *         The {@link XMLStreamWriter} which will be used to write the SOAP data.
	 * @param  soapVersion
	 *         The used SOAP version.
	 */
	//TODO: enable setting and writing of the encodingstyle attribute
	//TODO: enable writing of qualified attributes to the Envelope, Header and Body element
	public SoapStreamWriter(XMLStreamWriter writer, SoapVersion soapVersion) {
		this.writer = PrefixXMLStreamWriter.enablePrefixDefaulting(writer);
		this.soapVersion = soapVersion;
	}
	
	/**
	 * Write a SOAP header block.
	 * 
	 * @param  block
	 *         The application specific header block data. The SOAP specific
	 *         attributes are added automatically.
	 * @param  role
	 *         The role which specifies the SOAP node which should process this block.
	 * @param  mustUnderstand
	 *         <code>true</code> if it is necessary that the addressed SOAP node does handle the message.
	 * @param  relay
	 *         <code>true</code> if the message should be forwarded.
	 * @throws XMLStreamException
	 * @throws SoapStreamException
	 *         If the SOAP header is already closed.
	 *         That happens if one of the following operations are called:
	 *         {@link #writeBodyBlock(XmlSerializable)},
	 *         {@link #writeFault(SoapFault)} or
	 *         {@link #close()}.
	 */
	public void writeHeaderBlock(XmlSerializable block, String role, boolean mustUnderstand, boolean relay) throws XMLStreamException, SoapStreamException {
		switch (position) {
		    case START:
		    	writeStartEnvelope();
		        writeStartHeader();
		        break;
		    case HEADER:
		    	break;
		    default:
		    	throw new SoapStreamException("header is already closed");
		}
		HeaderBlockWriter hbwriter = new HeaderBlockWriter(writer, soapVersion, role, mustUnderstand, relay);
		block.writeXmlRepresentation(hbwriter);
		hbwriter.writeAllSubtreeEndElements();
	}
	

	/**
	 * Write a SOAP fault and automatically closes the message.
	 * 
	 * @param  soapFault
	 *         The fault struct which is serialized by its
	 *         {@link XmlSerializable#writeXmlRepresentation(XMLStreamWriter)}
	 *         operation.
	 * @throws XMLStreamException
	 * @throws SoapStreamException
	 *         If the body is not empty or if the body is closed already.
	 *         That happens if one of the following operations are called:
	 *         {@link #writeBodyBlock(XmlSerializable)} or
	 *         {@link #close()}.
	 */
	public void writeFault(SoapFault soapFault) throws XMLStreamException, SoapStreamException {
		switch (position) {
		    case START:
		    	writeStartEnvelope();
		        writeStartBody();
		        break;
		    case HEADER:
		    	writer.writeEndElement();
		        writeStartBody();
		    	break;
		    case BODY:
		    	break;
		    case BODY_BLOCK:
		    	throw new SoapStreamException("body is not empty");
		    case END:
		    	throw new SoapStreamException("body is already closed");
		}
		writeSoapFault(soapFault);
		close();
	}
	


	private void writeSoapFault(SoapFault soapFault) throws XMLStreamException {
		if (soapFault.getCode() == null)
			throw new RuntimeException("no code has been set");
		if (!soapFault.getReasonIterator().hasNext())
			throw new RuntimeException("at least one reason has to be given");

		String versionNamespace = soapVersion.getNamespace();
		String versionPrefix = writer.getPrefix(versionNamespace);
		
		if (versionPrefix == null)
			throw new RuntimeException("SOAP version namespace is unbound");

		// write fault element
		writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT);
		
		// write code element
		writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT_CODE);
		
		// write code value
		writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT_CODE_VALUE);
		String value = PrefixXMLStreamWriter.writeValueNamespace(writer, versionNamespace, soapFault.getCode().getId());
		writer.writeCharacters(value);
		writer.writeEndElement();

		// write subcode element
		List<QName> subCodeList = soapFault.getSubCodeList();
		int i = 0, l = subCodeList == null ? 0 : subCodeList.size();
		for (; i < l; i ++) {
			QName subcode = subCodeList.get(i);
			writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT_SUBCODE);
			writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT_CODE_VALUE);
			value = PrefixXMLStreamWriter.writeValueNamespace(writer, subcode.getNamespaceURI(), subcode.getLocalPart());
			writer.writeCharacters(value);
			writer.writeEndElement();
		}
		// close all subcode elements
		for (; i > 0; i --)
			writer.writeEndElement();
		
		// close code element
		writer.writeEndElement();

		// write reason element
		
		writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT_REASON);
		Iterator<Reason> reasonIterator = soapFault.getReasonIterator();
		while (reasonIterator.hasNext()) {
			Reason reason = reasonIterator.next();
			writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT_REASON_TEXT);
			writer.writeAttribute(NAMESPACE_XML_NAMESPACE,
					ATTRIBUTE_NAME_FAULT_REASON_TEXT_LANGUAGE, reason.getLanguage());
			writer.writeCharacters(reason.getText());
			writer.writeEndElement();
		}
		writer.writeEndElement();
		
		// write node
		String node = soapFault.getNode();
		if (node != null) {
			writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT_NODE);
			writer.writeCharacters(node);
			writer.writeEndElement();
		}

		// write role
		String role = soapFault.getRole();
		if (role != null) {
			writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT_ROLE);
			writer.writeCharacters(role);
			writer.writeEndElement();
		}

		writer.writeStartElement(versionNamespace, ELEMENT_NAME_FAULT_DETAIL);

		XMLSubtreeWriter swriter = new DemandQualifiedRootWriter(writer);
		soapFault.writeDetailEntries(swriter);
		swriter.writeAllSubtreeEndElements();
		writer.writeEndElement(); // detail end element
		writer.writeEndElement(); // fault end element
		position = Position.BODY_BLOCK;
		close();
	}
	
	/**
	 * Write a SOAP body block.
	 * 
	 * @param  block
	 *         The application specific body block data.
	 * @throws XMLStreamException
	 * @throws SoapStreamException
	 *         If the SOAP body is already closed.
	 *         That happens if one of the following operations are called:
	 *         {@link #writeFault(SoapFault)} or
	 *         {@link #close()}.
	 */
	public void writeBodyBlock(XmlSerializable block) throws XMLStreamException {
		switch (position) {
		    case START:
		    	writeStartEnvelope();
		        writeStartBody();
		        break;
		    case HEADER:
		    	writer.writeEndElement();
		        writeStartBody();
		    	break;
		    case END:
		    	throw new SoapStreamException("body is already closed");
		}
		BodyBlockWriter bbwriter = new BodyBlockWriter(writer, soapVersion);
		block.writeXmlRepresentation(bbwriter);
		bbwriter.writeAllSubtreeEndElements();
		position = Position.BODY_BLOCK;
	}

	/**
	 * Closes the SOAP message. All following calls of
	 * {@link #writeHeaderBlock(XmlSerializable, String, boolean, boolean)},
	 * {@link #writeBodyBlock(XmlSerializable)} or
	 * {@link #writeFault(SoapFault)}
	 * will cause a {@link SoapStreamException}.
	 * 
	 * @throws XMLStreamException
	 */
	public void close() throws XMLStreamException {
		switch (position) {
		    case START:
		    	writeStartEnvelope();
		        writeStartBody();
		        close();
		        break;
		    case HEADER:
		    	writer.writeEndElement();
		        writeStartBody();
		    case BODY:
		    case BODY_BLOCK:
		    	writer.writeEndElement(); // close body element
		    	writer.writeEndElement(); // close envelope element
		    	writer.writeEndDocument();
		    	writer.close();
		    	position = Position.END;
		}
	}
	
	private void writeStartEnvelope() throws XMLStreamException {
		if (soapVersion != SoapVersion.SOAP_1_2)
			throw new SoapStreamException("only SOAP version '"+SoapVersion.SOAP_1_2.getNamespace()+"' "+
			"is implemented right now");
		writer.writeStartDocument();
		writer.setPrefix(PREFIX_ENVELOPE, soapVersion.getNamespace());
		writer.writeStartElement(soapVersion.getNamespace(), ELEMENT_NAME_ENVELOPE);
	}
	
	private void writeStartHeader() throws XMLStreamException {
		writer.writeStartElement(soapVersion.getNamespace(), ELEMENT_NAME_HEADER);
        position = Position.HEADER;
	}
	
	private void writeStartBody() throws XMLStreamException {
		writer.writeStartElement(soapVersion.getNamespace(), ELEMENT_NAME_BODY);
        position = Position.BODY;
	}
}
