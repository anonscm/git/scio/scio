/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.evolvis.scio.soap.SoapConstants;
import org.evolvis.scio.util.XMLSubtreeReader;

/**
 * A {@link XMLStreamReader} which can be used to read SOAP header blocks.
 * SOAP specific attributes in the root tag will be hidden when calling one of
 * the following attribute related operations:
 * {@link #getAttributeCount()},
 * {@link #getAttributeLocalName(int)},
 * {@link #getAttributeName(int)},
 * {@link #getAttributeNamespace(int)},
 * {@link #getAttributePrefix(int)},
 * {@link #getAttributeType(int)},
 * {@link #getAttributeValue(int)} or
 * {@link #getAttributeValue(String, String)}.
 * The values of the hidden SOAP attributes can be accessed by the following operations:
 * {@link #getRole()},
 * {@link #isMustUnderstand()} and
 * {@link #isRelay()}.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class HeaderBlockReader extends XMLSubtreeReader implements SoapConstants {
	
	private String version;
	private String role;
	private boolean mustUnderstand = false;
	private boolean relay = false;
	
	/**
	 * A mapping for the viewable attibutes of the header block root node to the real attributes.
	 * This is used to hide SOAP specific attributes from the application specific code.
	 */
	private List<Integer> mapAttributes;
	
	public HeaderBlockReader(XMLStreamReader delegate, SoapVersion soapVersion) {
		super(delegate);
		version = soapVersion.getNamespace();
	}

	/**
	 * Read the SOAP specific attributes {@link #role}, {@link #mustUnderstand} and {@link #relay}
	 * from the root tag and create the mapping {@link #mapAttributes}.
	 */
	private void readAllSoapAttributes() {
		if (depth == 0) {
			if (mapAttributes == null && getEventType() == XMLStreamConstants.START_ELEMENT) {
				mapAttributes = new LinkedList<Integer>();
				for (int i = 0; i < super.getAttributeCount(); i++) {
					QName qname = super.getAttributeName(i);
					if (version.equals(qname.getNamespaceURI())) {
						String name = qname.getLocalPart();
						if (ATTRIBUTE_NAME_HEADER_BLOCK_ROLE.equals(name)) {
							role = DatatypeConverter.parseString(super.getAttributeValue(i));
						} else if (ATTRIBUTE_NAME_HEADER_BLOCK_MUST_UNDERSTAND.equals(name)) {
							mustUnderstand = DatatypeConverter.parseBoolean(super.getAttributeValue(i));
						} else if (ATTRIBUTE_NAME_HEADER_BLOCK_RELAY.equals(name)) {
							relay = DatatypeConverter.parseBoolean(super.getAttributeValue(i));
						} else
							throw new RuntimeException("unknown attribute "+qname);
					} else
						mapAttributes.add(i);
				}
			}
		} else if (mapAttributes != null)
			mapAttributes = null; // not needed anymore => to garbage
	}

	@Override
	public int next() throws XMLStreamException {
		readAllSoapAttributes();
		return super.next();
	}

	@Override
	public int nextTag() throws XMLStreamException {
		readAllSoapAttributes();
		return super.nextTag();
	}

	@Override
	public int getAttributeCount() {
		if (depth == 0) {
			readAllSoapAttributes();
			return mapAttributes.size();
		}
		return super.getAttributeCount();
	}

	@Override
	public String getAttributeLocalName(int index) {
		if (depth == 0) {
			readAllSoapAttributes();
			index = mapAttributes.get(index);
		}
		return super.getAttributeLocalName(index);
	}

	@Override
	public QName getAttributeName(int index) {
		if (depth == 0) {
			readAllSoapAttributes();
			index = mapAttributes.get(index);
		}
		return super.getAttributeName(index);
	}

	@Override
	public String getAttributeNamespace(int index) {
		if (depth == 0) {
			readAllSoapAttributes();
			index = mapAttributes.get(index);
		}
		return super.getAttributeNamespace(index);
	}

	@Override
	public String getAttributePrefix(int index) {
		if (depth == 0) {
			readAllSoapAttributes();
			index = mapAttributes.get(index);
		}
		return super.getAttributePrefix(index);
	}

	@Override
	public String getAttributeType(int index) {
		if (depth == 0) {
			readAllSoapAttributes();
			index = mapAttributes.get(index);
		}
		return super.getAttributeType(index);
	}

	@Override
	public String getAttributeValue(int index) {
		if (depth == 0) {
			readAllSoapAttributes();
			index = mapAttributes.get(index);
		}
		return super.getAttributeValue(index);
	}

	@Override
	public String getAttributeValue(String namespaceURI, String localName) {
		if (depth != 0 || !version.equals(namespaceURI))
			return super.getAttributeValue(namespaceURI, localName);
		return null;
	}

	/**
	 * Get the role of the addressed SOAP node.
	 * 
	 * @return The role of the addressed SOAP node
	 */
	public String getRole() {
		readAllSoapAttributes();
		return role;
	}

	/**
	 * Returns <code>true</code> if the addressed SOAP node
	 * must handle and understand this header block.
	 * 
	 * @return <code>true</code> if the addressed SOAP node
	 *         must handle and understand this header block.
	 */
	public boolean isMustUnderstand() {
		readAllSoapAttributes();
		return mustUnderstand;
	}

	/**
	 * Returns <code>true</code> if the header block should
	 * be relayed by a SOAP node to following SOAP nodes
	 * after it is processed.
	 * 
	 * @return <code>true</code> if the header block should
	 *         be relayed by a SOAP node to following SOAP nodes
	 *         after it is processed.
	 */
	public boolean isRelay() {
		readAllSoapAttributes();
		return relay;
	}
}
