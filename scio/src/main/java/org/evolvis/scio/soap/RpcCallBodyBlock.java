/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.XmlSerializable;
import org.evolvis.scio.bind.BindContext;
import org.evolvis.scio.bind.BindContextHolder;
import org.evolvis.scio.bind.Marshaller;
import org.evolvis.scio.util.XMLSubtreeWriter;
import org.evolvis.scio.util.XMLUtil;

/**
 * Used by the class {@link DynamicProxyStub} ... TODO
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
class RpcCallBodyBlock implements XmlSerializable {
	
	private QName operationName;
	private List<Parameter> parameterList;
	
	public RpcCallBodyBlock(QName operationName) {
		this.operationName = operationName;
	}
	
	public void addParameter(QName name, Object value) {
		if (parameterList == null)
			parameterList = new LinkedList<Parameter>();
		parameterList.add(new Parameter(name, value));
	}
	
	@SuppressWarnings("unchecked") // superess warnings for JAXBElement creation
	public void writeXmlRepresentation(XMLStreamWriter writer) throws XMLStreamException {
		writer.writeStartElement(operationName.getNamespaceURI(), operationName.getLocalPart());
		if(parameterList != null) {
			for (Parameter parameter : parameterList) {
				try {
					BindContext bindContext = BindContextHolder.getDefaultContext();
					if (StubFactory.USE_SCIO_JAXB) {
						Marshaller marshaller = bindContext.createMarshaller();
						//
						if (XMLUtil.isComplexType(parameter.value.getClass()))
							marshaller.marshal(parameter.value, writer);
						else {
							JAXBElement x = new JAXBElement( 
									parameter.name, parameter.value.getClass(), parameter.value );
							marshaller.marshal(x, writer);
						}
					} else {
						JAXBContext jc = JAXBContext.newInstance(bindContext.getPackageSearchPath());
						javax.xml.bind.Marshaller marshaller = jc.createMarshaller();
						// encapsulate writer to force marshaller not to close superior tags
						// (com.sun.xml.bind.v2.runtime.MarshallerImpl did it)
						XMLSubtreeWriter subWriter = new XMLSubtreeWriter(writer);
						// same as above but different Marshaller class
						if (XMLUtil.isComplexType(parameter.value.getClass()))
							marshaller.marshal(parameter.value, subWriter);
						else {
							JAXBElement x = new JAXBElement( 
									parameter.name, parameter.value.getClass(), parameter.value );
							marshaller.marshal(x, subWriter);
						}
						subWriter.writeAllSubtreeEndElements();
					}
				} catch (JAXBException e) {
					e.printStackTrace();// TODO remove
					throw new XMLStreamException(e);
				}
			}
		}
	}
	
	private class Parameter {

		private QName name;
		private Object value;
		
		public Parameter(QName name, Object value) {
			this.name = name;
			this.value = value;
		}
	}
}
