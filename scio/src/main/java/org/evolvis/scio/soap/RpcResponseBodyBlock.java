/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;

import org.evolvis.scio.bind.BindContext;
import org.evolvis.scio.bind.BindContextHolder;
import org.evolvis.scio.bind.Unmarshaller;
import org.evolvis.scio.util.XMLUtil;

/**
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class RpcResponseBodyBlock {

	private static final String SUFFIX_RESPONSE = "Response";
	
	private Object response;

	public RpcResponseBodyBlock(BodyBlockReader reader, Class<?> returnType, QName returnName, QName operationName) throws XMLStreamException, JAXBException {
		reader.require(XMLStreamConstants.START_ELEMENT, operationName.getNamespaceURI(), operationName.getLocalPart() + SUFFIX_RESPONSE);
		int event = reader.nextTag();
		if (event == XMLStreamConstants.START_ELEMENT) {
			if (XMLUtil.isComplexType(returnType)) { // complex type
				BindContext bindContext = BindContextHolder.getDefaultContext();
				if (StubFactory.USE_SCIO_JAXB) {
					Unmarshaller unmarshaller = bindContext.createUnarshaller();
					response = unmarshaller.unmarshal(reader);
				} else {
					JAXBContext jc = JAXBContext.newInstance(bindContext.getPackageSearchPath()); //TODO: get somewhere
					javax.xml.bind.Unmarshaller unmarshaller = jc.createUnmarshaller();
					response = unmarshaller.unmarshal(reader);
				}
			} else {                                 // simple type
				reader.require(XMLStreamConstants.START_ELEMENT, returnName.getNamespaceURI(), returnName.getLocalPart());
				JAXBElement<?> je;
				BindContext bindContext = BindContextHolder.getDefaultContext();
				if (StubFactory.USE_SCIO_JAXB) {
					Unmarshaller unmarshaller = bindContext.createUnarshaller();
					je = (JAXBElement<?>) unmarshaller.unmarshal(reader, returnType);
				} else {
					JAXBContext jc = JAXBContext.newInstance(bindContext.getPackageSearchPath());
					javax.xml.bind.Unmarshaller unmarshaller = jc.createUnmarshaller();
					je = (JAXBElement<?>) unmarshaller.unmarshal(reader, returnType);
				}
				response = je.getValue();
			}
		}
	}
		
	public Object getResponse() {
		return response;
	}
}
