/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingType;

import org.evolvis.scio.soap.transport.OutInMep;

/**
 * This class is used by the class {@link StubFactory} to use reflection to translate
 * Java calls to a JAXWS annotated interface at runtime to SOAP calls.
 * 
 * @see StubFactory
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
@SuppressWarnings("unused")
class DynamicProxyStub implements InvocationHandler {
	
	private static final String ERROR_MESSAGE_WRONG_SOAP_STYLE = "only rpc/literal and document/literal wrapped styles are supported";
	
	private Class stubClass;
	private String serverUrl;
	private String contextPath;

	private WebService webService;
	
	/**
	 * This constructor will be called in {@link StubFactory#createStub(String, Class)}
	 * to handle the reflection for the given stub interface and translate
	 * calls to this interface to SOAP calls to the specified service url.
	 * 
	 * @param  serverUrl
	 *         The url of the web service
	 * @param  stubClass
	 *         The interface which will be used to perform the calls 
	 */
	@SuppressWarnings("unchecked")
	public DynamicProxyStub(String serverUrl, Class stubClass) {
		this.stubClass = stubClass;
		this.serverUrl = serverUrl;
		webService = (WebService) stubClass.getAnnotation(WebService.class);
	}
   
    /**
	 * Check which type of SOAP binding should be used. Currently only the most
	 * importend styles "Rpc/literal" and "Document/literal wrapped" are
	 * supported.
	 * The "Document/literal wrapped" style is set if the specified {@link SOAPBinding}
	 * is configured with its default values.
	 * The "Rpc/literal" style is set if the parameter {@link SOAPBinding#style()}
	 * is set to {@link SOAPBinding.Style#RPC} and the other parameters does have
	 * the default values.
	 * 
	 * @param  soapBinding
	 *         The annotation of a class or a operation which should be analyzed.
	 * @return
	 *         <code>true</code> if the style "Rpc/literal" is used,
	 *         <code>false</code> if the style "Document/literal wrapped" is used
	 * @throws RuntimeException
	 *         If the configured binding type is not "Rpc/literal" or "Document/literal wrapped".
	 */
    private static boolean isRpcStyle(SOAPBinding soapBinding) {
    	if (soapBinding.style() == SOAPBinding.Style.RPC) { // rpc style
			   if (    soapBinding.use()            != SOAPBinding.Use.LITERAL          || 
				       soapBinding.parameterStyle() != SOAPBinding.ParameterStyle.WRAPPED)
				   throw new RuntimeException(ERROR_MESSAGE_WRONG_SOAP_STYLE);
			   return true;
		   } else if (soapBinding.style() == SOAPBinding.Style.DOCUMENT) { // document style
			   if (    soapBinding.use()            != SOAPBinding.Use.LITERAL          || 
				       soapBinding.parameterStyle() != SOAPBinding.ParameterStyle.WRAPPED)
				   throw new RuntimeException(ERROR_MESSAGE_WRONG_SOAP_STYLE);
			   return false;
		   } else // should not happen (only to values for SOAPBinding.Style available)
			   throw new RuntimeException(ERROR_MESSAGE_WRONG_SOAP_STYLE);
    }

    /**
     * Check if the specified method does have a {@link SOAPBinding} annotation.
     * If not it is checked if the declaring class does have this annotation.
     * If not, the default values of {@link SOAPBinding} are used.
     * Return the return value of the method {@link #isRpcStyle(Method)}
     * for this annotation.
     * 
     * @see #isRpcStyle(Method)
     * 
     * @param  method
     * @return
     */
    private static boolean isRpcStyle(Method method) {
    	SOAPBinding sb = method.getAnnotation(SOAPBinding.class);
    	if (sb == null)
    		sb = method.getDeclaringClass().getAnnotation(SOAPBinding.class);
    	if (sb == null)
    		 return false; // no SOAPBinding annotation => default of SOAPBinding
    	return isRpcStyle(sb);
    }
      
    /** 
     * First inspects the given method and the parameters and their annotations
     * and then uses {@link RpcCallBodyBlock} to send a SOAP call.
     * The recieved response will be handled by {@link RpcResponseBodyBlock}
     * and the retrieved object will be returned.
     * 
     * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object, java.lang.reflect.Method, java.lang.Object[])
     * @throws SoapFault
     *         If the returned SOAP message does contain a SOAP fault.
     */
    public Object invoke(Object proxy, Method method, Object[] args)
        throws Throwable {
        WebMethod webMethod;
        WebResult webResult;
        
        /** If a SOAPBinding annotation is specified for the given method, the global 
         *  SOAPBinding is overwritten */
        boolean rpcStyle = isRpcStyle(method);

        webMethod = method.getAnnotation(WebMethod.class);
        webResult = method.getAnnotation(WebResult.class);
        QName operationName = new QName(webService.targetNamespace(), method.getName());
    	RpcCallBodyBlock rpcCall = new RpcCallBodyBlock(operationName);
    	
    	
		Annotation[][] paramAnnotations = method.getParameterAnnotations();
		for (int i = 0; i < paramAnnotations.length ; i++)
			for (Annotation annotation : paramAnnotations[i]) {
				if (annotation instanceof WebParam) {
					WebParam param = (WebParam) annotation;
					if (param.mode() == WebParam.Mode.IN || param.mode() == WebParam.Mode.INOUT)
						rpcCall.addParameter(new QName("", rpcStyle ? param.partName() : param.name()), args[i]);
						continue;
				}
			}
        QName resultName = new QName("", rpcStyle ? webResult.partName() : webResult.name());
			OutInMep oi = new OutInMep(serverUrl);
			SoapStreamWriter ow = oi.getOutWriter();
			ow.writeBodyBlock(rpcCall);
			ow.close();
			SoapStreamReader ir = oi.getInReader();
			if (ir.hasFault())
				throw ir.getFault();
			if (ir.hasNextBodyBlock()) {
				BodyBlockReader block = ir.nextBodyBlock();
				RpcResponseBodyBlock rbb = new RpcResponseBodyBlock(block, method.getReturnType(), resultName, operationName);
				return rbb.getResponse();
			}
			return null;
    }
}
