/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.util.List;

import javax.xml.namespace.QName;

import org.evolvis.scio.soap.SoapFault;
import org.evolvis.scio.soap.SoapStreamException;
import org.evolvis.scio.soap.SoapConstants.SoapFaultCode;

/**
 * The default soap fault factory. This factory will be used by the
 * {@link SoapStreamReader} if the constructor
 * {@link SoapStreamReader#SoapReader(javax.xml.stream.XMLStreamReader)}
 * is used. If apllication specific fault classes should be used, a
 * deriving factory which will handle the creation of the fault classes
 * must be passed to
 * {@link SoapStreamReader#SoapReader(javax.xml.stream.XMLStreamReader, SoapFaultFactory)}.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class SoapFaultFactory {
	
	/**
	 * Create a new soap fault.
	 * 
	 * @param  code
	 *         The fault code
	 * @param  subCodeList
	 *         The fault subcodes. This can be an empty list
	 * @return A soap fault which holds the fault code and subcodes
	 * @throws SoapStreamException
	 */
	public SoapFault createSoapFault(SoapFaultCode code, List<QName> subCodeList) throws SoapStreamException {
		return new SoapFault(code, subCodeList);
	}
}
