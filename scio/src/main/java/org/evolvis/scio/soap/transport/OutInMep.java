/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.soap.SoapConstants;
import org.evolvis.scio.soap.SoapStreamException;
import org.evolvis.scio.soap.SoapStreamReader;
import org.evolvis.scio.soap.SoapStreamWriter;

/**
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class OutInMep {

	private String url;
	private ServiceConnection connection;
	private OutputStream out;
	
	public OutInMep(String url) {
		this.url = url;
	}
	
    protected ServiceConnection getServiceConnection() throws IOException {
        return new ServiceConnectionSE(url);
    }
    
	public SoapStreamWriter getOutWriter() throws IOException, XMLStreamException {
        connection = getServiceConnection();
        connection.setRequestProperty("SOAPAction", "");
        connection.setRequestProperty("User-Agent", SoapConstants.USER_AGENT_NAME);
        connection.setRequestProperty("Content-Type", "text/xml; charset=" + SoapConstants.SOAP_ENCODING);
        connection.setRequestProperty("Connection", "close");
        connection.setRequestMethod("POST");
//        connection.connect();
        out = connection.openOutputStream();
  
	    // Create an output factory
	    XMLOutputFactory factory = XMLOutputFactory.newInstance();
	    XMLStreamWriter writer;
	    // Create an XML stream writer
	    writer = factory.createXMLStreamWriter(out, SoapConstants.SOAP_ENCODING);
	    SoapStreamWriter sw = new SoapStreamWriter(writer);
        return sw;
	}
	
	public SoapStreamReader getInReader() throws IOException, XMLStreamException, SoapStreamException {
        out.close();
        InputStream is;
        try {
            connection.connect();
            is = connection.openInputStream();
        } catch (IOException e) {
            is = connection.getErrorStream();
            if (is == null) {
                connection.disconnect();
                throw e;
            }
        }
	    XMLInputFactory factory = XMLInputFactory.newInstance();
	    XMLStreamReader reader;
	    reader = factory.createXMLStreamReader(is, SoapConstants.SOAP_ENCODING);
	    SoapStreamReader sr = new SoapStreamReader(reader);
		return sr;
	}
	
}
