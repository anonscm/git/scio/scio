/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.soap.SoapConstants.SoapVersion;

/**
 * Wrappes an {@link DemandQualifiedRootWriter} and is used by the class {@link SoapStreamWriter}
 * to hide soap specific data to the application and write this data automatically.
 * It is also possible to control the writing process of an application.
 * Currently it is checked if the header block is empty or not. This can be checked by the
 * {@link #isEmpty()} method.
 * It is only allowed to write element nodes at the root level.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class BodyBlockWriter extends DemandQualifiedRootWriter {

	@SuppressWarnings("unused")
	private SoapVersion soapVersion; // may be useful later
	
	private static final XMLStreamException NOT_ALLOWED_ELEMENT_AT_ROOT_LEVEL =
		new SoapStreamException("it is not allowed to write this element at the SOAP body level");
	
	private boolean empty = true;

	private void throwExceptionIfUnderRoot() throws XMLStreamException {
		if (depth == 0)
			throw NOT_ALLOWED_ELEMENT_AT_ROOT_LEVEL;
	}
	
	/**
	 * Returns <code>true</code> if something is written.
	 * 
	 * @return <code>true</code> if something is written
	 */
	public boolean isEmpty() {
		return empty;
	}

	/**
	 * Create a {@link BodyBlockWriter} which delegates all methods to the
	 * specified delegate object in regard of the specified SOAP version.
	 * 
	 * @param  delegate
	 *         The object to which all operations are delegated
	 * @param  soapVersion
	 *         The SOAP version which should be used.
	 */
	public BodyBlockWriter(XMLStreamWriter delegate, SoapVersion soapVersion) {
		super(delegate);
		this.soapVersion = soapVersion;
	}

	// writing operations which may throw an exception if the writer is on the root level
	
	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeAttribute(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void writeAttribute(String prefix, String namespaceURI, String localName, String value) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeAttribute(prefix, namespaceURI, localName, value);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeAttribute(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void writeAttribute(String namespaceURI, String localName, String value) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeAttribute(namespaceURI, localName, value);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeAttribute(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeAttribute(String localName, String value) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeAttribute(localName, value);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * )
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeCData(java.lang.String)
	 */
	@Override
	public void writeCData(String data) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeCData(data);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeCharacters(char[], int, int)
	 */
	@Override
	public void writeCharacters(char[] text, int start, int len) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeCharacters(text, start, len);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeCharacters(java.lang.String)
	 */
	@Override
	public void writeCharacters(String text) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeCharacters(text);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeDefaultNamespace(java.lang.String)
	 */
	@Override
	public void writeDefaultNamespace(String namespaceURI) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeDefaultNamespace(namespaceURI);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeEntityRef(java.lang.String)
	 */
	@Override
	public void writeEntityRef(String name) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeEntityRef(name);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeNamespace(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeNamespace(String prefix, String namespaceURI) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeNamespace(prefix, namespaceURI);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeProcessingInstruction(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeProcessingInstruction(String target, String data) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeProcessingInstruction(target, data);
		empty = false;
	}

	/**
	 * This will throw an {@link SoapStreamException} if the writer is on the root level.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeProcessingInstruction(java.lang.String)
	 */
	@Override
	public void writeProcessingInstruction(String target) throws XMLStreamException {
		throwExceptionIfUnderRoot();
		super.writeProcessingInstruction(target);
		empty = false;
	}
	
	/**
	 * This will aleays throw an {@link SoapStreamException}.
	 * 
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeDTD(java.lang.String)
	 */
	@Override
	public void writeDTD(String dtd) throws XMLStreamException {
		throw NOT_ALLOWED_ELEMENT_AT_ROOT_LEVEL;
	}

	// delegated writing operations
	
	/**
	 * @see org.evolvis.scio.soap.DemandQualifiedRootWriter#writeStartElement(java.lang.String)
	 */
	@Override
	public void writeStartElement(String localName) throws XMLStreamException {
		super.writeStartElement(localName);
		empty = false;
	}
	
	/**
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeEmptyElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void writeEmptyElement(String prefix, String localName, String namespaceURI) throws XMLStreamException {
		super.writeEmptyElement(prefix, localName, namespaceURI);
		empty = false;
	}

	/**
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeEmptyElement(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeEmptyElement(String namespaceURI, String localName) throws XMLStreamException {
		super.writeEmptyElement(namespaceURI, localName);
		empty = false;
	}

	/**
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeEmptyElement(java.lang.String)
	 */
	@Override
	public void writeEmptyElement(String localName) throws XMLStreamException {
		super.writeEmptyElement(localName);
		empty = false;
	}

	/**
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeEndElement()
	 */
	@Override
	public void writeEndElement() throws XMLStreamException {
		super.writeEndElement();
		empty = false;
	}

	/**
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeStartElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void writeStartElement(String prefix, String localName, String namespaceURI) throws XMLStreamException {
		super.writeStartElement(prefix, localName, namespaceURI);
		empty = false;
	}

	/**
	 * @see org.evolvis.scio.util.XMLSubtreeWriter#writeStartElement(java.lang.String, java.lang.String)
	 */
	@Override
	public void writeStartElement(String namespaceURI, String localName) throws XMLStreamException {
		super.writeStartElement(namespaceURI, localName);
		empty = false;
	}
}
