/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.soap;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.evolvis.scio.soap.SoapConstants;
import org.evolvis.scio.soap.SoapFault;
import org.evolvis.scio.soap.SoapStreamException;
import org.evolvis.scio.soap.SoapFault.Reason;
import org.evolvis.scio.util.XMLSubtreeReader;

/**
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class SoapStreamReader implements SoapConstants {
	
	private XMLStreamReader reader;
	private SoapVersion soapVersion;
	private String sver; /** same as the namespace of the above variable {@link #soapVersion}
	                         (because its needed very often). */ 
	
	private SoapFaultFactory faultFactory;
	private SoapFault fault;
	
	private Position position = Position.START;
	private enum Position {
		START,               /** read nothing */
		ENVELOPE,            /** positioned on the envelope start element */
		HEADER_BLOCK,        /** positioned on a start element which is a child of the header element */
		BODY,                /** positioned on the body start element */
		FIRST_BODY_BLOCK,    /** positioned on the first start element which is child of the body element */
		BODY_BLOCK,          /** positioned on a start element which is a child of the body and
		                         which is not a fault */
		END                  /** stream is closed */
	};

	private static final Logger logger = Logger.getLogger(SoapStreamReader.class.getName());
	
	public SoapStreamReader(XMLStreamReader reader) {
		this(reader, null);
	}
	
	public SoapStreamReader(XMLStreamReader reader, SoapFaultFactory faultFactory) {
		this.reader = reader;
		this.faultFactory = faultFactory == null ? new SoapFaultFactory() : faultFactory;
	}
	
	public SoapVersion getSoapVersion() throws XMLStreamException {
		readUntilEnvelope();
		return soapVersion;
	}
	
	private void skipPreviousHeaderBlock() throws XMLStreamException {
		if (headerBlockReader != null) {
			headerBlockReader.skipSubTree();
			reader.nextTag();
			if (reader.isStartElement())
				position = Position.HEADER_BLOCK;
			else {
				reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_HEADER);
				reader.nextTag();
				reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_BODY);
				position = Position.BODY;
			}
		}
		headerBlockReader = null;
	}
	
	private void skipPreviousBodyBlock() throws XMLStreamException {
		if (bodyBlockReader != null) {
			bodyBlockReader.skipSubTree();
			reader.nextTag();
			if (reader.isStartElement())
				position = Position.BODY_BLOCK;
			else
				closeBody();
		}
		bodyBlockReader = null;
	}

	/**
	 * If the current stream {@link #position} is {@link Position#HEADER_BLOCK} all header
	 * data will be skipped and the position will be changed to {@link Position#BODY}.
	 * 
	 * @throws XMLStreamException
	 */
	private void skipHeader() throws XMLStreamException {
		if (position == Position.HEADER_BLOCK) {
			for (int depth = 1; depth >= 0; ) { // ignore all header data
				switch (reader.next()) {
					case XMLStreamConstants.START_ELEMENT:
						depth++;break;
					case XMLStreamConstants.END_ELEMENT:
						depth--;break;
				}
			}
			reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_HEADER);
			reader.nextTag();
			reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_BODY);
			position = Position.BODY;
		}
	}

	private HeaderBlockReader headerBlockReader;
	private HeaderBlockReader preloadedHeaderBlockReader;
	private BodyBlockReader bodyBlockReader;
	private BodyBlockReader preloadedBodyBlockReader;
	

	public HeaderBlockReader nextHeaderBlock() throws XMLStreamException {
		if (preloadedHeaderBlockReader != null) {
			headerBlockReader = preloadedHeaderBlockReader;
			preloadedHeaderBlockReader = null;
			return headerBlockReader;
		}
		readUntilHeader();
		if (position == Position.HEADER_BLOCK)
			skipPreviousHeaderBlock();
		if (position == Position.HEADER_BLOCK)
			headerBlockReader = new HeaderBlockReader(reader, soapVersion);
		else
			throw new NoSuchElementException("header element is already closed");
		return headerBlockReader;
	}
	
	private void closeBody() throws XMLStreamException {
		reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_BODY);
		reader.nextTag();
		reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_ENVELOPE);
		reader.close();
		position = Position.END;
	}
	
	public BodyBlockReader nextBodyBlock() throws XMLStreamException {
		if (preloadedBodyBlockReader != null) {
			bodyBlockReader = preloadedBodyBlockReader;
			preloadedBodyBlockReader = null;
			return bodyBlockReader;
		}
		readUntilFirstBodyBlock();
		if (position == Position.FIRST_BODY_BLOCK || position == Position.BODY_BLOCK)
			skipPreviousBodyBlock();
		if (position == Position.FIRST_BODY_BLOCK || position == Position.BODY_BLOCK)
			bodyBlockReader = new BodyBlockReader(reader, soapVersion);
		else
			throw new NoSuchElementException("body element is already closed");
		return bodyBlockReader;
	}
	
	public boolean hasNextHeaderBlock() throws XMLStreamException {
		readUntilHeader();
		if (position == Position.HEADER_BLOCK) {
			try {
				preloadedHeaderBlockReader = nextHeaderBlock();
				return true;
			} catch (NoSuchElementException e) {
				// do nothing
			}
		}
		return false;
	}
		
	public boolean hasNextBodyBlock() throws XMLStreamException {
		readFault();
		if (position == Position.FIRST_BODY_BLOCK || position == Position.BODY_BLOCK) {
			try {
				preloadedBodyBlockReader = nextBodyBlock();
				return true;
			} catch (NoSuchElementException e) {
				// do nothing
			}
		}
		return false;
	}
	
	public boolean hasFault() throws XMLStreamException {
		readFault();
		return fault != null;
	}
	
	public SoapFault getFault() throws XMLStreamException {
		readFault();
		return fault;
	}

	/**
	 * If nothing is read before, the first element of the stream will be read.
	 * This must be the envelope start element.
	 * The {@link #position} is changed from {@link Position#START} to {@link Position#ENVELOPE}.
	 * The SOAP version which is specified by the namespace of the envelope element is
	 * read.
	 * 
	 * @throws  XMLStreamException
	 *          If the root element does not have the localname {@link SoapConstants#ELEMENT_NAME_ENVELOPE}
	 *          or if the namespace of the envelope element is not available in the enumeration
	 *          {@link SoapVersion}.
	 */
	private void readUntilEnvelope() throws XMLStreamException {
		if (position == Position.START) {
			reader.nextTag();
			sver = reader.getNamespaceURI();
			reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_ENVELOPE);
			soapVersion = SoapVersion.valueOfNamespace(sver);
			if (soapVersion == null)
				throw new SoapStreamException("unknown SOAP version '"+sver+"'");
			if (soapVersion != SoapVersion.SOAP_1_2)
				logger.info("only SOAP version '"+SoapVersion.SOAP_1_2.getNamespace()+"' "
						+"is implemented right now");
			//	throw new SoapStreamException("only SOAP version '"+SoapVersion.SOAP_1_2.getNamespace()+"' "+
			//			"is implemented right now");
			position = Position.ENVELOPE;
		}
	}

	/**
	 * If the readers position is before the header start element, it is read until
	 * the start element of the first header block is reached or if no header block is
	 * available it is read until the first body block start element is reached.
	 * If the {@link #position} is {@link Position#START} or {@link Position#ENVELOPE}
	 * it will be changed to {@link Position#HEADER_BLOCK} or {@link Position#BODY}.
	 * 
	 * @throws XMLStreamException
	 */
	private void readUntilHeader() throws XMLStreamException {
		if (position == Position.START)
			readUntilEnvelope();
		if (position == Position.ENVELOPE) {
			reader.nextTag();
			if (reader.getLocalName().equals(ELEMENT_NAME_HEADER) &&
					reader.getNamespaceURI().equals(sver)) { // on header start element
				reader.nextTag();
				if (reader.isStartElement()) // on first header block start element
					position = Position.HEADER_BLOCK;
				else { // on closing header end element => header is empty
					reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_HEADER);
					reader.next();
					reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_BODY);
					position = Position.BODY;
				}
			} else { // must be the body start element
				reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_BODY);
				position = Position.BODY;
			}
		}
	}
	
	/**
	 * Reads the SOAP stream until the body start element is reached.
	 * If the stream contains header blocks, then they will be skipped.
	 * If the {@link #position} is {@link Position#START}, {@link Position#ENVELOPE} or
	 * {@link Position#HEADER_BLOCK} it will be changed to {@link Position#BODY}.
	 * 
	 * @throws XMLStreamException
	 */
	private void readUntilBody() throws XMLStreamException {
		readUntilHeader();
		if (position == Position.HEADER_BLOCK) {
			skipPreviousHeaderBlock();
			skipHeader();
		}
	}

	/**
	 * Reads the SOAP stream until the start element of the first element of the
	 * first body block is reached. If the body is empty the document will be closed.
	 * If the {@link #position} is {@link Position#START}, {@link Position#ENVELOPE},
	 * {@link Position#HEADER_BLOCK}or {@link Position#BODY} it will be changed to
	 * {@link Position#FIRST_BODY_BLOCK} or
	 * {@link Position#END}.
	 * 
	 * @throws XMLStreamException
	 */
	private void readUntilFirstBodyBlock() throws XMLStreamException {
		readUntilBody();
		if (position == Position.BODY) {
			reader.nextTag();
			if (reader.isStartElement()) { // on first body block
				position = Position.FIRST_BODY_BLOCK;
			} else // body is empty => end
				closeBody();
		}
	}
	
	/** TODO renew
	 * If the stream position is after the start element of the first body block element,
	 * nothing is done, in the other case all stream data will be skipped until this
	 * element is reached. If it is a SOAP fault element, all child elements are
	 * read until the first detail entry is reached. If the fault containes no detail
	 * information the stream is closed.
	 * If this operation read something it will change the {@link #position} to
	 * {@link Position#FIRST_BODY_BLOCK},
	 * {@link Position#FAULT_DETAIL_BLOCK} or
	 * {@link Position#END}.
	 * 
	 * @throws XMLStreamException
	 */
	private void readFault() throws XMLStreamException {
		readUntilFirstBodyBlock();
		if (position == Position.FIRST_BODY_BLOCK) {
			if (    reader.isStartElement() &&
					reader.getLocalName().equals(ELEMENT_NAME_FAULT) &&
					reader.getNamespaceURI().equals(sver)) { // on fault start element
				reader.nextTag();
				reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_FAULT_CODE);
				reader.nextTag();
				
				// read mandatory code value
				QName codeValue = readCodeValue();
				if (!codeValue.getNamespaceURI().equals(sver))
					throw new SoapStreamException("SOAP fault code value must have the namespace '"+sver+"'");
				SoapFaultCode faultCode = SoapFaultCode.valueOfId(codeValue.getLocalPart());
				if (faultCode == null)
					throw new SoapStreamException("unknown SOAP fault code '"+codeValue.getLocalPart()+"'");
				
				// read optional subcodes
				LinkedList<QName> faultSubCodeList = new LinkedList<QName>();
				int i = 0;
				
				System.out.println(reader.hasNext());
				System.out.println(reader.getName());
				//reader.nextTag();

				System.out.println(reader.hasNext());
				System.out.println(reader.getEventType());
				
				
				for (reader.nextTag(); reader.getEventType() == XMLStreamConstants.START_ELEMENT; reader.nextTag(), i++) { // new subcode element
					reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_FAULT_SUBCODE);
					reader.nextTag();
					codeValue = readCodeValue();
					faultSubCodeList.add(codeValue);
				}
				// read all subcode end elements
				while (i --> 0) { 
					reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_FAULT_SUBCODE);
					reader.nextTag();
				}
				// read code end element
				reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_FAULT_CODE);
				// create fault object
				fault = faultFactory.createSoapFault(faultCode, faultSubCodeList);

				// read reasons (min 1)
				reader.nextTag();
				reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_FAULT_REASON);
				reader.nextTag();
				do {
					reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_FAULT_REASON_TEXT);
					String lang = reader.getAttributeValue(NAMESPACE_XML_NAMESPACE, ATTRIBUTE_NAME_FAULT_REASON_TEXT_LANGUAGE);
					String message = reader.getElementText();
					fault.addReason(new Reason(lang, message));
					reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_FAULT_REASON_TEXT);
					reader.nextTag();
				} while (reader.getEventType() == XMLStreamConstants.START_ELEMENT); // another text element ?
				reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_FAULT_REASON);
				reader.nextTag();
				
				// read optional node
				if (reader.getEventType() == XMLStreamConstants.START_ELEMENT &&
						ELEMENT_NAME_FAULT_NODE.equals(reader.getLocalName()) &&
						sver.equals(reader.getNamespaceURI())) {
					fault.setNode(reader.getElementText());
					reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_FAULT_NODE);
					reader.nextTag();
				}
				
				// read optional role
				if (reader.getEventType() == XMLStreamConstants.START_ELEMENT &&
						ELEMENT_NAME_FAULT_ROLE.equals(reader.getLocalName()) &&
						sver.equals(reader.getNamespaceURI())) {
					fault.setRole(reader.getElementText());
					reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_FAULT_ROLE);
					reader.nextTag();
				}
				
				// read optional detail entries
				if (reader.getEventType() == XMLStreamConstants.START_ELEMENT &&
						ELEMENT_NAME_FAULT_DETAIL.equals(reader.getLocalName()) &&
						sver.equals(reader.getNamespaceURI())) {
					reader.nextTag();

					while (reader.isStartElement()) {
						XMLSubtreeReader detailReader = new XMLSubtreeReader(reader);
						fault.readDetailEntry(detailReader);
						detailReader.skipSubTree();
						reader.nextTag();
					}

					reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_FAULT_DETAIL);
					reader.nextTag();
				}
				reader.require(XMLStreamConstants.END_ELEMENT, sver, ELEMENT_NAME_FAULT);
				reader.nextTag();
				closeBody();
			}
		}
	}

	/**
	 * If the position of the stream is on a fault code value start element,
	 * then the fault code value will be returned.
	 * If the parent of this value is the code element, the returned local name of the
	 * value must be one element of {@link SoapFaultCode}.
	 * 
	 * @return The fault code value.
	 * @throws XMLStreamException
	 *         If the stream is not positioned on a SOAP fault code value start element, or
	 *         if the text value has no prefix or if its namespace is not set.
	 */
	private QName readCodeValue() throws XMLStreamException {
		reader.require(XMLStreamConstants.START_ELEMENT, sver, ELEMENT_NAME_FAULT_CODE_VALUE);
		String value = reader.getElementText();
		int i = value.indexOf(XML_PREFIX_SEPARATOR);
		if (i == -1)
			throw new SoapStreamException("SOAP fault code value must have a prefix"); //TODO: maybe use default namespace instead?
		String vns = reader.getNamespaceContext().getNamespaceURI(value.substring(0, i)); // get namespace of prefix
		if (vns == null)
			throw new SoapStreamException("SOAP fault code value must have a namespace");
		value = value.substring(i+1); // remove prefix
		return new QName(vns, value);
	}
}
