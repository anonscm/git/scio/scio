/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.util;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * Encapsulates an instance of {@link XMLStreamWriter} and ensures, that no
 * xml element which is written before is closed.
 * This can be useful if an instance of {@link XMLStreamWriter} has to be passed
 * and it has to be assured that the xml data which is already written is not affected.
 * It can also be useful to close all xml elements which are opened by some instance
 * which out of the responsibility of the class which uses the encapsulated {@link XMLStreamWriter}.
 * The operatins
 * {@link #close()},
 * {@link #writeEndDocument()},
 * {@link #writeStartDocument()},
 * {@link #writeStartDocument(String)} and
 * {@link #writeStartDocument(String, String)}
 * are not allowed and will throw an {@link XMLStreamException} if they are called.
 * All of the other operations are delegated to the encapsulated {@link XMLStreamWriter} instance.
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class XMLSubtreeWriter implements XMLStreamWriter {
	
	protected XMLStreamWriter delegate;
	protected int depth = 0;

	/**
	 * Creates an instance of {@link XMLStreamWriter} which delegates most of the operaions
	 * to the specified encapsulated instance. The access to the operations which write
	 * tags is controlled by this class.
	 * 
	 * @param   delegate
	 *          The encapsulated instance which will be used to delegate the called
	 *          operations to.
	 */
	public XMLSubtreeWriter(XMLStreamWriter delegate) {
		this.delegate = delegate;
	}
	
	// added operations

	/**
	 * Closes all elements which are opened by this {@link XMLSubtreeWriter} via
	 * the {@link XMLStreamWriter#writeEndElement()} method.
	 * The operation {@link #getSubtreeDepth()} will return 0 if it is called after this operation.
	 * 
	 * @throws XMLStreamException
	 */
	public void writeAllSubtreeEndElements() throws XMLStreamException {
		while (depth > 0)
			writeEndElement();
	}
	
	/**
	 * Returns the current depth of the subtree. The initial depth is 0.
	 * 
	 * @return  The current depth of the subtree
	 */
	public int getSubtreeDepth() {
		return depth;
	}
	
	// operations which affect the subtree depth

	/**
	 * Writes the end tag of the last element which is opened by this instance.
	 * This will decrease the subtree depth by 1.
	 * 
	 * @throws  XMLStreamException
	 *          If no element which is written by this instance is open at the moment.
	 *          This is incurred if the {@link #getSubtreeDepth()} method returns 0.
	 * 
	 * @see javax.xml.stream.XMLStreamWriter#writeEndElement()
	 * @see #getSubtreeDepth()
	 */
	public void writeEndElement() throws XMLStreamException {
		if (depth == 0)
			throw new XMLStreamException("It is not allowed end a superior xml element for an xml subtree writer");
		delegate.writeEndElement();
		depth --;
	}
	
	/**
	 * Writes the specified start element.
	 * This will increase the subtree depth by 1.
	 * 
	 * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String)
	 * @see #getSubtreeDepth()
	 */
	public void writeStartElement(String localName) throws XMLStreamException {
		delegate.writeStartElement(localName);
		depth ++;
	}

	/**
	 * Writes the specified start element.
	 * This will increase the subtree depth by 1.
	 * 
	 * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String, java.lang.String)
	 * @see #getSubtreeDepth()
	 */
	public void writeStartElement(String namespaceURI, String localName)
			throws XMLStreamException {
		delegate.writeStartElement(namespaceURI, localName);
		depth ++;
	}

	/**
	 * Writes the specified start element.
	 * This will increase the subtree depth by 1.
	 * 
	 * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String, java.lang.String, java.lang.String)
	 * @see #getSubtreeDepth()
	 */
	public void writeStartElement(String prefix, String localName,
			String namespaceURI) throws XMLStreamException {
		delegate.writeStartElement(prefix, localName, namespaceURI);
		depth ++;
	}
	
	// operations which are functionless
	
	/**
	 * Closes any start tags and writes corresponding end tags.
	 * 
	 * @see javax.xml.stream.XMLStreamWriter#close()
	 * @see #writeAllSubtreeEndElements()
	 */
	public void close() throws XMLStreamException {
		writeAllSubtreeEndElements();
	}

	/**
	 * Closes any start tags and writes corresponding end tags.
	 * 
	 * @see javax.xml.stream.XMLStreamWriter#writeEndDocument()
	 * @see #writeAllSubtreeEndElements()
	 */
	public void writeEndDocument() throws XMLStreamException {
		writeAllSubtreeEndElements();
	}

	/**
	 * This operation is functionless.
	 * 
	 * @see javax.xml.stream.XMLStreamWriter#writeStartDocument()
	 */
	public void writeStartDocument() throws XMLStreamException {
		// do nothing
	}

	/**
	 * This operation is functionless.
	 * 
	 * @see javax.xml.stream.XMLStreamWriter#writeStartDocument(java.lang.String)
	 */
	public void writeStartDocument(String version) throws XMLStreamException {
		// do nothing
	}

	/**
	 * This operation is functionless.
	 * 
	 * @see javax.xml.stream.XMLStreamWriter#writeStartDocument(java.lang.String, java.lang.String)
	 */
	public void writeStartDocument(String encoding, String version)
			throws XMLStreamException {
		// do nothing
	}
	
	// delegating operations
	
	/**
	 * @see javax.xml.stream.XMLStreamWriter#flush()
	 */
	public void flush() throws XMLStreamException {
		delegate.flush();
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#getNamespaceContext()
	 */
	public NamespaceContext getNamespaceContext() {
		return delegate.getNamespaceContext();
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#getPrefix(java.lang.String)
	 */
	public String getPrefix(String uri) throws XMLStreamException {
		return delegate.getPrefix(uri);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#getProperty(java.lang.String)
	 */
	public Object getProperty(String name) throws IllegalArgumentException {
		return delegate.getProperty(name);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#setDefaultNamespace(java.lang.String)
	 */
	public void setDefaultNamespace(String uri) throws XMLStreamException {
		delegate.setDefaultNamespace(uri);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#setNamespaceContext(javax.xml.namespace.NamespaceContext)
	 */
	public void setNamespaceContext(NamespaceContext context)
			throws XMLStreamException {
		delegate.setNamespaceContext(context);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#setPrefix(java.lang.String, java.lang.String)
	 */
	public void setPrefix(String prefix, String uri) throws XMLStreamException {
		delegate.setPrefix(prefix, uri);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String, java.lang.String)
	 */
	public void writeAttribute(String localName, String value)
			throws XMLStreamException {
		delegate.writeAttribute(localName, value);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void writeAttribute(String namespaceURI, String localName,
			String value) throws XMLStreamException {
		delegate.writeAttribute(namespaceURI, localName, value);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void writeAttribute(String prefix, String namespaceURI,
			String localName, String value) throws XMLStreamException {
		delegate.writeAttribute(prefix, namespaceURI, localName, value);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeCData(java.lang.String)
	 */
	public void writeCData(String data) throws XMLStreamException {
		delegate.writeCData(data);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeCharacters(java.lang.String)
	 */
	public void writeCharacters(String text) throws XMLStreamException {
		delegate.writeCharacters(text);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeCharacters(char[], int, int)
	 */
	public void writeCharacters(char[] text, int start, int len)
			throws XMLStreamException {
		delegate.writeCharacters(text, start, len);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeComment(java.lang.String)
	 */
	public void writeComment(String data) throws XMLStreamException {
		delegate.writeComment(data);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeDTD(java.lang.String)
	 */
	public void writeDTD(String dtd) throws XMLStreamException {
		delegate.writeDTD(dtd);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeDefaultNamespace(java.lang.String)
	 */
	public void writeDefaultNamespace(String namespaceURI)
			throws XMLStreamException {
		delegate.writeDefaultNamespace(namespaceURI);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String)
	 */
	public void writeEmptyElement(String localName) throws XMLStreamException {
		delegate.writeEmptyElement(localName);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String, java.lang.String)
	 */
	public void writeEmptyElement(String namespaceURI, String localName)
			throws XMLStreamException {
		delegate.writeEmptyElement(namespaceURI, localName);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void writeEmptyElement(String prefix, String localName,
			String namespaceURI) throws XMLStreamException {
		delegate.writeEmptyElement(prefix, localName, namespaceURI);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeEntityRef(java.lang.String)
	 */
	public void writeEntityRef(String name) throws XMLStreamException {
		delegate.writeEntityRef(name);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeNamespace(java.lang.String, java.lang.String)
	 */
	public void writeNamespace(String prefix, String namespaceURI)
			throws XMLStreamException {
		delegate.writeNamespace(prefix, namespaceURI);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeProcessingInstruction(java.lang.String)
	 */
	public void writeProcessingInstruction(String target)
			throws XMLStreamException {
		delegate.writeProcessingInstruction(target);
	}

	/**
	 * @see javax.xml.stream.XMLStreamWriter#writeProcessingInstruction(java.lang.String, java.lang.String)
	 */
	public void writeProcessingInstruction(String target, String data)
			throws XMLStreamException {
		delegate.writeProcessingInstruction(target, data);
	}
}
