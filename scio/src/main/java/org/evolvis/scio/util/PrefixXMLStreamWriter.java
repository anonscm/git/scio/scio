/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.util;

import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * This class can be used to force an existing instance of {@link XMLStreamWriter} to support
 * the property {@value #PROPERTY_IS_PREFIX_DEFAULTING}.
 * This feature is necessary if you want to use one {@link XMLStreamWriter} in different contexts
 * and don't want to redefine already specified namespaces.
 * If an {@link XMLStreamWriter} supports this property the namespaces are written automatically
 * and the method {@link #writeNamespace(String, String)} has not to be called.
 * If a namespace prefix is needed which is not specified before, then a new prefix will be generated.
 *
 * For exmaple if you have an instance <code>writer</code> of {@link XMLStreamWriter} and use the following code:
 *
 * <p><blockquote><pre>
 *     writer = PrefixXMLStreamWriter.enablePrefixDefaulting(writer); // enable property {@value #PROPERTY_IS_PREFIX_DEFAULTING}
 *     // writer.getProperty(PrefixXMLStreamWriter.PROPERTY_IS_PREFIX_DEFAULTING) == Boolean.TRUE
 *
 *     writer.writeStartDocument();
 *     writer.setPrefix("c", "http://schemas.xmlsoap.org/soap/encoding/");
 *     String env = "http://schemas.xmlsoap.org/soap/envelope/"
 *     writer.writeStartElement(env, "Envelope");
 *     writer.writeEmptyElement(env, "Header");
 *     writer.writeEmptyElement(env, "Body");
 *     writer.writeEndElement();
 *     writer.writeEndDocument();
 * </pre></blockquote>
 *
 * This XML-Stream will be created:
 *
 * <p><blockquote><pre>
 * &lt;?xml version='1.0'?&gt;
 * &lt;n0:Envelope xmlns:n0="http://schemas.xmlsoap.org/soap/envelope/" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/"&gt;
 *     &lt;n0:Header/&gt;
 *     &lt;n0:Body/&gt;
 * &lt;/n0:Envelope&gt;
 * </pre></blockquote>
 *
 *
 * @autor Hendrik Helwich, tarent GmbH
 */
public class PrefixXMLStreamWriter implements XMLStreamWriter {
    /**
     * This property is supported by this {@link XMLStreamWriter}.
     * {@link #getProperty(String)} called with this property will allways return {@link Boolean#TRUE}.
     * It can be assured that a {@link XMLStreamWriter} supports this property if the method
     * {@link #enablePrefixDefaulting(XMLStreamWriter)} is called.
     */
    public static final String PROPERTY_IS_PREFIX_DEFAULTING = "javax.xml.stream.isPrefixDefaulting";
    private static final String PREFIX_NAMESPACE_PREFIX = "n"; // prefix used to genereate namespaces (prefix + number)
    private XMLStreamWriter writer; // the wrapped writer which type is not this classtype 
    private int namespaceCounter = 0; // number used for the first generated namespace (PREFIX_NAMESPACE_PREFIX + namespaceCounter)

    /**
     * Remember all prefixes set via the {@link #setPrefix(String, String)} method to write
     * them away and clear it if one of the following methods are called:
     * {@link #writeStartElement(String)},
     * {@link #writeStartElement(String, String)},
     * {@link #writeStartElement(String, String, String)},
     * {@link #writeEmptyElement(String)},
     * {@link #writeEmptyElement(String, String)},
     * {@link #writeEmptyElement(String, String, String)},
     * {@link #writeAttribute(String, String)},
     * {@link #writeAttribute(String, String, String)},
     * {@link #writeAttribute(String, String, String, String)}.
     *
     * The list will also be cleared if the method
     * {@link #writeEndElement()}
     * is called.
     */
    private List<String> prefixList = new LinkedList<String>();

    /**
     * Create an instance of {@link PrefixXMLStreamWriter} which holds an instance of {@link XMLStreamWriter}.
     * This constructor is only used by the method {@link #enablePrefixDefaulting(XMLStreamWriter)}
     * which assures that the specified {@link XMLStreamWriter} is not an instance of {@link PrefixXMLStreamWriter}.
     *
     * @param   writer
     */
    private PrefixXMLStreamWriter(XMLStreamWriter writer) {
        this.writer = writer;
    }

    /**
     * The returned {@link XMLStreamWriter} is forced to support the property {@value #PROPERTY_IS_PREFIX_DEFAULTING}.
     * If the given {@link XMLStreamWriter} already supports this property it is returned by this method
     * otherwise an instance of {@link PrefixXMLStreamWriter} is created which is wrapping the given
     * {@link XMLStreamWriter} and adding the desired functionality.
     *
     * @param   writer
     *          Any {@link XMLStreamWriter}.
     * @return  An {@link XMLStreamWriter} which supports the property {@value #PROPERTY_IS_PREFIX_DEFAULTING}
     *          and has the same state as the given {@link XMLStreamWriter}.
     */
    public static XMLStreamWriter enablePrefixDefaulting(XMLStreamWriter writer) {
        if (isPrefixDefaulting(writer)) {
            // the specified writer already supports "prefix defaulting" => return 
            return writer;
        } else {
            return new PrefixXMLStreamWriter(writer);
        }
    }

    /**
     * Checks if the given {@link XMLStreamWriter} is supporting the property
     * {@value #PROPERTY_IS_PREFIX_DEFAULTING}.
     *
     * @param   writer
     * @return  <code>true</code> if the given {@link XMLStreamWriter} is supporting the
     *          property {@value #PROPERTY_IS_PREFIX_DEFAULTING}.
     */
    public static boolean isPrefixDefaulting(XMLStreamWriter writer) {
        try {
            return writer instanceof PrefixXMLStreamWriter ||
            Boolean.TRUE.equals(writer.getProperty(
                    PROPERTY_IS_PREFIX_DEFAULTING));
        } catch (IllegalArgumentException e) { // property is not supported

            return false;
        }
    }

    /**
     * The interface {@link XMLStreamWriter} does not offer the functionality to specify
     * a namespace for an attribute value or an element text node. This is needed for example
     * if you want to specify an XML-Schema simple type.
     * If the given namespace is not defined at the state of the given {@link XMLStreamWriter},
     * a new namespace prefix will be created and the namespace will be written to the {@link XMLStreamWriter}.
     *
     * @param  writer
     *         The {@link XMLStreamWriter} which should be used to write the namespace.
     * @param  namespaceURI
     *         The namespace URI which should be used for the value.
     * @param  value
     *         The value of the attribute or the element text node
     * @return The value with the created namespace prefix.
     * @throws XMLStreamException
     */
    public static String writeValueNamespace(XMLStreamWriter writer,
        String namespaceURI, String value) throws XMLStreamException {
        String prefix = writer.getPrefix(namespaceURI);

        if (prefix == null) { // value namespace is undefined
            prefix = getNextNamespacePrefix(writer);
            writer.setPrefix(prefix, namespaceURI);
            writer.writeNamespace(prefix, namespaceURI);
        }

        return prefix + ":" + value;
    }

    /**
     * Creates a new unused namespace prefix.
     *
     * @return a new unused namespace prefix
     */
    private String getNextNamespacePrefix() {
        String prefix;

        do {
            prefix = PREFIX_NAMESPACE_PREFIX + namespaceCounter++;
        } while (null != writer.getNamespaceContext().getNamespaceURI(prefix));

        return prefix;
    }

    /**
     * The same method like {@link #getNextNamespacePrefix()} which also works for generic
     * {@link XMLStreamWriter}s.
     *
     * @param  writer
     * @return a new unused namespace prefix
     */
    private static String getNextNamespacePrefix(XMLStreamWriter writer) {
        if (writer instanceof PrefixXMLStreamWriter) {
            return ((PrefixXMLStreamWriter) writer).getNextNamespacePrefix();
        }

        int namespaceCounter = 0;
        String prefix;

        do {
            prefix = PREFIX_NAMESPACE_PREFIX + namespaceCounter++;
        } while (null != writer.getNamespaceContext().getNamespaceURI(prefix));

        return prefix;
    }

    /**
     * Writes all namespaces which have been set via the {@link #setPrefix(String, String)} method
     * but which have not been written via the {@link #writeNamespace(String, String)} method.
     *
     * @throws XMLStreamException
     */
    private void writeNamespaces() throws XMLStreamException {
        for (String prefix : prefixList) {
            String uri = writer.getNamespaceContext().getNamespaceURI(prefix);
            writer.writeNamespace(prefix, uri);
        }

        prefixList.clear();
    }

    // ----------------- functions of javax.xml.stream.XMLStreamWriter

    /**
     * @see javax.xml.stream.XMLStreamWriter#getProperty(java.lang.String)
     */
    public Object getProperty(String name) throws IllegalArgumentException {
        if (PROPERTY_IS_PREFIX_DEFAULTING.equals(name)) {
            return Boolean.TRUE;
        } else {
            return writer.getProperty(name);
        }
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String, java.lang.String)
     */
    public void writeStartElement(String namespaceURI, String localName)
        throws XMLStreamException {
        String prefix = writer.getPrefix(namespaceURI);

        if (prefix == null) { // namespace is undefined
            prefix = getNextNamespacePrefix();
            writer.setPrefix(prefix, namespaceURI);
            writer.writeStartElement(namespaceURI, localName);
            writeNamespace(prefix, namespaceURI);
        } else {
            writer.writeStartElement(namespaceURI, localName);
        }

        writeNamespaces();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String)
     */
    public void writeStartElement(String localName) throws XMLStreamException {
        writer.writeStartElement(localName);
        writeNamespaces();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartElement(java.lang.String, java.lang.String, java.lang.String)
     */
    public void writeStartElement(String prefix, String localName,
        String namespaceURI) throws XMLStreamException {
        writer.writeStartElement(prefix, localName, namespaceURI);
        writeNamespaces();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEndElement()
     */
    public void writeEndElement() throws XMLStreamException {
        writer.writeEndElement();
        prefixList.clear();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String, java.lang.String)
     */
    public void writeEmptyElement(String namespaceURI, String localName)
        throws XMLStreamException {
        writeStartElement(namespaceURI, localName);
        writeEndElement();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String)
     */
    public void writeEmptyElement(String localName) throws XMLStreamException {
        writeStartElement(localName);
        writeEndElement();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEmptyElement(java.lang.String, java.lang.String, java.lang.String)
     */
    public void writeEmptyElement(String prefix, String localName,
        String namespaceURI) throws XMLStreamException {
        writeStartElement(prefix, localName, namespaceURI);
        writeEndElement();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String, java.lang.String, java.lang.String)
     */
    public void writeAttribute(String namespaceURI, String localName,
        String value) throws XMLStreamException {
        String prefix = writer.getPrefix(namespaceURI);

        if (prefix == null) { // namespace is undefined
            prefix = getNextNamespacePrefix();
            writer.setPrefix(prefix, namespaceURI);
            writeNamespace(prefix, namespaceURI);
        }

        writeAttribute(prefix, namespaceURI, localName, value);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public void writeAttribute(String prefix, String namespaceURI,
        String localName, String value) throws XMLStreamException {
        writeNamespaces();
        writer.writeAttribute(prefix, namespaceURI, localName, value);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeAttribute(java.lang.String, java.lang.String)
     */
    public void writeAttribute(String localName, String value)
        throws XMLStreamException {
        writeNamespaces();
        writer.writeAttribute(localName, value);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#setPrefix(java.lang.String, java.lang.String)
     */
    public void setPrefix(String prefix, String uri) throws XMLStreamException {
        prefixList.add(prefix);
        writer.setPrefix(prefix, uri);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeNamespace(java.lang.String, java.lang.String)
     */
    public void writeNamespace(String prefix, String namespaceURI)
        throws XMLStreamException {
        // prefix.equals(getPrefix(namespaceURI)) cannot be used to check if the 
    	// namespace is already written!
        prefixList.remove(prefix);
        writer.writeNamespace(prefix, namespaceURI);
    }

    // ----------------- forwarding functions

    /**
     * @see javax.xml.stream.XMLStreamWriter#close()
     */
    public void close() throws XMLStreamException {
        writer.close();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#flush()
     */
    public void flush() throws XMLStreamException {
        writer.flush();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#getNamespaceContext()
     */
    public NamespaceContext getNamespaceContext() {
        return writer.getNamespaceContext();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#getPrefix(java.lang.String)
     */
    public String getPrefix(String uri) throws XMLStreamException {
        return writer.getPrefix(uri);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#setDefaultNamespace(java.lang.String)
     */
    public void setDefaultNamespace(String uri) throws XMLStreamException {
        writer.setDefaultNamespace(uri);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#setNamespaceContext(javax.xml.namespace.NamespaceContext)
     */
    public void setNamespaceContext(NamespaceContext context)
        throws XMLStreamException {
        writer.setNamespaceContext(context);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeCData(java.lang.String)
     */
    public void writeCData(String data) throws XMLStreamException {
        writer.writeCData(data);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeCharacters(java.lang.String)
     */
    public void writeCharacters(String text) throws XMLStreamException {
        writer.writeCharacters(text);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeCharacters(char[], int, int)
     */
    public void writeCharacters(char[] text, int start, int len)
        throws XMLStreamException {
        writer.writeCharacters(text, start, len);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeComment(java.lang.String)
     */
    public void writeComment(String data) throws XMLStreamException {
        writer.writeComment(data);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeDTD(java.lang.String)
     */
    public void writeDTD(String dtd) throws XMLStreamException {
        writer.writeDTD(dtd);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeDefaultNamespace(java.lang.String)
     */
    public void writeDefaultNamespace(String namespaceURI)
        throws XMLStreamException {
        writer.writeDefaultNamespace(namespaceURI);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEndDocument()
     */
    public void writeEndDocument() throws XMLStreamException {
        writer.writeEndDocument();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeEntityRef(java.lang.String)
     */
    public void writeEntityRef(String name) throws XMLStreamException {
        writer.writeEntityRef(name);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeProcessingInstruction(java.lang.String)
     */
    public void writeProcessingInstruction(String target)
        throws XMLStreamException {
        writer.writeProcessingInstruction(target);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeProcessingInstruction(java.lang.String, java.lang.String)
     */
    public void writeProcessingInstruction(String target, String data)
        throws XMLStreamException {
        writer.writeProcessingInstruction(target, data);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartDocument()
     */
    public void writeStartDocument() throws XMLStreamException {
        writer.writeStartDocument();
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartDocument(java.lang.String)
     */
    public void writeStartDocument(String version) throws XMLStreamException {
        writer.writeStartDocument(version);
    }

    /**
     * @see javax.xml.stream.XMLStreamWriter#writeStartDocument(java.lang.String, java.lang.String)
     */
    public void writeStartDocument(String encoding, String version)
        throws XMLStreamException {
        writer.writeStartDocument(encoding, version);
    }
}
