/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.util;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * Delegating XMLStreamReader implementation for robust handling of XML Subtrees.
 * This implementation ensures, that the underlaying reader will never read more 
 * than the current subtree.
 * 
 * @author Sebastian Mancke, tarent GmbH
 */
public class XMLSubtreeReader implements XMLStreamReader {
	
	private XMLStreamReader delegate;
	protected int depth = 0;
	
	public XMLSubtreeReader(XMLStreamReader delegate) {
		this.delegate = delegate;
	}
	
	/**
	 * Consumes the whole subtree. After this, 
	 * the reader is positioned at the end tag of the subtree.
	 * @throws XMLStreamException 
	 */
	public void skipSubTree() throws XMLStreamException {
		while (hasNext())
			next();		
	}
	
	public void close() throws XMLStreamException {
		// do nothing here
	}

	public boolean hasNext() throws XMLStreamException {
		return (depth >= 0);
	}
	
	public String getElementText() throws XMLStreamException {
		depth--;
		return delegate.getElementText();
	}

	public int next() throws XMLStreamException {
		if (depth < -1)
			throw new XMLStreamException("end of xml subtree has been reached");

		if (depth < 0) {
			depth--;
			return END_DOCUMENT;
		}

		int event = delegate.next();
		if (event == START_ELEMENT)
			depth++;
		else
			if (event == END_ELEMENT)
				depth--;

		return event;
	}

	public int nextTag() throws XMLStreamException {
		if (depth < -1)
			throw new XMLStreamException("end of xml subtree has reached");
	
		if (depth < 0) {
			depth--;
			return END_DOCUMENT;
		}

		int event = delegate.nextTag();
		if (event == START_ELEMENT)
			depth++;
		else
			if (event == END_ELEMENT)
				depth--;

		return event;
	}

	public int getEventType() {
		if (depth < -1)
			return END_DOCUMENT;
		return delegate.getEventType();
	}
	
	// auto generated delegate methods

	public int getAttributeCount() {
		return delegate.getAttributeCount();
	}

	public String getAttributeLocalName(int arg0) {
		return delegate.getAttributeLocalName(arg0);
	}

	public QName getAttributeName(int arg0) {
		return delegate.getAttributeName(arg0);
	}

	public String getAttributeNamespace(int arg0) {
		return delegate.getAttributeNamespace(arg0);
	}

	public String getAttributePrefix(int arg0) {
		return delegate.getAttributePrefix(arg0);
	}

	public String getAttributeType(int arg0) {
		return delegate.getAttributeType(arg0);
	}

	public String getAttributeValue(int arg0) {
		return delegate.getAttributeValue(arg0);
	}

	public String getAttributeValue(String arg0, String arg1) {
		return delegate.getAttributeValue(arg0, arg1);
	}

	public String getCharacterEncodingScheme() {
		return delegate.getCharacterEncodingScheme();
	}

	public String getEncoding() {
		return delegate.getEncoding();
	}

	public String getLocalName() {
		return delegate.getLocalName();
	}

	public Location getLocation() {
		return delegate.getLocation();
	}

	public QName getName() {
		return delegate.getName();
	}

	public NamespaceContext getNamespaceContext() {
		return delegate.getNamespaceContext();
	}

	public int getNamespaceCount() {
		return delegate.getNamespaceCount();
	}

	public String getNamespacePrefix(int arg0) {
		return delegate.getNamespacePrefix(arg0);
	}

	public String getNamespaceURI() {
		return delegate.getNamespaceURI();
	}

	public String getNamespaceURI(int arg0) {
		return delegate.getNamespaceURI(arg0);
	}

	public String getNamespaceURI(String arg0) {
		return delegate.getNamespaceURI(arg0);
	}

	public String getPIData() {
		return delegate.getPIData();
	}

	public String getPITarget() {
		return delegate.getPITarget();
	}

	public String getPrefix() {
		return delegate.getPrefix();
	}

	public Object getProperty(String arg0) throws IllegalArgumentException {
		return delegate.getProperty(arg0);
	}

	public String getText() {
		return delegate.getText();
	}

	public char[] getTextCharacters() {
		return delegate.getTextCharacters();
	}

	public int getTextCharacters(int arg0, char[] arg1, int arg2, int arg3)
			throws XMLStreamException {
		return delegate.getTextCharacters(arg0, arg1, arg2, arg3);
	}

	public int getTextLength() {
		return delegate.getTextLength();
	}

	public int getTextStart() {
		return delegate.getTextStart();
	}

	public String getVersion() {
		return delegate.getVersion();
	}

	public boolean hasName() {
		return delegate.hasName();
	}

	public boolean hasText() {
		return delegate.hasText();
	}

	public boolean isAttributeSpecified(int arg0) {
		return delegate.isAttributeSpecified(arg0);
	}

	public boolean isCharacters() {
		return delegate.isCharacters();
	}

	public boolean isEndElement() {
		return delegate.isEndElement();
	}

	public boolean isStandalone() {
		return delegate.isStandalone();
	}

	public boolean isStartElement() {
		return delegate.isStartElement();
	}

	public boolean isWhiteSpace() {
		return delegate.isWhiteSpace();
	}

	public void require(int arg0, String arg1, String arg2)
			throws XMLStreamException {
		delegate.require(arg0, arg1, arg2);
	}

	public boolean standaloneSet() {
		return delegate.standaloneSet();
	}
	
}
