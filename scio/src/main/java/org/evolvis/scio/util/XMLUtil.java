/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.util;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.XmlSerializable;
import org.evolvis.scio.bind.BindContext;
import org.evolvis.scio.bind.Marshaller;

public class XMLUtil {

	public static final String NS_XSD = "http://www.w3.org/2001/XMLSchema"; 
	public static final String NS_XSI = "http://www.w3.org/2001/XMLSchema-instance";
	public static final QName XSI_TYPE = new QName(NS_XSI, "type");
	public static final QName XSI_NIL = new QName(NS_XSI, "nil");
	
	public static final QName XSD_STRING = new QName(NS_XSD, "string");
	public static final QName XSD_DECIMAL = new QName(NS_XSD, "decimal");
	public static final QName XSD_INTEGER = new QName(NS_XSD, "integer");
	public static final QName XSD_FLOAT = new QName(NS_XSD, "float");
	public static final QName XSD_BOOLEAN = new QName(NS_XSD, "boolean");
	public static final QName XSD_DATE = new QName(NS_XSD, "date");
	public static final QName XSD_TIME = new QName(NS_XSD, "time");

	/**
	 * Returns the element XSI type of the current element.
	 * If no type is provided, the attribute name is used as type. 
	 * @param parser
	 * @return the type
	 */
	public static QName getElementType(XMLStreamReader parser) {
		String type = parser.getAttributeValue(XMLUtil.XSI_TYPE.getNamespaceURI(), XMLUtil.XSI_TYPE.getLocalPart());
		if (type == null)
			return parser.getName();
		int pos = type.indexOf(":");
		if (pos != -1) {
			return new QName(parser.getNamespaceContext().getNamespaceURI(type.substring(0, pos)),
						 	type.substring(pos+1));
		}
		return new QName(null, type);
	}
		
	/**
	 * @param reader
	 * @return
	 */
	public static boolean isNullElement(XMLStreamReader reader) {
		return "true".equalsIgnoreCase(reader.getAttributeValue(XMLUtil.XSI_NIL.getNamespaceURI(), XMLUtil.XSI_NIL.getLocalPart()));
	}
	
	public static boolean isTag(XMLStreamReader parser, QName qname) {
		return qname.equals(parser.getName());
	}

	public static boolean isTag(XMLStreamReader parser, String ns, String localName) {
		return ns.equals(parser.getNamespaceURI()) && localName.equals(parser.getLocalName());
	}
	
	/**
	 * Skips the complete subtree, the parser is positioned on.
	 * Precondition: StartElement, Postcondition: EndElement of the corresponding Element. 
	 * @param reader
	 * @throws XMLStreamException 
	 */
	public static void skipSubtree(XMLStreamReader reader) throws XMLStreamException {
		int deepth = 1;								
		while (deepth >= 1) {
			int event = reader.next();
			if (event == XMLStreamConstants.START_ELEMENT)
				deepth++;
			else if (event == XMLStreamConstants.END_ELEMENT)
				deepth--;										
		}
	}
	
	/**
	 * Pipes an xml subtree from an {@link XMLStreamReader} to an {@link XMLStreamWriter}.
	 * The given {@link XMLStreamReader} must be located on a start element node.
	 * It will be read until the corresponding end element is found.
	 * All data which is read will be written to the given {@link XMLStreamWriter}.
	 * 
	 * @param  reader
	 *         The {@link XMLStreamReader} from which the actual subtree is read.
	 * @param  writer
	 *         The {@link XMLStreamWriter} to which the actual subtree is written.
	 * @throws XMLStreamException
	 *         If the specified {@link XMLStreamReader} is not located on a start element node.
	 */
	public static void pipeXmlSubTree(XMLStreamReader reader, XMLStreamWriter writer) throws XMLStreamException {
		int event = reader.getEventType();
		if (event != XMLStreamConstants.START_ELEMENT)
			throw new XMLStreamException("the input stream must be located at a start element");
		int depth = 0;
		for (; reader.hasNext(); event = reader.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					writer.writeStartElement(reader.getPrefix(), reader.getLocalName(), reader.getNamespaceURI());
					for (int i=0, j=reader.getAttributeCount(); i<j; i++)
						writer.writeAttribute(reader.getAttributePrefix(i), reader.getAttributeNamespace(i),
								reader.getAttributeLocalName(i), reader.getAttributeValue(i));
					depth ++;
					break;
				case XMLStreamConstants.END_ELEMENT:
					writer.writeEndElement();
					if (-- depth <= 0)
						return;
					break;
				case XMLStreamConstants.PROCESSING_INSTRUCTION:
					writer.writeProcessingInstruction(reader.getPITarget(), reader.getPIData());
					break;
				case XMLStreamConstants.CHARACTERS:
					writer.writeCharacters(reader.getText());
					break;
				case XMLStreamConstants.COMMENT:
					writer.writeComment(reader.getText());
					break;
				case XMLStreamConstants.SPACE:
					writer.writeCharacters(reader.getText());
					break;
				/*case XMLStreamConstants.START_DOCUMENT:
					out.writeStartDocument(in.getEncoding(), in.getVersion());
					break;
				case XMLStreamConstants.END_DOCUMENT:
					out.writeEndDocument();
					return;*/
				case XMLStreamConstants.ENTITY_REFERENCE:
					writer.writeEntityRef(reader.getText());
					break;
				case XMLStreamConstants.ATTRIBUTE:
					// nothing to do
					break;
				case XMLStreamConstants.DTD:
					writer.writeDTD(reader.getText());
					break;
				case XMLStreamConstants.CDATA:
					writer.writeEntityRef(reader.getText());
					break;
				case XMLStreamConstants.NAMESPACE:
					for (int i=0, j=reader.getNamespaceCount(); i<j; i++)
						writer.writeNamespace(reader.getNamespacePrefix(i), reader.getNamespaceURI(i));
					break;
				case XMLStreamConstants.NOTATION_DECLARATION:
					throw new UnsupportedOperationException("please implement");
				case XMLStreamConstants.ENTITY_DECLARATION:
					throw new UnsupportedOperationException("please implement");
				default:
					throw new RuntimeException("unexpected even type: "+event); // this should be impossible
			}
		}
	}
	
	/**
	 * Creates a new instance of {@link XmlSerializable} which uses the jaxb marshaller
	 * {@link Marshaller} to write an xml serialisation.
	 * 
	 * @param  jaxbObject
	 *         The instance which will be marshalled.
	 * @return A wrapper class which can be used to serialize an instance with jaxb.
	 */
	public static XmlSerializable createXmlSerializable(BindContext bindContext, Object jaxbObject) {
		final Object object = jaxbObject;
		final BindContext context = bindContext;
		return new XmlSerializable() {
			public void writeXmlRepresentation(XMLStreamWriter writer) throws XMLStreamException {
				writer = PrefixXMLStreamWriter.enablePrefixDefaulting(writer);
				try {
					Marshaller marshaller = context.createMarshaller();
					marshaller.marshal(object, writer);
				} catch (JAXBException e) {
					throw new RuntimeException(e);
				}
			}
		};
	}
	
	@SuppressWarnings("unchecked")
	public static XmlSerializable createXmlSerializable(BindContext bindContext, QName qname, Object jaxbObject) {
		JAXBElement jaxbElement = new JAXBElement(qname, jaxbObject.getClass(), jaxbObject);
		return new JAXBSerializable(bindContext, jaxbElement);
	}
	
    /**
     * Returns true for the string values "1" and "true", ignoring upper/lower
     * case and whitespace, false otherwise.
     */
	public static boolean stringToBoolean(String booleanAsString) {
        if (booleanAsString == null)
            return false;
        booleanAsString = booleanAsString.trim().toLowerCase();
        return (booleanAsString.equals("1") || booleanAsString.equals("true"));
    }
	
	/**
	 * Returns <code>true</code> if the specified {@link Class} does have an 
	 * {@link XmlRootElement} annotation.
	 * 
	 * @param   _class
	 * @return  <code>true</code> if the specified {@link Class} does have an 
	 *          {@link XmlRootElement} annotation.
	 */
	public static boolean isComplexType(Class<?> _class) {
		return _class.getAnnotation(XmlRootElement.class) != null;
	}
}

/**
 * This class is used by the method {@link XMLUtil#createXmlSerializable(QName, Object)}
 * to create an instance of the interface {@link XmlSerializable} which uses a {@link Marshaller}
 * to serialize the wrapped {@link JAXBElement}.
 * 
 * @author Hendrik Helwich
 *
 */
class JAXBSerializable implements XmlSerializable {

	private JAXBElement jaxbElement;
	private BindContext bindContext;
	
	/**
	 * The specified {@link JAXBElement} will be marshalled with a {@link Marshaller}
	 * if the method {@link #writeXmlRepresentation(XMLStreamWriter)} is called.
	 * 
	 * @param  jaxbElement
	 */
	public JAXBSerializable(BindContext bindContext, JAXBElement jaxbElement) {
		this.bindContext = bindContext;
		this.jaxbElement = jaxbElement;
	}
	 
	public void writeXmlRepresentation(XMLStreamWriter writer) throws XMLStreamException {
		writer = PrefixXMLStreamWriter.enablePrefixDefaulting(writer);
		try {
			Marshaller marshaller = bindContext.createMarshaller();
			marshaller.marshal(jaxbElement, writer);
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}
}