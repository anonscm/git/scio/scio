/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.bind;


/**
 * This class can be used to hold instances of the class {@link BindContext}.
 * This can be used to create a {@link BindContext} instance at initialization time
 * and register it to this class. 
 * 
 * @author Hendrik Helwich, tarent GmbH
 */
public class BindContextHolder {
	
	private BindContextHolder() {}

	private static BindContext defaultContext;

	public static BindContext getDefaultContext() {
		if (defaultContext == null)
			throw new NullPointerException("the default context has not been set");
		return defaultContext;
	}

	public static void setDefaultContext(BindContext defaultContext) {
		BindContextHolder.defaultContext = defaultContext;
	}
}
