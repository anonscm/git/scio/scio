/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.bind;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.namespace.QName;

import org.evolvis.scio.util.XMLUtil;

public class BindContext {

	
	protected static String DEFAULT_NAME = "##default";
	private String packageSearchPath;
	
	Map<QName, MarshallingInfo> infoByType = new HashMap<QName, MarshallingInfo>();	
	Map<String, MarshallingInfo> infoByClassName = new HashMap<String, MarshallingInfo>();
	
	protected BindContext(String packageSearchPath) {	
		this.packageSearchPath = packageSearchPath;
	}
	
	public static BindContext newInstance(String packageSearchPath) throws JAXBException {
		BindContext cntx = new BindContext(packageSearchPath);
		String[] packages = packageSearchPath.split("\\:");
		// traverse in reverse order for overrides
		for (int i = packages.length-1; i>=0; i--) {
			String indexFile = "/"+packages[i].replace(".", "/") +"/jaxb.index";
			InputStream is = BindContext.class.getResourceAsStream(indexFile);
			if (is != null) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				String line;
				String className = null;
				try {
					while (null != (line = reader.readLine())) {
						line = line.trim();
						if (line.startsWith("#") || "".equals(line))
							continue;
						className = packages[i]+"."+line;
						Class clazz = Class.forName(className);
						cntx.addMarshallingInfo(cntx.resolveInfo(clazz));
					}
				} catch (ClassNotFoundException e) {
					throw new JAXBException("class referenced in jaxb.index, but not found: '"+className+"'", e);
				} catch (IOException e) {
					throw new JAXBException("io error while resolving classes", e);
				}
			} else {
				//TODO: implement Object Factory here
				throw new JAXBException("index file jaxb.index not found in package "+packages[i]);
			}
		}
		return cntx;
	}
	
	public static  BindContext newInstance(Class...classes) throws JAXBException {
		BindContext cntx = new BindContext(null);
		for (Class clazz : classes) {
			cntx.addMarshallingInfo(cntx.resolveInfo(clazz));			
		}
		return cntx;
	}
	
	public Marshaller createMarshaller() throws JAXBException {
		return new Marshaller(this);
	}
	
	public Unmarshaller createUnarshaller() throws JAXBException {
		return new Unmarshaller(this);
	}
	
	public MarshallingInfo getInfo(QName type) {
		return infoByType.get(type);
	}

	public MarshallingInfo getInfo(Class javaClass) {
		if (javaClass == null)
			throw new IllegalArgumentException("supplied java class has to be not null");
		return infoByClassName.get(javaClass.getName());
	}

	public void addMarshallingInfo(MarshallingInfo info) {
		assert(info.targetType != null);
		infoByType.put(info.targetType, info);
		infoByClassName.put(info.targetClass.getName(), info);
	}
	
	// TODO: handle inheritance
	@SuppressWarnings("unchecked") // remove warnings: getAnnotation
	private MarshallingInfo resolveInfo(Class javaClass) {
		MarshallingInfo info = new MarshallingInfo();
		info.targetClass = javaClass;
		
		// get the type
		XmlType typeAno = (XmlType)javaClass.getAnnotation(XmlType.class);
		if (null != typeAno) {			
			info.targetType = new QName(typeAno.namespace(), typeAno.name());
		} else {
			if (javaClass.isArray()) { // Array type
				Class elementType = javaClass.getComponentType();
				if (byte.class.equals(elementType)) { // byte array type
					String ns = "";
					info.targetType = new QName(ns, lcFirst(javaClass.getSimpleName()));
				} else { // not byte array type
					String ns = "";
					info.targetType = new QName(ns, lcFirst(javaClass.getSimpleName()));
				}
			} else { // not array type
				Package pckg = javaClass.getPackage();
				XmlSchema schemaAno = pckg == null ? null : pckg.getAnnotation(XmlSchema.class);
				String ns = (null != schemaAno) ? schemaAno.namespace() : javaClass.getPackage().getName();
				info.targetType = new QName(ns, lcFirst(javaClass.getSimpleName())); 
			}
		}
		
		XmlRootElement rootAno = (XmlRootElement) javaClass.getAnnotation(XmlRootElement.class);
		if (null != rootAno) {
			info.isXMLRootType = true;
			if (!isDefault(rootAno.name()))
				info.name = rootAno.name();
			if (!isDefault(rootAno.namespace()))
				info.namespace = rootAno.namespace();
			
			// if @XmlRootElement is present with name information, but
			// @XmlType was not, we will provide it as type
			if (typeAno == null && info.name != null && info.namespace != null)
				info.targetType = new QName(info.namespace, info.name);			
		}
		if (info.name == null)
			info.name = info.targetType.getLocalPart();
		if (info.namespace == null)
			info.namespace = info.targetType.getNamespaceURI();
		
		for (Method m : javaClass.getMethods()) {
			String methodName = m.getName();
			Class[] args = m.getParameterTypes();
			if ((args.length == 0 && (methodName.startsWith("get")  || methodName.startsWith("is")) && !methodName.equals("getClass"))
 				|| args.length == 1 && methodName.startsWith("set")) {
				
				String propertyName = methodName.startsWith("is") ? lcFirst(methodName.substring(2)) : lcFirst(methodName.substring(3));
				MarshallingElement element = info.getPropertyByBeanName(propertyName);
				if (element == null) {
					element = new MarshallingElement();
					element.beanPropertyName = propertyName;
					element.name = propertyName;
					info.addProperty(element);
				}
				if (methodName.startsWith("set")) {
					element.setMethod = m;
					element.javaPropertyType = args[0];
				}
				else {
					element.getMethod = m;
					element.javaPropertyType = m.getReturnType();
				}
				element.isPrimitiveType = isPrimitiveJavaType(element.javaPropertyType) || String.class.equals(element.javaPropertyType); 
				if (element.primitiveType == null)
					element.primitiveType = getPrimitiveTypeFor(element.javaPropertyType);
				
				XmlElement elementAno = m.getAnnotation(XmlElement.class);
				if (elementAno != null) { 
					if (! "\u0000".equals(elementAno.defaultValue()))
						element.defaultValue = elementAno.defaultValue();
					element.nillable = elementAno.nillable();
					element.required = elementAno.required();
					if (!isDefault(elementAno.namespace()))
						element.namespaceName = elementAno.namespace(); 
					if (!isDefault(elementAno.name()))
						element.name = elementAno.name();
				}
				
				XmlAttribute attributeAno = m.getAnnotation(XmlAttribute.class);
				if (attributeAno != null) { 
					element.encodeAsAttribute = true;
					element.required = attributeAno.required();
					if (!isDefault(attributeAno.namespace()))
						element.namespaceName = attributeAno.namespace(); 
					if (!isDefault(attributeAno.name()))
						element.name = attributeAno.name();
				}
				
				// it is simple content, if it is primitive type,
				// or if it has an XmlValue annotation,
				// or if it is an XmlSchemaType
				element.simpleContent = element.simpleContent || element.isPrimitiveType;
				if (null != m.getAnnotation(XmlValue.class))
					element.simpleContent = true;

				XmlSchemaType schemaTypeAno = m.getAnnotation(XmlSchemaType.class);				
				if (schemaTypeAno != null) {
					element.simpleContent = true;
					element.primitiveType = new QName(schemaTypeAno.namespace(), schemaTypeAno.name());
				}
				
				if (element.namespaceName == null)
					element.namespaceName = info.namespace;
			}
		}
		
		// remove all fields, if they only have do not have a get Method (only a set Method),
		// because they are no valid java beand fields.
		for (Iterator<MarshallingElement> iter = info.getAllProperties(); iter.hasNext();) {
			if (iter.next().getMethod == null)
				iter.remove();
		}

		return info;
	}
	
	protected static boolean isPrimitiveJavaType(Class javaPropertyType) {
		if (javaPropertyType.isPrimitive())
			return true;
		
		if (String.class.equals(javaPropertyType))
			return true;
		if (Integer.class.equals(javaPropertyType))
			return true;
		if (Boolean.class.equals(javaPropertyType))
			return true;
		if (Float.class.equals(javaPropertyType))
			return true;
		if (Double.class.equals(javaPropertyType))
			return true;
		if (Character.class.equals(javaPropertyType))
			return true;
		if (Byte.class.equals(javaPropertyType))
			return true;
		if (Short.class.equals(javaPropertyType))
			return true;
		if (Date.class.equals(javaPropertyType))
			return true; 
		if(Long.class.equals(javaPropertyType))
			return true;
		return false;
	}

	private boolean isDefault(String name) {
		return null == name || DEFAULT_NAME.equals(name);
	}
	
	protected static QName getPrimitiveTypeFor(Class targetJavaClass) {
		if (String.class.equals(targetJavaClass))
			return XMLUtil.XSD_STRING;
		if (Integer.class.equals(targetJavaClass))
			return XMLUtil.XSD_INTEGER;
		if (Boolean.class.equals(targetJavaClass))
			return XMLUtil.XSD_BOOLEAN;
		if (Float.class.equals(targetJavaClass))
			return XMLUtil.XSD_FLOAT;
		if (Date.class.equals(targetJavaClass) || Calendar.class.equals(targetJavaClass))
			return XMLUtil.XSD_DATE;
		return null;
	}

	protected static String lcFirst(String name) {
		if (name.length() == 0)
			return name;
		return name.substring(0, 1).toLowerCase() + (name.length() > 1 ? name.substring(1) : "");
	}

	public String getPackageSearchPath() {
		return packageSearchPath;
	}	
}
