/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.bind;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.scio.converter.DateConverter;
import org.evolvis.scio.util.PrefixXMLStreamWriter;
import org.evolvis.scio.util.XMLUtil;
import org.evolvis.scio.xmpp.utils.Base64;

/**
 * Simple straight forward marshaller, 
 * which should implement a lightweight subset of JAXB.  
 * 
 * @author Sebastian Mancke, tarent GmbH
 */
public class Marshaller {

	private BindContext context;
	int namespaceCounter = 1;
	private static Object[] EMPTY_OBJECT_ARRAY = new Object[]{};

	public Marshaller(BindContext context) {
		this.context = context;
	}
	
	public void marshal(Object jaxbElement, File output) throws JAXBException {
		try {
			marshal(jaxbElement, new FileOutputStream(output));
		} catch (IOException e) {
			throw new JAXBException("error while writing to file", e);
		}
	}

	public void marshal(Object jaxbElement, OutputStream outputStream) throws JAXBException {
		try {
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(outputStream);
			writer.writeStartDocument();
			marshal(jaxbElement, writer);
			writer.close();						
		} catch (JAXBException e) {
			throw e;
		} catch (Exception e) {		
			throw new JAXBException("error while doing xml output", e);
		}
	}

	//TODO: Extend for all primitve types
	public void marshal(JAXBElement jaxbElement, XMLStreamWriter writer) throws JAXBException {
		if (jaxbElement == null)
			throw new IllegalArgumentException("jaxbElement must not be null");
		QName qname = jaxbElement.getName();
		Class _class = jaxbElement.getDeclaredType();
		Object value = jaxbElement.getValue();
		try {
			if (jaxbElement.isNil()) {
				String xsiPrefix =  ensureNamespace(XMLUtil.XSI_TYPE.getNamespaceURI(), writer, "xsi");
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeAttribute(xsiPrefix, XMLUtil.XSI_TYPE.getNamespaceURI(), "nil", "true");
				writer.writeEndElement();
			} else if (Boolean.class.equals(_class)) {
				Boolean b = (Boolean) value;
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(b.toString());
				writer.writeEndElement();
			} else if (Byte.class.equals(_class)) {
				Byte b = (Byte) value;
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(b.toString());
				writer.writeEndElement();
			} else if (Integer.class.equals(_class)) {
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(value.toString());
				writer.writeEndElement();
			} else if (Double.class.equals(_class)) {
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(value.toString());
				writer.writeEndElement();
			} else if (Short.class.equals(_class)) {
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(value.toString());
				writer.writeEndElement();
			} else if (Long.class.equals(_class)) {
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(value.toString());
				writer.writeEndElement();
			} else if (Float.class.equals(_class)) {
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(value.toString());
				writer.writeEndElement();
			} else if (String.class.equals(_class)) {
				String s = (String) value;
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(s);
				writer.writeEndElement();
			} else if (byte[].class.equals(_class)) {
				byte[] b = (byte[]) value;
				String base64 = Base64.encode(b); 
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(base64);
				writer.writeEndElement();
			} else if (Date.class.equals(_class)) {
				writer.writeStartElement(qname.getNamespaceURI(), qname.getLocalPart());
				writer.writeCharacters(convertToString(value));
				writer.writeEndElement();
			} else {
				marshal(jaxbElement.getValue(), writer);
			}
		} catch (XMLStreamException e) {
			throw new JAXBException("error while doing xml output", e);
		}
	}

	public void marshal(Object jaxbElement, XMLStreamWriter writer) throws JAXBException {
		if (jaxbElement == null)
			throw new IllegalArgumentException("jaxbElement must not be null");
		MarshallingInfo info = context.getInfo(jaxbElement.getClass());
		if (info == null)
			throw new JAXBException("no information to marshal '"+jaxbElement.getClass());
		if (! info.isXMLRootType)
			throw new JAXBException("marshal element must be an XMLRootType: '"+jaxbElement.getClass());
		try {
			if (PrefixXMLStreamWriter.isPrefixDefaulting(writer))
				writer.writeStartElement(info.namespace, info.name);
			else
				writeNamespacedStartElement(writer, info.namespace, info.name);
			marshalChildElement(jaxbElement, info, writer);
			endChild(writer);
		} catch (XMLStreamException e) {
			throw new JAXBException("error while doing xml output", e);
		}
	}
	
	/**
	 * Marshals a child element to the writer.
	 * This method assumes, that the start is already
	 * written and the end tag will be written by the caller.
	 * @param jaxbElement
	 * @param writer
	 * @throws JAXBException
	 */
	protected void marshalChildElement(Object jaxbElement, MarshallingInfo info, XMLStreamWriter writer) throws JAXBException {
		// writing type

		try {
			String typePrefix = ensureNamespace(info.targetType.getNamespaceURI(), writer, null);
			String xsiPrefix =  ensureNamespace(XMLUtil.XSI_TYPE.getNamespaceURI(), writer, "xsi");
			writer.writeAttribute(xsiPrefix, XMLUtil.XSI_TYPE.getNamespaceURI(), XMLUtil.XSI_TYPE.getLocalPart(), typePrefix+":"+info.targetType.getLocalPart());
		
			// output all attributes 
			for (Iterator<MarshallingElement> iter = info.getAttributeProperties(); iter.hasNext();) {
				MarshallingElement element = iter.next();
				String value = convertToString(getRawValue(element, jaxbElement));
				if (value != null) {
					if (element.namespaceName != null && !"".equals(element.namespaceName) && ! info.namespace.equals(element.namespaceName)) {
						String attrPrefix = ensureNamespace(element.namespaceName, writer, null);
						writer.writeAttribute(attrPrefix, element.namespaceName, element.name, value);
					} else {
						writer.writeAttribute(element.name, value);					
					}
				} else {
					// null values in attributes will be skipped by jaxb
				}
			}

			// output all elements
			for (Iterator<MarshallingElement> iter = info.getElementProperties(); iter.hasNext();) {
				MarshallingElement element = iter.next();

				Object rawValue = getRawValue(element, jaxbElement);
				if (rawValue == null) {
					if (element.nillable)
						writeNullElement(writer, info, element);
					// else skip
				} else {
					if (element.simpleContent) {
						startChild(writer, info, element);
						writer.writeCharacters(convertToString(rawValue));
						endChild(writer);
					} else {
						if (element.isListType()) {
							for (Object item: (List)rawValue) {
								if (item == null) {
									writeNullElement(writer, info, element);
								} else {
									startChild(writer, info, element);
									marshalChildElement(item, childInfo(item.getClass()), writer);
									endChild(writer);
								}
							}
						} else {
							startChild(writer, info, element);
							marshalChildElement(rawValue, childInfo(element.javaPropertyType), writer);
							endChild(writer);
						}
					}
				}
			}
		} catch (XMLStreamException e) {
			throw new JAXBException("error while doing xml output", e);
		}
	}

	private void writeNullElement(XMLStreamWriter writer, MarshallingInfo info, MarshallingElement element) throws XMLStreamException {
		startChild(writer, info, element);
		String xsiPrefix =  ensureNamespace(XMLUtil.XSI_TYPE.getNamespaceURI(), writer, "xsi");
		writer.writeAttribute(xsiPrefix, XMLUtil.XSI_TYPE.getNamespaceURI(), "nil", "true");
		endChild(writer);		
	}

	/**
	 * @param writer
	 * @throws XMLStreamException
	 */
	private void endChild(XMLStreamWriter writer) throws XMLStreamException {
		writer.writeEndElement();
	}

	/**
	 * @param info
	 * @param writer
	 * @param element
	 * @throws XMLStreamException
	 */
	private void startChild(XMLStreamWriter writer, MarshallingInfo info, MarshallingElement element) throws XMLStreamException {
		if (element.namespaceName == null || element.namespaceName.equals(info.namespace))
			writer.writeStartElement(element.name);
		else {
			writeNamespacedStartElement(writer, element.namespaceName, element.name);
		}
	}

	private MarshallingInfo childInfo(Class clazz) throws JAXBException {
		MarshallingInfo childInfo = context.getInfo(clazz);
		if (childInfo == null)
			throw new JAXBException("no information to marshal '"+clazz);
		return childInfo;
	}

	private Object getRawValue(MarshallingElement element, Object jaxbElement) throws JAXBException {
		try {
			return element.getMethod.invoke(jaxbElement, EMPTY_OBJECT_ARRAY);
		} catch (Exception e) {
			throw new JAXBException("error while accessing attribute '"+element.beanPropertyName+"' of class '"+element.javaPropertyType+"'");
		}		
	}

/*	private String getSimpleValue(MarshallingElement element, Object jaxbElement) throws JAXBException {		
		Object rawValue;
		try {
			rawValue = element.getMethod.invoke(jaxbElement, EMPTY_OBJECT_ARRAY);
			return convertToString(rawValue);
		} catch (Exception e) {
			throw new JAXBException("error while accessing attribute '"+element.beanPropertyName+"' of class '"+jaxbElement.getClass()+"'");
		}		
	}
*/
	private String convertToString(Object rawValue) throws JAXBException {
		if (rawValue == null)
			return null;
		if (rawValue instanceof String)
			return (String) rawValue;
		if (rawValue instanceof Date)
			return DateConverter.dateToString((Date) rawValue, DateConverter.DATE_TIME);
		if (rawValue instanceof List)
			return collase((List)rawValue);
		return rawValue.toString();
	}

	private String collase(List list) throws JAXBException {
		StringBuffer sb = new StringBuffer();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			sb.append(convertToString(iter.next()));
			if (iter.hasNext())
				sb.append(" ");
		}
		return sb.toString();
	}

	/**
	 * @param info
	 * @param writer
	 * @return
	 * @throws XMLStreamException
	 */
	private String ensureNamespace(String ns, XMLStreamWriter writer, String defaultPrefix)
			throws XMLStreamException {
		String typePrefix = writer.getPrefix(ns);
		if (typePrefix == null) {
			typePrefix = getNextNSPrefix(writer, defaultPrefix);
			writer.setPrefix(typePrefix, ns);
			writer.writeNamespace(typePrefix, ns);
		}
		return typePrefix;
	}
	
	private void writeNamespacedStartElement(XMLStreamWriter writer, String ns, String name) throws XMLStreamException {
		String prefix = writer.getPrefix(ns);
		if (prefix == null) {
			prefix = getNextNSPrefix(writer, null);
			writer.setPrefix(prefix, ns);
			writer.writeStartElement(prefix, name, ns);
			writer.writeNamespace(prefix, ns);
		}		
	}

	private String getNextNSPrefix(XMLStreamWriter writer, String defaultPrefix) {
		String prefixCanditate = defaultPrefix != null ? defaultPrefix : "ns"+(namespaceCounter++);
		while (null != writer.getNamespaceContext().getNamespaceURI(prefixCanditate)) {
			prefixCanditate = "ns"+(namespaceCounter++);			
		}
		return prefixCanditate;
	}
}
