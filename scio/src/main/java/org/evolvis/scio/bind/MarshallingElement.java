/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.bind;

import java.lang.reflect.Method;
import java.util.List;

import javax.xml.namespace.QName;

/**
 * Description struct for the marshalling information of one property;
 *  
 * @author Sebastian Mancke, tarent GmbH
 */
public class MarshallingElement {
	
	/**
	 * Name of the java property.
	 */
	public String beanPropertyName;
	
	/**
	 * Name of the tag or attribute in the xml
	 */
	public String name;	
	
	/**
	 * Namespace name of the tag or attribute in the xml
	 */
	public String namespaceName;

	/**
	 * The corresponding java set method
	 */
	public Method setMethod;
	
	/**
	 * The corresponding java getMethod
	 */
	public Method getMethod;	
	
	/**
	 * The XML Schema type, if this is a primitive type
	 */
	public QName primitiveType;
	
	/**
	 * Flag, whether this is a primitive java type  
	 */
	public boolean isPrimitiveType = false;
	
	/**
	 * Indicates, if the contents of the element is
	 * represented in a single string.
	 */
	public boolean simpleContent = false;
	
	/**
	 * Flag, whether this should be encoded as attribute or as element
	 */
	public boolean encodeAsAttribute = false;
	
	/**
	 * Flag, whether this element may have an xsi:nil value.
	 * If nillable = false, or it is an attribute, the element will 
	 * not be included in the xml, if it is null.  
	 */
	public boolean nillable = false;
	
	/**
	 * Flag to indicate, whether an element has to be present
	 */
	public boolean required = false;
	
	/**
	 * The java type of the element
	 */
	public Class javaPropertyType;
	
	/**
	 * A default value, which should be applied, 
	 * if the element is not present in xml. 
	 */
	public String defaultValue = null;
	
	public String toString() {
		return "MarshallingElement("
		+ ", name="+name
		+ ", namespaceName="+namespaceName
		+ ", primitiveType="+primitiveType
		+ ", isPrimitiveType="+isPrimitiveType
		+ ", encodeAsAttribute="+encodeAsAttribute
		+ ", nillable="+nillable
		+ ", required="+required
		+ ", simpleContent="+simpleContent
		+ ", targetJavaClass="+javaPropertyType
		+ ", defaultValue="+defaultValue
		+ ")";
	}

	public boolean isListType() {
		return List.class.isAssignableFrom(javaPropertyType);
	}
}
