/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.bind;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.evolvis.scio.converter.DateConverter;
import org.evolvis.scio.util.XMLUtil;

/**
 * Simple straight forward marshaller, 
 * which should implement a lightweight subset of JAXB.  
 * 
 * @author Sebastian Mancke, tarent GmbH
 */
public class Unmarshaller {
	
	protected static Class[] EMPTY_CLASS_PARAMETER = new Class[]{};
	protected static Object[] EMPTY_OBJECT_PARAMETER = new Object[]{};
	
	private BindContext context;

	public Unmarshaller(BindContext context) {
		this.context = context;
	}

	public Object unmarshal(File file) throws JAXBException {		
		XMLInputFactory factory = XMLInputFactory.newInstance();
		try {
			XMLStreamReader parser = factory.createXMLStreamReader(new FileInputStream(file));
			return unmarshal(parser);
		} catch (Exception e) {
			throw new JAXBException("error while reading from file "+file.getAbsolutePath(), e);
		}	
	}
	
	public Object unmarshal(XMLStreamReader reader) throws JAXBException {		
		try {
			if (!reader.isStartElement())
				reader.nextTag();

			//System.out.println("in unmarshal: "+reader.getName());
			if (XMLUtil.isNullElement(reader)) {
				XMLUtil.skipSubtree(reader);
				return null;
			}

			// create actual object			
			QName type = XMLUtil.getElementType(reader);
			MarshallingInfo info = context.getInfo(type);
			if (info == null)
				throw new JAXBException("no jaxb configuration found for xml type "+ type);
			return unmarshal(reader, info);
		} catch (XMLStreamException e) {
			throw new JAXBException("xml error on unmarshaling", e);
		}
	}

	public Object unmarshal(XMLStreamReader reader, MarshallingInfo info) throws XMLStreamException , JAXBException {				
		Object current = getInstance(info); 
		
		int attributeCount = reader.getAttributeCount();
		for (int i=0; i<attributeCount; i++) {
			QName attributeName = reader.getAttributeName(i);				
			MarshallingElement element = info.getAttributePropertyByXMLName(attributeName);
			if (element != null) {
				setPrimitiveValue(current, element, reader.getAttributeValue(i));
			}// else
			 //	System.out.println("ignoring unknown attribute: "+attributeName);				
		}
			
		Map<MarshallingElement,List<Object>> listProperties = null;
		int event;
		while (true) {
			event = reader.next();
			if (event == XMLStreamConstants.START_ELEMENT) {
				MarshallingElement element = info.getElementPropertyByXMLName(reader.getLocalName());										
				if (element != null) {
					try {
						if (element.isPrimitiveType) {
							if (XMLUtil.isNullElement(reader))
								set(current, element, null);
							else
								setPrimitiveValue(current, element, reader.getElementText());
						} else {
							// TODO: Check if this handling is proper: e.g. primitives in a list
							Object child = null;
							if (element.simpleContent) {
								//TODO: support for schema type conversion
								child = reader.getElementText();
							} else {
								MarshallingInfo childInfo = context.getInfo(element.javaPropertyType);						
								child = childInfo != null ? unmarshal(reader, childInfo) : unmarshal(reader);
							}
							if (element.isListType()) {
								if (listProperties == null)
									listProperties = new HashMap<MarshallingElement, List<Object>>(4);	
								List<Object> propertyList = listProperties.get(element);
								if (propertyList == null) {
									// TODO: support for generics
									propertyList = new ArrayList<Object>();
									listProperties.put(element, propertyList);
								}
								propertyList.add(child);
							} else {
								set(current, element, child);
							}					
						}
					} catch (Exception e){
						throw new JAXBException("Error while unmarshalling property "+element.beanPropertyName+" of "+info.targetClass, e);						
					}
				} else {
					System.out.println("ignoring unknown element: "+reader.getName());
					XMLUtil.skipSubtree(reader);
				}
			}
			else if (event == XMLStreamConstants.END_ELEMENT) {
				break;							
			}
		}
		if (listProperties != null) {
			for (Map.Entry<MarshallingElement, List<Object>> entry : listProperties.entrySet()) {
				set(current, entry.getKey(), entry.getValue());
			}
		}
		return current;
	}

	
	
	private void setPrimitiveValue(Object current, MarshallingElement element, String value) throws JAXBException {
		if (element.setMethod == null)
			System.out.println("ignoring value because no setter (read only): "+element.beanPropertyName);
		Class targetType = element.setMethod.getParameterTypes()[0];
		set(current, element, convert(value, targetType));
	}

	/**
	 * @param current
	 * @param element
	 * @param targetValue
	 * @throws JAXBException
	 */
	private void set(Object current, MarshallingElement element, Object targetValue) throws JAXBException {
		try {
			element.setMethod.invoke(current, targetValue);
		} catch (Exception e) {
			throw new JAXBException("error whild setting property '"+element.name+"'", e);
		}
	}
	
	protected Object convert(String simpleValue, Class toType) throws JAXBException {
		if (simpleValue == null) {
			if (toType.isPrimitive())
				throw new JAXBException("can not assign null value to primitive java type "+toType);
			return null;
		}
		if (String.class.equals(toType)) {
			return simpleValue;
		} 
		else if (Integer.class.equals(toType)) {
			return Integer.parseInt(simpleValue);
		} else if (Float.class.equals(toType)) {
			return Float.parseFloat(simpleValue);
		} else if (Long.class.equals(toType)) {
			return Long.parseLong(simpleValue);
		} else if (Double.class.equals(toType)) {
			return Double.parseDouble(simpleValue);
		} else if (Boolean.class.equals(toType)) {
			return new Boolean(simpleValue.toLowerCase().equals("true") || simpleValue.equals(1));
		} else if(Date.class.equals(toType)) {
			return DateConverter.stringToDate(simpleValue, DateConverter.DATE_TIME);
		}
		// TODO: Convert time, base64
		throw new JAXBException("conversion not possible for simple value to type: "+ toType);
	}
	
	protected boolean isPrimitiveType(Class toType) {
		if (String.class.equals(toType)) {
			return true;
		} 
		else if (Integer.class.equals(toType)) {
			return true;
		} else if (Float.class.equals(toType)) {
			return true;
		} else if (Long.class.equals(toType)) {
			return true;
		} else if (Double.class.equals(toType)) {
			return true;
		} else if (Boolean.class.equals(toType)) {
			return true;
		} else if(Date.class.equals(toType)) {
			return true;
		}
		// TODO: Convert time, base64
		return false;
	}
	
	private Object getInstance(MarshallingInfo info) throws JAXBException{
		try {
			return info.targetClass.getConstructor(EMPTY_CLASS_PARAMETER).newInstance(EMPTY_OBJECT_PARAMETER);
		} catch (NoSuchMethodException e) {
			throw new JAXBException("no void cotructor in class "+ info.targetClass);			
		} catch (Exception e) {
			throw new JAXBException("error while creating instance of "+ info.targetClass, e);
		}
	}

	/**
	 * Not sure, if this mehtod handles primitive types correct!
	 */
	public JAXBElement unmarshal(XMLStreamReader reader, Class declaredType) throws JAXBException {	 
		Object returnObject = null;
		try {
			 MarshallingInfo info = context.getInfo(declaredType);
			 if (info != null) {
					returnObject = unmarshal(reader, info);				
					return new JAXBElement(info.targetType, declaredType, returnObject);
			 } else {
				 
				 if (! isPrimitiveType(declaredType))
					 throw new JAXBException("no jaxb mapping for declaredType: "+ declaredType);
				 			
				 returnObject = convert(reader.getElementText(), declaredType);
				 JAXBElement returnElement = new JAXBElement(reader.getName(), declaredType, returnObject);
				 XMLUtil.skipSubtree(reader);				 
				 return returnElement;
			}
		} catch (XMLStreamException e) {
			throw new JAXBException("error on parsing", e);
		}
	}
}

