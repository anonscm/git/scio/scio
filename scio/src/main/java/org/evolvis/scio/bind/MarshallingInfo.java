/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio.bind;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

/**
 * MarshallingInfo is a description of a mapping from
 * java type to xml type. It Consists of some global properties, 
 * as well as a set of attribute descriptions.
 * 
 * <p>Normally this class should not be used directly. The normal way of 
 * creating a configuration is by using the {@link BindContext}. 
 * But in the case, the underlaying system lacks  in annotation support,
 * or a customized configuration should be created, there are some helper methods
 * to create a configuration.</p> 
 *   
 * @author Sebastian Mancke, tarent GmbH
 */
public class MarshallingInfo {

	public static int TYPE_ATTRIBUTES = 1;
	public static int TYPE_ELEMENTS = 2;

	public Class targetClass;
	public QName targetType;
	public String name;
	public String namespace;
	public boolean isXMLRootType = false;
	protected List<MarshallingElement> properties = new ArrayList<MarshallingElement>();
	
	public MarshallingInfo() {
		
	}

	/**
	 * Contruct a new marshalling desciption.
	 * The elementName will be set to the target type.
	 * 
	 * @param targetType The xml schema type
	 * @param targetClass the corresponding java class
	 * @param isXMLRootType flag, whether this may be a to level type
	 */
	public MarshallingInfo(QName targetType, Class targetClass, boolean isXMLRootType) {
		this.isXMLRootType = isXMLRootType;
		this.targetClass = targetClass;
		this.targetType = targetType;
		this.name = targetType.getLocalPart();
		this.namespace = targetType.getNamespaceURI();		
	}
	
	/**
	 * Contruct a new marshalling desciption.
	 * 
	 * @param elementName The name, the element should have, if it is used standalone
	 * @param targetType The xml schema type
	 * @param targetClass the corresponding java class
	 * @param isXMLRootType flag, whether this may be a to level type
	 */
	public MarshallingInfo(QName elementName, QName targetType, Class targetClass, boolean isXMLRootType) {
		this.isXMLRootType = isXMLRootType;
		this.targetClass = targetClass;
		this.targetType = targetType;
		this.name = elementName.getLocalPart();
		this.namespace = elementName.getNamespaceURI();		
	}
	
	/**
	 * Adds one {@link MarshallingElement} to the list of properties
	 * @param property
	 */
	public void addProperty(MarshallingElement property) {
		properties.add(property);
	}
	
	/**
	 * Convinience Method for adding a {@link MarshallingElement} to the list of elements.
	 * This methods tries to find the right defaults based on reflection, without 
	 * using the annotations. 	
	 * 
	 * <p>Use {@see #addProperty(MarshallingElement)} to get full control over the serialisation</p> 
	 * @throws JAXBException 
	 */
	public void addElement(String name, Class javaTargetType) throws JAXBException {
		addElement(name, javaTargetType, false);
	}

	/**
	 * Method for adding a {@link MarshallingElement} to the list of elements.
	 * This methods tries to find the right defaults based on reflection, without 
	 * using the annotations.
	 * 
	 * <p>Does not work for boolean accessors e.g. isXXX().</p>
	 * <p>Use {@see #addProperty(MarshallingElement)} to get full control over the serialisation</p> 
	 * @throws JAXBException 
	 */
	public void addElement(String name, Class javaPropertyType, boolean nillable) throws JAXBException {
		MarshallingElement element = new MarshallingElement();
		element.beanPropertyName = name;
		element.name = name;
		element.encodeAsAttribute = false;
		element.nillable = nillable;
		element.isPrimitiveType = BindContext.isPrimitiveJavaType(javaPropertyType);
		if (element.isPrimitiveType) {
			element.primitiveType = BindContext.getPrimitiveTypeFor(javaPropertyType);
			element.simpleContent = true;
		}
		try {
			element.getMethod = targetClass.getMethod("get"+ucFirst(name), new Class[0]);
			element.setMethod = targetClass.getMethod("set"+ucFirst(name), new Class[]{element.getMethod.getReturnType()});
		} catch (Exception e) {
			throw new JAXBException("error while method lookup in binding configuration", e);
		}
		addProperty(element);
	}
	
	/**
	 * Method for adding a {@link MarshallingElement} to the list of attributes.
	 * This methods tries to find the right defaults based on reflection, without 
	 * using the annotations.
	 * 
	 * <p>Does not work for boolean accessors e.g. isXXX().</p>
	 * <p>Use {@see #addProperty(MarshallingElement)} to get full control over the serialisation</p> 
	 * @throws JAXBException 
	 */
	public void addAttribute(String name, Class javaPropertyType) throws JAXBException {
		MarshallingElement attribute = new MarshallingElement();
		attribute.beanPropertyName = name;
		attribute.encodeAsAttribute = true;
		attribute.nillable = false;
		attribute.simpleContent = true;
		attribute.isPrimitiveType = BindContext.isPrimitiveJavaType(javaPropertyType);
		if (attribute.isPrimitiveType) {
			attribute.primitiveType = BindContext.getPrimitiveTypeFor(javaPropertyType);
		}
		try {
			attribute.getMethod = targetClass.getMethod("get"+ucFirst(name), new Class[0]);
			attribute.setMethod = targetClass.getMethod("set"+ucFirst(name), new Class[]{attribute.getMethod.getReturnType()});
		} catch (Exception e) {
			throw new JAXBException("error while method lookup in binding configuration", e);
		}
		addProperty(attribute);
	}

	public MarshallingElement getPropertyByBeanName(String name) {
		for (MarshallingElement element : properties) {
			if (name.equals(element.beanPropertyName))
				return element;
		}
		return null;
	}
	
	public MarshallingElement getPropertyByXMLName(String name) {
		for (MarshallingElement element : properties) {
			if (name.equals(element.name))
				return element;
		}
		return null;
	}

	public MarshallingElement getAttributePropertyByXMLName(QName name) {
		for (Iterator<MarshallingElement> iter = getAttributeProperties(); iter.hasNext();) {
			MarshallingElement element = iter.next();
			if (name.getLocalPart().equals(element.name) && (name.getNamespaceURI() == null || "".equals(name.getNamespaceURI()) || name.getNamespaceURI().equals(element.namespaceName)) )
				return element;
		}
		return null;
	}

	public MarshallingElement getElementPropertyByXMLName(QName name) {
		for (Iterator<MarshallingElement> iter = getElementProperties(); iter.hasNext();) {
			MarshallingElement element = iter.next();
			if (name.getLocalPart().equals(element.name) && (name.getNamespaceURI() == null || "".equals(name.getNamespaceURI()) || name.getNamespaceURI().equals(element.namespaceName)) )
				return element;
		}
		return null;
	}

	public MarshallingElement getElementPropertyByXMLName(String localName) {
		for (Iterator<MarshallingElement> iter = getElementProperties(); iter.hasNext();) {
			MarshallingElement element = iter.next();
			if (localName.equals(element.name))
				return element;
		}
		return null;
	}

	public MarshallingElement getPropertyByXMLName(QName name) {
		for (MarshallingElement element : properties) {
			if (name.getLocalPart().equals(element.name) && (name.getNamespaceURI() == null || "".equals(name.getNamespaceURI()) || name.getNamespaceURI().equals(element.namespaceName)) )
				return element;
		}
		return null;
	}
		
	public Iterator<MarshallingElement> getAllProperties() {
		return properties.iterator();		
	}

	public Iterator<MarshallingElement> getAttributeProperties() {
		return new SkipIterator(properties.iterator(), TYPE_ATTRIBUTES);		
	}

	public Iterator<MarshallingElement> getElementProperties() {		
		return new SkipIterator(properties.iterator(), TYPE_ELEMENTS);		
	}
	
	protected static String ucFirst(String name) {
		if (name.length() == 0)
			return name;
		return name.substring(0, 1).toUpperCase() + (name.length() > 1 ? name.substring(1) : "");
	}	

	public String toString() {
		return "MarshallingInfo(" 
				+"namespace="+namespace
				+", name="+name
				+", targetClass="+targetClass
				+", targetType="+targetType
				+", isXMLRootType="+isXMLRootType
				+")";
	}
	
	class SkipIterator implements Iterator<MarshallingElement> {

		Iterator<MarshallingElement> delegate;
		int wanted_type;
		MarshallingElement next;
		
		public SkipIterator(Iterator<MarshallingElement> delegate, int wanted_type) {
			this.delegate = delegate;
			this.wanted_type = wanted_type;
			findNext();
		}
		
		protected void findNext() {
			if (!delegate.hasNext()) {
				next = null;
				return;
			}
			next = delegate.next();
			if ((wanted_type == TYPE_ATTRIBUTES && !next.encodeAsAttribute)
				|| (wanted_type == TYPE_ELEMENTS && next.encodeAsAttribute))
				findNext();			
		}
		
		public boolean hasNext() {
			return next != null;
		}

		public MarshallingElement next() {
			if (next == null)
				throw new IllegalStateException("no more items");
			MarshallingElement result = next;					
			findNext();
			return result;
		}

		public void remove() {
			delegate.remove();
		}		
	}

}
