/*
 * scio,
 * lightweight messaging framework
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'scio'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.scio;

import javax.xml.namespace.QName;


/**
 * A message is a logical construct for exchange of information 
 * between communication partners. The message consists of meta data describing the message, 
 * as well as the content of the message.
 * 
 * <p>A message can be originated by different underlying systems 
 * (e.g. WebService interface, REST interface, XMPP message, GUI event, database trigger).
 * Depending on the this, the properties of the message are derived from a different origin.
 * But since they represent semantical facts common to every message exchange, they should be 
 * available in all those system with well defined meaning</p>
 * 
 * <p>Depending on the content type of the message,
 * the content may be reprensented in different java types. Aside of this
 * type specific representation, each message has to provide a representation in XML.
 * For some message types (e.g. XML Documents), this may be the same representation.<p>
 *
 * @autor Sebastian Mancke, tarent GmbH
 */
public interface Message extends XmlSerializable {

    /**
     * A unique id of the message within this system.
     *
     * @return The id of the message. 
     *         <null>, if the message is new (was never send through the system).
     */
    public String getId();

    /**
     * Sets the unique id of the message within this system.
     *
     */
    public void setId(String theId);
    
    /**
     * Returns, whether the contents of this message is represented by a postion in
     * an underlaying stream. Streaming messages may be only processed once. The
     * processing of a streaming message has to be done immediately after the notification
     * with the message (not asynchronous or at a later time).
     *
     */
    public boolean isStreamingMessage();
    
    /**
     * Indicates, whether this messages represents an error message 
     */
    public boolean isErrorMessage();
    
    /**
     * The id of the the original message causing this recall message
     * in an asynchronous request response scenario. This recall id is 
     * used for message correlation.
     */
    //TODO: this may not suffer, because a complete id history (maybe a stack) may be needed here.
    public String getRecallId();

    /**
     * The subject, describing the content or the intention of the message.
     * 
     */
    public String getSubject();

    /**
     * The sender of the message
     */
    public String getFrom();

    /**
     * The desired recipient of the message
     */
    public String getTo();

    /**
     * The type of the content of the message.
     */
    public QName getContentType();

    /**
     * The content of the message. This method returns a different java type,
     * depending on the content type of the message.
     */
    public Object getContent();
  
}